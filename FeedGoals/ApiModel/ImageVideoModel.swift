//
//  ImageVideoModel.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 16/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import AVKit

class ImageVideoModel: NSObject {
    
    var imageSelect = UIImage(),
    isVideoSelected = Bool(),
    videoUrl = URL(string: ""),
    videoUrlInString = String(),
    videoThumbhImage = UIImage(),
    isSelected = Bool(),
    idMedia = String()
}

class SaveMyFilterModel: NSObject {
    
    var filterImageInString = String(),
    filterName = String(),
    isSelected = Bool(),
    filterId = String(),
    isVideofilterApply = Bool(),
    
    isPresetApply = Bool(),
    presetName = String(),
    
    editFilerName = String(),
    editFilterValue = Double(),
    isEditFilterApply = Bool(),
    filterKeyEdit = String(),
    filterInputkeyEdit = String(),
    
    isColorFilterApply = Bool(),
    colorIndex = Int(),
    colorHue = Double(),
    colorSaturation = Double(),
    colorLuminance = Double(),
    
    dustFilterImageInString = String(),
    isDustFilterApply = Bool(),
    dustAlphaVal = Double(),
    
    flareFilterImageInString = String(),
    isFlareFilterApply = Bool(),
    flareAlphaVal = Double(),
    
    effectsFilterImageInString = String(),
    isEffectsFilterApply = Bool(),
    effectAlphaVal = Double()
}

class VideoFilterModel: NSObject {
    
    var isPresetVideoApply = Bool(),
    isEditVideoApply = Bool(),
    isColorVideoApply = Bool(),
    isDustVideoApply = Bool(),
    isFlareVideoApply = Bool(),
    isEffectVideoApply = Bool(),
    
    videoUrlFiltered = URL(string: ""),
    thumbVideoImage = UIImage(),
    
    presetFilterName = String(),
    
    filterNameEdit = String(),
    filterKeyEdit = String(),
    filterInputkeyEdit = String(),
    filterValEdit = CGFloat(),
    
    colorApplyIndex = Int(),
    colorHue = CGFloat(),
    colorSaturation = CGFloat(),
    colorLuminance = CGFloat(),
    
    dustFlareEffectUrl = URL(string: ""),
    
    presetCiImage = CIImage(),
    colorCiImage = CIImage(),
    editCiImage = CIImage(),
    lastFilterCiImage = CIImage()
}
