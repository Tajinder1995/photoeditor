//
//  AppDelegate.swift
//  Ordinem_Staff
//
//  Created by netset on 05/11/19.
//  Copyright © 2019 netset. All rights reserved.
// # netset.App.FeedGoals   (netset.FeedGoals.App(Old)) n
// https://web.getappbox.com?url=/s/pvz7wa4lv19uqtk/appinfo.json

import UIKit
import Firebase
import SwiftKeychainWrapper
import CoreData

var arraySaveMyFilter = [SaveMyFilterModel]()
var arraySelectImageVideo = [ImageVideoModel]()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 13.0, *) {
            let windowScene = UIApplication.shared
                .connectedScenes
                .filter { $0.activationState == .foregroundActive }
                .first
            if let windowScene = windowScene as? UIWindowScene {
                window = UIWindow(windowScene: windowScene)
            }
        }
        FirebaseApp.configure()
        if !KeychainWrapper.standard.hasValue(forKey: "deviceUniqueId") {
            KeychainWrapper.standard.set(deviceId, forKey: "deviceUniqueId")
        }
        fetchMediaDetailFromDB()
        fetchSaveFilterValueFromDB()
        return true
    }
    
    func fetchMediaDetailFromDB() {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MediaTable")
        do {
            let detailMediaTable = try managedContext.fetch(fetchRequest)
            saveValueInArrayFromDB(arrayMediaList: detailMediaTable)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func saveValueInArrayFromDB(arrayMediaList: [NSManagedObject]) {
        arraySelectImageVideo.removeAll()
        for i in 0..<arrayMediaList.count {
            let dictDetail = arrayMediaList[i]
            let objImageDetail = ImageVideoModel()
            objImageDetail.isVideoSelected = dictDetail.value(forKeyPath: "isVideoSelected") as? Bool ?? false
            objImageDetail.idMedia = dictDetail.value(forKeyPath: "id") as? String ?? ""
            if objImageDetail.isVideoSelected {
                let videoUrlGet = getUrlVideoFromdirectory(filePath: dictDetail.value(forKeyPath: "videoUrl") as? String ?? "")
                let imageData = dictDetail.value(forKeyPath: "videoThumbhImage") as? Data
                if videoUrlGet != nil && imageData != nil {
                    objImageDetail.videoUrl = videoUrlGet
                    objImageDetail.videoUrlInString = dictDetail.value(forKeyPath: "videoUrl") as? String ?? ""
                    objImageDetail.videoThumbhImage = UIImage(data: imageData!)!
                    arraySelectImageVideo.append(objImageDetail)
                }
            } else {
                let imageDataGet = dictDetail.value(forKeyPath: "imageSelectUrl") as? Data
                if imageDataGet != nil {
                    objImageDetail.imageSelect = UIImage(data: imageDataGet!)!
                    arraySelectImageVideo.append(objImageDetail)
                }
            }
        }
    }
    
    func getUrlVideoFromdirectory(filePath: String) -> URL?  {
        let fileManager = FileManager.default
        let videoPath = (getDirectoryPath() as NSString).appendingPathComponent(filePath)
        if fileManager.fileExists(atPath: videoPath){
            return URL(fileURLWithPath: videoPath)
        } else {
            return nil
        }
    }
    
    func fetchSaveFilterValueFromDB() {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SaveFiltersTable")
        do {
            let detailSaveFilter = try managedContext.fetch(fetchRequest)
            saveValueInArrayOfSaveFilters(arrayFilterList: detailSaveFilter)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func saveValueInArrayOfSaveFilters(arrayFilterList: [NSManagedObject]) {
        arraySaveMyFilter.removeAll()
        for i in 0..<arrayFilterList.count {
            let dictDetail = arrayFilterList[i]
            let objSaveModel = SaveMyFilterModel()
            objSaveModel.isVideofilterApply = dictDetail.value(forKeyPath: "isVideofilterApply") as? Bool ?? false
            objSaveModel.presetName = dictDetail.value(forKeyPath: "presetName") as? String ?? ""
            objSaveModel.isPresetApply = dictDetail.value(forKeyPath: "isPresetApply") as? Bool ?? false
            
            objSaveModel.isEditFilterApply = dictDetail.value(forKeyPath: "isEditFilterApply") as? Bool ?? false
            objSaveModel.editFilterValue = dictDetail.value(forKeyPath: "editFilterValue") as? Double ?? 0.0
            objSaveModel.editFilerName = dictDetail.value(forKeyPath: "editFilerName") as? String ?? ""
            objSaveModel.filterInputkeyEdit = dictDetail.value(forKeyPath: "filterInputkeyEdit") as? String ?? ""
            objSaveModel.filterKeyEdit = dictDetail.value(forKeyPath: "filterKeyEdit") as? String ?? ""
            
            objSaveModel.colorIndex = dictDetail.value(forKeyPath: "colorIndex") as? Int ?? 0
            objSaveModel.isColorFilterApply = dictDetail.value(forKeyPath: "isColorFilterApply") as? Bool ?? false
            objSaveModel.colorHue = dictDetail.value(forKeyPath: "colorHue") as? Double ?? 0.0
            objSaveModel.colorSaturation = dictDetail.value(forKeyPath: "colorSaturation") as? Double ?? 0.0
            objSaveModel.colorLuminance = dictDetail.value(forKeyPath: "colorLuminance") as? Double ?? 0.0
            
            objSaveModel.isDustFilterApply = dictDetail.value(forKeyPath: "isDustFilterApply") as? Bool ?? false
            objSaveModel.dustFilterImageInString = dictDetail.value(forKeyPath: "dustFilterImageInString") as? String ?? ""
            objSaveModel.dustAlphaVal = dictDetail.value(forKeyPath: "dustAlphaVal") as? Double ?? 0.0
            
            objSaveModel.isFlareFilterApply = dictDetail.value(forKeyPath: "isFlareFilterApply") as? Bool ?? false
            objSaveModel.flareAlphaVal = dictDetail.value(forKeyPath: "flareAlphaVal") as? Double ?? 0.0
            objSaveModel.flareFilterImageInString = dictDetail.value(forKeyPath: "flareFilterImageInString") as? String ?? ""
            
            objSaveModel.isEffectsFilterApply = dictDetail.value(forKeyPath: "isEffectsFilterApply") as? Bool ?? false
            objSaveModel.effectAlphaVal = dictDetail.value(forKeyPath: "effectAlphaVal") as? Double ?? 0.0
            objSaveModel.effectsFilterImageInString = dictDetail.value(forKeyPath: "effectsFilterImageInString") as? String ?? ""
            
            objSaveModel.filterId = dictDetail.value(forKeyPath: "filterId") as? String ?? ""
            objSaveModel.filterImageInString = dictDetail.value(forKeyPath: "filterImageInString") as? String ?? ""
            objSaveModel.filterName = dictDetail.value(forKeyPath: "filterName") as? String ?? ""
            objSaveModel.isSelected = dictDetail.value(forKeyPath: "isSelected") as? Bool ?? false
            
            arraySaveMyFilter.append(objSaveModel)
        }
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "FeedGoals")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func clearAllFilesFromTempDirectory() {
        let fileManager = FileManager.default
        do {
            let strTempPath = getDirectoryPath()
            let filePaths = try fileManager.contentsOfDirectory(atPath: strTempPath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: strTempPath + "/" + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
}
