//
//  EditingEditFilterVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 19/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class EditingEditFilterCVC: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var lblEditFilterName: UILabel!
    @IBOutlet weak var imgVwEditPic: UIImageView!
}
