//
//  EditingFilterCVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 17/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class EditingFilterCVC: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var imgVwFilter: UIImageView!
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var imgVwLock: UIImageView!
    
}
