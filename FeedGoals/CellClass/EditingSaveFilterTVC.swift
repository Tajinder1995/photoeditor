//
//  EditingSaveFilterTVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 21/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class EditingSaveFilterTVC: UITableViewCell {

    //MARK:- IBOutlets
    @IBOutlet weak var lblFilterName: UILabel!
    @IBOutlet weak var imgVwFilter: UIImageView!
    @IBOutlet weak var vwBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
