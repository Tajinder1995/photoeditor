//
//  HomePhotoCVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 14/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class HomePhotoCVC: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var imgVwPhoto: UIImageView!
    @IBOutlet weak var imgVwVideoIcon: UIImageView!
}
