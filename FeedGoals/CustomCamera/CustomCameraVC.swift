//
//  ViewController.swift
//  camera
//
//  Created by Natalia Terlecka on 10/10/14.
//  Copyright (c) 2014 Imaginary Cloud. All rights reserved.
//

import CoreLocation
import UIKit

class CustomCameraVC: UIViewController {
    
    // MARK: - @IBOutlets
    @IBOutlet var flashModeImageView: UIImageView!
    @IBOutlet var cameraTypeImageView: UIImageView!
    @IBOutlet var cameraView: UIView!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var vwButtonFirstLayer: UIView!
    @IBOutlet weak var vwButtonSecondLayer: UIView!
    @IBOutlet weak var lblTimer: UILabel!
    
    // MARK :- Variables
    let cameraManager = CameraManager()
    var timerVideo = Timer()
    var videoTimeCount = 0
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCameraManager()
        let currentCameraState = cameraManager.currentCameraStatus()
        if currentCameraState == .notDetermined {
            cameraPermissionsMethod()
        } else if currentCameraState == .ready {
            addCameraToView()
        } else {
            cameraPermissionsMethod()
        }
        flashModeImageView.image = UIImage(named: "flash_off")
        if cameraManager.hasFlash {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(changeFlashMode))
            flashModeImageView.addGestureRecognizer(tapGesture)
        }
        btnPhoto.backgroundColor = .clear
        btnVideo.backgroundColor = Color.AppColor
        
        cameraTypeImageView.image = UIImage(named: "switch_camera")
        let cameraTypeGesture = UITapGestureRecognizer(target: self, action: #selector(changeCameraDevice))
        cameraTypeImageView.addGestureRecognizer(cameraTypeGesture)
        cameraManager.cameraOutputQuality = .high
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraManager.resumeCaptureSession()
        cameraManager.startQRCodeDetection { result in
            switch result {
                case .success(let value):
                    print(value)
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopQRCodeDetection()
        cameraManager.stopCaptureSession()
    }
    
    // MARK: - ViewController
    fileprivate func setupCameraManager() {
        cameraManager.shouldEnableExposure = true
        cameraManager.writeFilesToPhoneLibrary = false
        cameraManager.shouldFlipFrontCameraImage = false
        cameraManager.showAccessPermissionPopupAutomatically = false
    }
    
    fileprivate func addCameraToView() {
        cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: CameraOutputMode.videoWithMic)
        cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
            let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) -> Void in }))
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func actionBtnCancel(_ sender: Any) {
        self.dissmisViewController(true)
    }
    
    // MARK: - @IBActions
    @IBAction func changeFlashMode(_ sender: UIButton) {
        switch cameraManager.changeFlashMode() {
            case .off:
                flashModeImageView.image = UIImage(named: "flash_off")
            case .on:
                flashModeImageView.image = UIImage(named: "flash_on")
            case .auto:
                flashModeImageView.image = UIImage(named: "flash_auto")
        }
    }
    
    @IBAction func actionBtnPhoto(_ sender: Any) {
        if !cameraButton.isSelected {
            lblTimer.isHidden = true
            cameraManager.cameraOutputMode = CameraOutputMode.stillImage
            if cameraManager.cameraOutputMode == .stillImage {
                cameraButton.isSelected = false
                btnPhoto.backgroundColor = Color.AppColor
                btnVideo.backgroundColor = .clear
            }
        }
    }
    
    @IBAction func actionBtnVideo(_ sender: Any) {
        if !cameraButton.isSelected {
            lblTimer.isHidden = false
            cameraManager.cameraOutputMode = CameraOutputMode.videoWithMic
            if cameraManager.cameraOutputMode == .videoWithMic || cameraManager.cameraOutputMode == .videoOnly {
                cameraButton.isSelected = false
                btnPhoto.backgroundColor = .clear
                btnVideo.backgroundColor = Color.AppColor
            }
        }
    }
    
    func string(from timeInterval: TimeInterval) -> String? {
        let interval = Int(timeInterval)
        let seconds: Int = interval % 60
        let minutes: Int = (interval / 60) % 60
        let hours: Int = interval / 3600
        return String(format: "%0.2ld:%0.2ld:%0.2ld", hours, minutes, seconds)
    }
    
    @IBAction func recordButtonTapped(_ sender: UIButton) {
        switch cameraManager.cameraOutputMode {
        case .stillImage:
            lblTimer.isHidden = true
            cameraManager.capturePictureWithCompletion { result in
                switch result {
                case .failure:
                    self.cameraManager.showErrorBlock("Error occurred", "Cannot save picture.")
                case .success(let content):
                    if let capturedData = content.asData {
                        let capturedImage = UIImage(data: capturedData)!
                        self.dismiss(animated: true) {
                            objPassImageDelegate?.passSelectedImage(selectImage: capturedImage.fixedOrientation())
                        }
                    }
                }
            }
        case .videoWithMic, .videoOnly:
            lblTimer.isHidden = false
            cameraButton.isSelected = !cameraButton.isSelected
            if sender.isSelected {
                startTimer()
                vwButtonSecondLayer.backgroundColor = Color.AppColor
                vwButtonFirstLayer.layer.borderColor = Color.AppColor.cgColor
                cameraManager.startRecordingVideo()
            } else {
                stopTimer()
                vwButtonSecondLayer.backgroundColor = .white
                vwButtonFirstLayer.layer.borderColor = UIColor.white.cgColor
                cameraManager.stopVideoRecording { (content, error) -> Void in
                    if error != nil {
                        self.cameraManager.showErrorBlock("Error occurred", "Cannot save video.")
                    } else {
                        if let capturedUrl = content {
                            self.dismiss(animated: true) {
                                objPassImageDelegate?.passSelectedVideo(videoUrl: capturedUrl, isCamera: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func startTimer() {
        timerVideo = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerCountIncrease), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        timerVideo.invalidate()
    }
    
    @objc func timerCountIncrease() {
        lblTimer.text = string(from: TimeInterval(videoTimeCount))
        videoTimeCount = videoTimeCount + 1
    }
    
    @objc func changeCameraDevice() {
        if !cameraButton.isSelected {
            cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
        }
    }
    
    func cameraPermissionsMethod() {
        cameraManager.askUserForCameraPermission { permissionGranted in
            if permissionGranted {
                self.addCameraToView()
            } else {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
        }
    }
    
    @IBAction func changeCameraQuality() {
        switch cameraManager.cameraOutputQuality {
            case .high:
                cameraManager.cameraOutputQuality = .medium
            case .medium:
                cameraManager.cameraOutputQuality = .low
            case .low:
                cameraManager.cameraOutputQuality = .high
            default:
                cameraManager.cameraOutputQuality = .high
        }
    }
}

public extension Data {
    
    func printExifData() {
        let cfdata: CFData = self as CFData
        let imageSourceRef = CGImageSourceCreateWithData(cfdata, nil)
        let imageProperties = CGImageSourceCopyMetadataAtIndex(imageSourceRef!, 0, nil)!
        let mutableMetadata = CGImageMetadataCreateMutableCopy(imageProperties)!
        CGImageMetadataEnumerateTagsUsingBlock(mutableMetadata, nil, nil) { _, tag in
            print(CGImageMetadataTagCopyName(tag)!, ":", CGImageMetadataTagCopyValue(tag)!)
            return true
        }
    }
}
