//
//  AppAlerts.swift
//  Ordinem
//
//  Created by netset on 1/25/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class AppAlerts {
    static var titleValue: AppAlerts {
        return AppAlerts()
    }
    fileprivate init(){}
    
    //MARK:- App Common Titles
    var chooseImage = "Choose Image"
    var camrea = "Camera"
    var ok = "Ok"
    var cancel = "Cancel"
    var camera = "Camera"
    var gallery = "Gallery"
    var cameraNotsupported = "Camera is not supported"
    var connectionProblem = "Connection Problem"
    var setting = "Setting"
    var locationProblem = "Location Problem"
    var error = "Error!"
    var ago = "ago"
    var year = "year"
    var years = "years"
    var month = "month"
    var months = "months"
    var day = "day"
    var days = "days"
    var hour = "hour"
    var hours = "hours"
    var minute = "minute"
    var minutes = "minutes"
    var second = "second"
    var seconds = "seconds"
    var aMoment = "a moment"
    var newPresent = "New Preset"
    
    //MARK:- App Common Alerts
    var pleaseReviewyournetworksettings = "Please Review your network settings"
    var checkInternet = "Please check your internet connection"
    var enableLocation = "Please enable your location"
    var ErrorUnabletoencodeJSONResponse = "Error: Unable to encode JSON Response"
    var serverError = "Server error, Please try again.."
    var serverNotResponding = "Server not responding"
    var validLink = "This url is not valid"
    //App Alert
    var imageVideoNotFount = "Image and video not found"
    var imageNotFount = "Image not found"
    var presetNameEnter = "Enter the preset name:"
    var loginFirstApp = "Please first login the App"
    var emailEmpty = "Email address field can't be empty"
    var emailNotValid = "Email address is not valid"
    var selectOneItem = "Please select any one item"
    var workingOnIt = "Working on these filter for video"
    var videoSavedGallery = "Video saved in gallery successfully"
    var imageSavedGallery = "Image saved in gallery successfully"
    var videoSaved = "Video saved successfully"
    var imageSaved = "Image saved successfully"

}
