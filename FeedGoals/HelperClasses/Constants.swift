//
//  AppDelegate.swift
//  Ordinem_Staff
//
//  Created by netset on 05/11/19.
//  Copyright © 2019 netset. All rights reserved.
// DA6C41

import UIKit

enum AppInfo {
    static let AppName = "FeedGoals"
    static let DeviceName =  UIDevice.current.name
    static let DeviceId =  UIDevice.current.identifierForVendor!.uuidString
    static let ServerLink = "https://www.ordinem-app.com"
}

enum Color {
    static let AppColor = UIColor(red: 218/255, green: 108/255, blue: 65/255, alpha: 1)
    static let selectedSaveFilterColor = UIColor(red: 236/255, green: 164/255, blue: 171/255, alpha: 1)
    
    static let redColorWithAlpha = UIColor(red: 232/255, green: 51/255, blue: 36/255, alpha: 0.3)
    static let brownColorWithAlpha = UIColor(red: 235/255, green: 136/255, blue: 51/255, alpha: 0.3)
    static let yellowColorWithAlpha = UIColor(red: 254/255, green: 255/255, blue: 87/255, alpha: 0.3)
    static let greenColorWithAlpha = UIColor(red: 115/255, green: 248/255, blue: 76/255, alpha: 0.3)
    static let orangeColorWithAlpha = UIColor(red: 226/255, green: 51/255, blue: 236/255, alpha: 0.3)
    static let pinkColorWithAlpha = UIColor(red: 119/255, green: 20/255, blue: 128/255, alpha: 0.3)
    static let purpleBlueColorWithAlpha = UIColor(red: 10/255, green: 5/255, blue: 204/255, alpha: 0.3)
    static let cyanColorWithAlpha = UIColor(red: 115/255, green: 245/255, blue: 253/255, alpha: 0.3)
    
    static let redColor = UIColor(red: 0.901961, green: 0.270588, blue: 0.270588, alpha: 1)
    static let orangeColor = UIColor(red: 0.901961, green: 0.584314, blue: 0.270588, alpha: 1)
    static let yellowColor = UIColor(red: 0.901961, green: 0.901961, blue: 0.270588, alpha: 1)
    static let greenColor = UIColor(red: 0.270588, green: 0.901961, blue: 0.270588, alpha: 1)
    static let aquaColor = UIColor(red: 0.270588, green: 0.901961, blue: 0.901961, alpha: 1)
    static let blueColor = UIColor(red: 0.270588, green: 0.270588, blue: 0.901961, alpha: 1)
    static let purpleColor = UIColor(red: 0.584314, green: 0.270588, blue: 0.901961, alpha: 1)
    static let magentaColor = UIColor(red: 0.901961, green: 0.270588, blue: 0.901961, alpha: 1)
}

enum ColorName {
    static let red = "Red"
    static let orange = "Orange"
    static let yellow = "Yellow"
    static let green = "Green"
    static let aqua = "Aqua"
    static let blue = "Blue"
    static let purple = "Purple"
    static let magenta = "Magenta"
}
 
enum DateFormatStyle {
    static let DDMMYYYY = "YYYY-MM-dd"
    static let YYYYMMDD = "dd.MM.yyyy"
    static let HHMMA = "hh:mm a"
    static let HHMM = "HH:mm"
    static let EEEE = "EEEE"
    static let YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss"
}

enum BottomFilters {
    static let Presets = 1
    static let Edit = 2
    static let Color = 3
    static let Dust = 4
    static let Flare = 5
    static let Effects = 6
    static let Save = 7
}

//MARK:- Header Param
enum Param {
    static let contentType = "Content-Type"
    static let auth = "apisecret"
    static let appVersion = "APPVERSION"
    static let authToken = "token"
    static let deviceType = "DEVICETYPE"
    static let ios = "ios"
    static let appJson = "application/json"
    static let deviceId = "DEVICEID"
    static let fcmToken = "FCMDEVICEID"
}

//MARK:- Get App Version Method
var getAppVersion: String {
    get {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
    }
}

//MARK:- Filter Dummy Image
var filterDummyImage: String {
    get {
        return "wallpaper"
    }
}

//MARK:- Get Slider Redius
var sliderRedius: CGFloat {
    get {
        return 28
    }
}

//MARK:- App Header
func headers() ->  [String:String] {
    var appVersion = String()
    if let info = Bundle.main.infoDictionary {
        appVersion = info["CFBundleShortVersionString"] as? String ?? "1.0"
    }
    let headers = [
        Param.contentType:Param.appJson,
        Param.deviceId:AppInfo.DeviceId,
        Param.deviceType:Param.ios,
        Param.appVersion:appVersion,
        Param.auth:accessToken,
    ]
    debugPrint("headers: ",headers)
    return headers
}

//MARK:- Save Data In Database Methods
var accessToken: String {
    get {
        return UserDefaults.standard.value(forKey: "accessToken") as? String ?? ""
    }
    set {
        UserDefaults.standard.setValue(newValue, forKey: "accessToken")
    }
}

//MARK:- Generate Random String Method
var generateRandomString: String {
    get {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomStr = String((0..<10).map{ _ in letters.randomElement()! })
        debugPrint("Random String :- ","\(Int64(Date().timeIntervalSince1970 * 1000))\(randomStr)")
        return "\(Int64(Date().timeIntervalSince1970 * 1000))\(randomStr)"
    }
}

func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

//MARK:- Edit Filter
enum EditFilter {
    static let exposure = "Exposure"
    static let brightness = "Brightness"
    static let contrest = "Contrast"
    static let shadow = "Shadows"
    static let hightlights = "Highlights"
    static let Saturation = "Saturation"
    static let vibrance = "Vibrance"
    static let temperature = "Temperature"
    static let tint = "Tint"
    static let sharpen = "Sharpen"
    static let vignette = "Vignette"
    static let grain = "Grain"
}

//MARK:- Edit Filter
enum OverlayFilterType {
    static let dust = "Dust"
    static let flare = "Flare"
    static let effects = "Effects"
}
