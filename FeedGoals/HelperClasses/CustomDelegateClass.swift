//
//  CustomDelegateClass.swift
//  Ordinem
//
//  Created by netset on 12/13/19.
//  Copyright © 2019 netset. All rights reserved.
//

import UIKit

//MARK:-Save Data Home Screen Delegate
protocol SaveDataHomeScreenDelegate {
    func saveDataInDatabase(_ objImageVideoModel: ImageVideoModel,selectIndex: Int)
}
var objSaveDataHomeScreenDelegate:SaveDataHomeScreenDelegate?

//MARK:-Save Data Home Screen Delegate
protocol SaveAndShareDelegate {
    func SaveItemInGallery(_ isSave: Bool)
}
var objSaveAndShareDelegate:SaveAndShareDelegate?
