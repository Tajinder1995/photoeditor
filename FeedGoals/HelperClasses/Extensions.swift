//
//  Extensions.swift
//  Ordinem_Staff
//
//  Created by netset on 11/11/19.
//  Copyright © 2019 netset. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            if UIDevice.current.userInterfaceIdiom == .phone {
                layer.cornerRadius = newValue
                layer.masksToBounds = newValue > 0
            }
        }
    }
    
    @IBInspectable var borderColor:UIColor? {
        get {
            return UIColor(cgColor:layer.borderColor!)
        } set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var borderWidth:Float {
        get {
            return Float(layer.borderWidth)
        }
        set {
            layer.borderWidth = CGFloat(newValue)
        }
    }
    
}
extension UIViewController {
    
    func pushToNextController(_ identifier: String,isAnimation: Bool) {
        let conrollerObj = storyboardObj.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(conrollerObj, animated: isAnimation)
    }
    
    func pushToNextControllerWithTitle(_ identifier: String,title: String,isAnimation: Bool) {
        let conrollerObj = storyboardObj.instantiateViewController(withIdentifier: identifier)
        conrollerObj.title = title
        self.navigationController?.pushViewController(conrollerObj, animated: isAnimation)
    }
    
    func popViewController(_ isAnimation: Bool) {
        self.navigationController?.popViewController(animated: isAnimation)
    }
    
    func presentViewController(_ identifier: String,isAnimation: Bool) {
        let conrollerObj = storyboardObj.instantiateViewController(withIdentifier: identifier)
        self.present(conrollerObj, animated: isAnimation, completion: nil)
    }
    
    func dissmisViewController(_ isAnimation: Bool) {
        self.dismiss(animated: isAnimation, completion: nil)
    }
    
    func reloadCurrentViewController() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view)
    }
    
    //MARK:- Display Toast Methos
    func displayAlertMessage(_ message: String) {
        let alert = UIAlertController(title: AppInfo.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Display Toast Methos
    func displayAlertMessageWithOk(_ message: String,completion:@escaping() -> Void) {
        let alert = UIAlertController(title: AppInfo.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default, handler: { (_) in
            completion()
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func shareImageButton(_ imageSend: UIImage) {
        let imageToShare = [ imageSend ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.mail,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.markupAsPDF,UIActivity.ActivityType.message,UIActivity.ActivityType.openInIBooks,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.postToWeibo,UIActivity.ActivityType.postToVimeo,UIActivity.ActivityType.postToTencentWeibo,UIActivity.ActivityType.postToFlickr]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareVideoButton(_ videoUrl: URL) {
        let videoToShare = [ videoUrl ]
        let activityViewController = UIActivityViewController(activityItems: videoToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.mail,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.markupAsPDF,UIActivity.ActivityType.message,UIActivity.ActivityType.openInIBooks,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.postToWeibo,UIActivity.ActivityType.postToVimeo,UIActivity.ActivityType.postToTencentWeibo,UIActivity.ActivityType.postToFlickr]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareTextButton(_ shareText: String) {
        let textToShare = [ shareText ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.copyToPasteboard,UIActivity.ActivityType.mail,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.markupAsPDF,UIActivity.ActivityType.message,UIActivity.ActivityType.openInIBooks,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.postToWeibo,UIActivity.ActivityType.postToVimeo,UIActivity.ActivityType.postToTencentWeibo,UIActivity.ActivityType.postToFlickr]
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension String {
    
    var isBlank : Bool {
        return (self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    var isNumeric : Bool {
        return NumberFormatter().number(from: self) != nil
    }
    
    var showCommaPrice : String {
        if self != "" {
            let priceStr = String(format: "%.2f", Double(self)!)
            return priceStr.replacingOccurrences(of: ".", with: ",")
        } else {
            return ""
        }
    }
    
    var isNumericOfPrice: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."]
        return Set(self).isSubset(of: nums)
    }
    
    func strikeThrough() -> NSAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,value: 1,range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}

extension Double {
    
    func setDecimalValue(_ value: Int) -> String {
        return String(format:"%.\(value)f", self)
    }
}

extension Float {
    
    func setDecimalValue(_ value: Int) -> String {
        return String(format:"%.\(value)f", self)
    }
}

extension UITextView {
    
    var isBlank : Bool {
        return (self.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!
    }
}

extension UITextField {
    
    var isBlank : Bool {
        return (self.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!
    }
    
    var trimmedValue : String {
        return (self.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
    }
    
    func rightImage(image:UIImage,imgW:Int,imgH:Int)  {
        let iconView = UIImageView(frame: CGRect(x: 8, y: 0, width: imgW, height: imgW))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 8, y: 0, width: imgW + 8, height: imgW))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    func leftImageAndPlaceHolder(image:UIImage,imgW:Int,imgH:Int,txtString: String) {
        let iconView = UIImageView(frame: CGRect(x: 8, y: 0, width: imgW, height: imgW))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 8, y: 0, width: imgW + 8, height: imgW))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.attributedPlaceholder = NSAttributedString(string: txtString, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
    }
    
    func bottomBorder() {
        let border = CALayer()
        let width  = CGFloat(1.0)
        border.borderColor = (UIColor.lightGray).cgColor
        border.borderWidth = width
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: 1)
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func setPlaceHolderColor(txtString: String){
        self.attributedPlaceholder = NSAttributedString(string: txtString, attributes: [NSAttributedString.Key.foregroundColor : UIColor.darkGray])
        
    }
    
    func goToNextTextFeild(nextTextFeild:UITextField) {
        self.resignFirstResponder()
        nextTextFeild.becomeFirstResponder()
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIWindow {
    
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }
    
    static func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        if let navigationController = vc as? UINavigationController,
            let visibleController = navigationController.visibleViewController  {
            return UIWindow.getVisibleViewControllerFrom( vc: visibleController )
        } else if let tabBarController = vc as? UITabBarController,
            let selectedTabController = tabBarController.selectedViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: selectedTabController )
        } else {
            if let presentedViewController = vc.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            } else {
                return vc
            }
        }
    }
}

extension Date {
    
    func getTimeInterval() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "\(AppAlerts.titleValue.year) \(AppAlerts.titleValue.ago)" :
                "\(year)" + " " + "\(AppAlerts.titleValue.years) \(AppAlerts.titleValue.ago)"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "\(AppAlerts.titleValue.month) \(AppAlerts.titleValue.ago)" :
                "\(month)" + " " + "\(AppAlerts.titleValue.months) \(AppAlerts.titleValue.ago)"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "\(AppAlerts.titleValue.day) \(AppAlerts.titleValue.ago)" :
                "\(day)" + " " + "\(AppAlerts.titleValue.days) \(AppAlerts.titleValue.ago)"
        } else if let hours = interval.hour, hours > 0 {
            return hours == 1 ? "\(hours)" + " " + "\(AppAlerts.titleValue.hour) \(AppAlerts.titleValue.ago)" :
                "\(hours)" + " " + "\(AppAlerts.titleValue.hours) \(AppAlerts.titleValue.ago)"
        } else if let mintue = interval.minute, mintue > 0 {
            return mintue == 1 ? "\(mintue)" + " " + "\(AppAlerts.titleValue.minute) \(AppAlerts.titleValue.ago)" :
                "\(mintue)" + " " + "\(AppAlerts.titleValue.minutes) \(AppAlerts.titleValue.ago)"
        } else if let seconds = interval.second, seconds > 0 {
            return seconds == 1 ? "\(seconds)" + " " + "\(AppAlerts.titleValue.second) \(AppAlerts.titleValue.ago)" :
                "\(seconds)" + " " + "\(AppAlerts.titleValue.seconds) \(AppAlerts.titleValue.ago)"
        } else {
            return "\(AppAlerts.titleValue.aMoment) \(AppAlerts.titleValue.ago)"
            
        }
    }
    
    func dayNumberOfWeek() -> Int? {
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
}

extension Date {
    
    var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    func isSameDate(_ comparisonDate: Date) -> Bool {
        let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
        return order == .orderedSame
    }
    
    func isBeforeDate(_ comparisonDate: Date) -> Bool {
        let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
        return order == .orderedAscending
    }
    
    func isAfterDate(_ comparisonDate: Date) -> Bool {
        let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
        return order == .orderedDescending
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }
}
extension UISlider {
    
    func setCustomThumb(thumbRadius: CGFloat) {
        let thumb = UIView()
        thumb.backgroundColor = .white
        thumb.layer.borderWidth = 2
        thumb.layer.borderColor = Color.AppColor.cgColor
        thumb.frame = CGRect(x: 0, y: thumbRadius / 2, width: thumbRadius, height: thumbRadius)
        thumb.layer.cornerRadius = thumbRadius / 2
        let renderer = UIGraphicsImageRenderer(bounds: thumb.bounds)
        let image = renderer.image { rendererContext in
            thumb.layer.render(in: rendererContext.cgContext)
        }
        self.setThumbImage(image, for: .normal)
    }
}
extension UIImage {
    
    func fixedOrientation() -> UIImage {
        if (imageOrientation == UIImage.Orientation.up) {
            return self
        }
        var transform:CGAffineTransform = CGAffineTransform.identity
        if (imageOrientation == UIImage.Orientation.down
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
        }
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
        }
        if (imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            transform = transform.translatedBy(x: 0, y: size.height);
            transform = transform.rotated(by: CGFloat(-M_PI_2));
        }
        if (imageOrientation == UIImage.Orientation.upMirrored
            || imageOrientation == UIImage.Orientation.downMirrored) {
            
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        if (imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            transform = transform.translatedBy(x: size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        let ctx:CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                                      bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                                      space: cgImage!.colorSpace!,
                                      bitmapInfo: cgImage!.bitmapInfo.rawValue)!
        ctx.concatenate(transform)
        if (imageOrientation == UIImage.Orientation.left
            || imageOrientation == UIImage.Orientation.leftMirrored
            || imageOrientation == UIImage.Orientation.right
            || imageOrientation == UIImage.Orientation.rightMirrored) {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.height,height:size.width))
        } else {
            ctx.draw(cgImage!, in: CGRect(x:0,y:0,width:size.width,height:size.height))
        }
        let cgimg:CGImage = ctx.makeImage()!
        let imgEnd:UIImage = UIImage(cgImage: cgimg)
        return imgEnd
    }
}
