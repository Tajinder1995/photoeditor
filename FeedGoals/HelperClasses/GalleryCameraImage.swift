//
//  GalleryCameraImage.swift
//  Ordinem_Staff
//
//  Created by netset on 11/11/19.
//  Copyright © 2019 netset. All rights reserved.
//

import UIKit

protocol PassImageDelegate {
    func passSelectedImage(selectImage: UIImage)
    func passSelectedVideo(videoUrl: URL,isCamera:Bool)
}
var objPassImageDelegate:PassImageDelegate?

class GalleryCameraImage: NSObject,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK:- variable Decleration
    var ImagePicker = UIImagePickerController()
    var imageTapped = Int()
    var controller = UIViewController()
    var countClicked = 0
    
    //Mark:- Choose Image Method
    func customActionSheet(_ currentCont:UIViewController,image:Bool,video:Bool) {
        controller = currentCont
        let myActionSheet = UIAlertController(title: AppAlerts.titleValue.chooseImage, message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: AppAlerts.titleValue.camera, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCamera(image:image,video:video)
        })
        let galleryAction = UIAlertAction(title: AppAlerts.titleValue.gallery, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.openGallary(image:image,video:video)
        })
        let cancelAction = UIAlertAction(title: AppAlerts.titleValue.cancel, style: UIDevice.current.userInterfaceIdiom == .pad ? .destructive : .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        myActionSheet.addAction(cameraAction)
        myActionSheet.addAction(galleryAction)
        myActionSheet.addAction(cancelAction)
        controller.present(myActionSheet, animated: true, completion: nil)
    }
    
    //MARK:- Open Image Camera
    func openCamera(image:Bool,video:Bool) {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            self.ImagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.ImagePicker.delegate = self
            self.ImagePicker.allowsEditing = false
            if (image == true) && (video == true){
                ImagePicker.mediaTypes = ["public.image", "public.movie"]
                //ImagePicker.videoMaximumDuration = 10
            }else if (image == true) && (video == false) {
                ImagePicker.mediaTypes = ["public.image"]
            }else if (image == false) && (video == true) {
                ImagePicker.mediaTypes = ["public.movie"]
                //ImagePicker.videoMaximumDuration = 10
            }
            self.ImagePicker.modalPresentationStyle = .overFullScreen
            self.ImagePicker.definesPresentationContext = true
            self.ImagePicker.providesPresentationContextTransitionStyle = true
            controller.present(self.ImagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: AppInfo.AppName, message: AppAlerts.titleValue.cameraNotsupported, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: AppAlerts.titleValue.ok, style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(okAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Open Image Gallery
    func openGallary(image:Bool,video:Bool) {
        countClicked = 0
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)) {
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            ImagePicker.delegate = self
            ImagePicker.allowsEditing = false
            if (image == true) && (video == true) {
                ImagePicker.sourceType = .photoLibrary
                ImagePicker.mediaTypes = ["public.image", "public.movie"]
                //ImagePicker.videoMaximumDuration = 10
            }else if (image == true) && (video == false) {
                ImagePicker.sourceType = .photoLibrary
                ImagePicker.mediaTypes = ["public.image"]
            }else if (image == false) && (video == true){
                ImagePicker.mediaTypes = ["public.movie"]
                //ImagePicker.videoMaximumDuration  = 10
                ImagePicker.allowsEditing = false
            }
            ImagePicker.modalPresentationStyle = .overFullScreen
            ImagePicker.definesPresentationContext = true
            ImagePicker.providesPresentationContextTransitionStyle = true
            controller.present(ImagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        countClicked = countClicked + 1
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        let mediaType = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaType)] as? String
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)]  as? UIImage ?? info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
        if countClicked == 1 {
            if mediaType  == "public.image" {
                picker.dismiss(animated: true, completion: nil)
                self.setSelectedimage(image!)
            }
            if mediaType == "public.movie" {
                let pickedVideo = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL ?? info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.referenceURL)] as! URL
                picker.dismiss(animated: true, completion: nil)
                setSelectedVideo(pickedVideo)
            }
        }
    }
    
    //MARK:- Selectedimage
    func setSelectedimage(_ image: UIImage) {
        objPassImageDelegate?.passSelectedImage(selectImage: image)
    }
    
    //MARK:- Selectedimage
    func setSelectedVideo(_ url: URL) {
        objPassImageDelegate?.passSelectedVideo(videoUrl: url, isCamera: false)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
