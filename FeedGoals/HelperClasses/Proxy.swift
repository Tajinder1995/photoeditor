//
//  Proxy.swift
//  Ordinem_Staff
//
//  Created by netset on 11/12/19.
//  Copyright © 2019 netset. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftKeychainWrapper
import AVKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let storyboardObj = UIStoryboard(name: "Main", bundle: nil)
let deviceId = KeychainWrapper.standard.string(forKey: "deviceUniqueId") ?? UUID().uuidString

class Proxy {
    static var shared: Proxy {
        return Proxy()
    }
    fileprivate init(){}
    
    //MARK:- Check Valid Email Method
    func isValidEmail(_ testStr:String) -> Bool  {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        return (testStr.range(of: emailRegEx, options:.regularExpression) != nil)
    }
    
    //MARK:- Show Activity Indicator Method
    func showActivityIndicator() {
        DispatchQueue.main.async {
            let activityData = ActivityData()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        }
    }
    
    //MARK:- Hide Activity Indicator Method
    func hideActivityIndicator()  {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    //MARK:- Display Toast Methos
    func displayStatusCodeAlert(_ message: String) {
        let alert = UIAlertController(title: AppInfo.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default, handler: nil)
        alert.addAction(action)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Display Toast Methos
    func displayStatusPresentAlert(_ message: String,controller: UIViewController) {
        let alert = UIAlertController(title: AppInfo.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default, handler: nil)
        alert.addAction(action)
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Display Toast Methos
    func showAlertOkMessage(message: String,completion:@escaping(_ isSuccess:Bool) -> Void) {
        let alert = UIAlertController(title: AppInfo.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        let actionOk = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default, handler: { (_) in
            completion(true)
        })
        alert.addAction(actionOk)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Display Toast Methos
    func showAlertMessageWithOkCancel(message: String,completion:@escaping(_ isSuccess:Bool) -> Void) {
        let alert = UIAlertController(title: AppInfo.AppName, message: message, preferredStyle: UIAlertController.Style.alert)
        let actionOk = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default, handler: { (_) in
            completion(true)
        })
        let actionCancel = UIAlertAction(title: AppAlerts.titleValue.cancel, style: .default, handler: { (_) in
            completion(false)
        })
        alert.addAction(actionOk)
        alert.addAction(actionCancel)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Get String From Date Method
    func getStringFromDate(date: Date,needFormat: String) -> String {
        var dateStr = String()
        if needFormat != "" {
            let formatter = DateFormatter()
            formatter.dateFormat = needFormat
            let locale = Locale.current
            formatter.locale = locale
            dateStr = formatter.string(from: date)
        }
        return dateStr
    }
    
    //MARK:- Get Date From String Method
    func getDateFromSting(string: String,stringFormat: String) -> Date {
        var date = Date()
        if string != "" {
            let formatter = DateFormatter()
            formatter.dateFormat = stringFormat
            let dateInDate = formatter.date(from: string)
            if dateInDate != nil {
                date = dateInDate!
            }
        }
        return date
    }
    
    //MARK:- UTC To Local Convert Method
    func UTCToLocal(_ UTCDateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale.current // Locale(identifier: "en_US_POSIX_POSIX")
        let UTCDate = dateFormatter.date(from: UTCDateString)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if UTCDate != nil {
            let UTCToCurrentFormat = dateFormatter.string(from: UTCDate!)
            return UTCToCurrentFormat
        } else {
            return ""
        }
    }
    
    //MARK:- Get Date And Time In String Method
    func getDateAndTimeInStr(getDate:String,backendFormat:String,needDateFormat:String,needTimeFormat:String) -> (String,String) {
        var resultDateInStr = String(),resultTimeInStr = String()
        if getDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = backendFormat//"yyyy-MM-dd hh:mm a"
            let dateInStr = dateFormatter.date(from: getDate)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = needDateFormat//"yyyy-MM-dd"
            if dateInStr != nil {
                resultDateInStr = dateFormatter1.string(from: dateInStr!)
            }
            
            dateFormatter1.dateFormat = needTimeFormat//"hh:mm a"
            let timeInStr = dateFormatter.date(from: getDate)
            if timeInStr != nil {
                resultTimeInStr = dateFormatter1.string(from: timeInStr!)
            }
        }
        return (resultDateInStr,resultTimeInStr)
    }
    
    //MARK:- Get Date In String Method
    func getDateInStr(getDate:String,backendFormat:String,needDateFormat:String) -> String {
        var resultDateInStr = String()
        if getDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = backendFormat
            let dateInStr = dateFormatter.date(from: getDate)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = needDateFormat
            if dateInStr != nil {
                resultDateInStr = dateFormatter1.string(from: dateInStr!)
            }
        }
        return resultDateInStr
    }
    
    //MARK:- Get Time In String Method
    func getTimeInStr(getDate:String,backendFormat:String,needTimeFormat:String) -> String {
        var resultTimeInStr = String()
        if getDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = backendFormat
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = needTimeFormat
            let timeInStr = dateFormatter.date(from: getDate)
            if timeInStr != nil {
                resultTimeInStr = dateFormatter1.string(from: timeInStr!)
            }
        }
        return resultTimeInStr
    }
    
    //MARK:- Open Setting Of App
    func openSettingApp() {
        let settingAlert = UIAlertController(title: AppAlerts.titleValue.connectionProblem, message: AppAlerts.titleValue.checkInternet, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AppAlerts.titleValue.cancel, style: UIAlertAction.Style.default, handler: nil)
        settingAlert.addAction(okAction)
        let openSetting = UIAlertAction(title: AppAlerts.titleValue.setting, style:UIAlertAction.Style.default, handler:{ (action: UIAlertAction!) in
            let url:URL = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: {
                    (success) in })
            } else {
                guard UIApplication.shared.openURL(url) else {
                    Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.pleaseReviewyournetworksettings)
                    return
                }
            }
        })
        settingAlert.addAction(openSetting)
        UIApplication.shared.keyWindow?.rootViewController?.present(settingAlert, animated: true, completion: nil)
    }
    
    //MARK:- Open Location Setting Of App
    func openLocationSettingApp() {
        let settingAlert = UIAlertController(title: AppAlerts.titleValue.locationProblem, message: AppAlerts.titleValue.enableLocation, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AppAlerts.titleValue.cancel, style: UIAlertAction.Style.default, handler: nil)
        settingAlert.addAction(okAction)
        let openSetting = UIAlertAction(title: AppAlerts.titleValue.setting, style:UIAlertAction.Style.default, handler:{ (action: UIAlertAction!) in
            let url:URL = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: {
                    (success) in })
            } else {
                guard UIApplication.shared.openURL(url) else {
                    Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.pleaseReviewyournetworksettings)
                    return
                }
            }
        })
        settingAlert.addAction(openSetting)
        UIApplication.shared.keyWindow?.rootViewController?.present(settingAlert, animated: true, completion: nil)
    }
    
    //MARK:- Open Permision Setting Alert Method
    func openPermisionSettingAlert(_ titleVal: String) {
        let settingAlert = UIAlertController(title: AppInfo.AppName, message: titleVal, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AppAlerts.titleValue.cancel, style: UIAlertAction.Style.default, handler: nil)
        settingAlert.addAction(okAction)
        let openSetting = UIAlertAction(title: AppAlerts.titleValue.setting, style:UIAlertAction.Style.default, handler:{ (action: UIAlertAction!) in
            let url:URL = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: {
                    (success) in })
            } else {
                guard UIApplication.shared.openURL(url) else {
                    Proxy.shared.displayStatusCodeAlert(titleVal)
                    return
                }
            }
        })
        settingAlert.addAction(openSetting)
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(settingAlert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Check Notification Permision Setting Method
    func checkNotificationPermisionSetting(_ completion:@escaping(_ isTrue: Bool) -> Void) {
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .notDetermined:
                    completion(false)
                case .denied:
                    completion(false)
                case .authorized:
                    completion(true)
                case .provisional:
                    completion(false)
                default:
                    completion(false)
                }
            })
        } else {
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    //MARK:- Latitude Method
    func getLatitude() -> String {
        if UserDefaults.standard.object(forKey: "lat") != nil {
            return UserDefaults.standard.object(forKey: "lat") as! String
        }
        return ""
    }
    
    //MARK:- Longitude Method
    func getLongitude() -> String {
        if UserDefaults.standard.object(forKey: "long") != nil {
            return UserDefaults.standard.object(forKey: "long") as! String
        }
        return ""
    }
    
    //MARK:- Open Url In Browser
    func openUrlInBrowser(_ urlStr: String) {
        if urlStr != "" {
            if urlStr.contains("https://") || urlStr.contains("http://") {
                if let url = URL(string: urlStr) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    } else {
                        Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.validLink)
                    }
                } else {
                    Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.validLink)
                }
            } else {
                if let url = URL(string: "https://\(urlStr)") {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    } else {
                        Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.validLink)
                    }
                } else {
                    Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.validLink)
                }
            }
        } else {
            Proxy.shared.displayStatusCodeAlert(AppAlerts.titleValue.validLink)
        }
    }
    
    //MARK:- Dismiss Any Alert Controller If Present
    func dismissAnyAlertControllerIfPresent() {
          guard let window :UIWindow = UIApplication.shared.keyWindow , var topVC = window.rootViewController?.presentedViewController else {return}
          while topVC.presentedViewController != nil  {
              topVC = topVC.presentedViewController!
          }
          if topVC.isKind(of: UIAlertController.self) {
              topVC.dismiss(animated: false, completion: nil)
          }
      }
    
    //MARK:- Set Status Bar Color Method
    func setStatusBarColor(_ status: UIStatusBarStyle) {
        UIApplication.shared.statusBarStyle = status
    }
    
    func getVideoThumbnailImageFromVideo(_ videoUrl: URL) -> UIImage {
        let asset = AVURLAsset(url: videoUrl , options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try! imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
        let thumbnail = UIImage(cgImage: cgImage)
        return thumbnail
    }
}
