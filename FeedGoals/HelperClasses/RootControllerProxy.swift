 //
 //  Extensions.swift
 //  Ordinem_Staff
 //
 //  Created by netset on 11/11/19.
 //  Copyright © 2019 netset. All rights reserved.
 //
 
 import UIKit
 
 class RootControllerProxy {
    static var shared: RootControllerProxy {
        return RootControllerProxy()
    }
    fileprivate init(){}
    //MARK:- Set Root Without Drawer Method
    func rootWithoutDrawer(_ identifier: String) {
        let blankController = storyboardObj.instantiateViewController(withIdentifier: identifier)
        var homeNavController:UINavigationController = UINavigationController()
        homeNavController = UINavigationController.init(rootViewController: blankController)
        homeNavController.isNavigationBarHidden = true
        UIApplication.shared.windows.first?.rootViewController = homeNavController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }

 }
