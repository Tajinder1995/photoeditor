//
//  SplashVC.swift
//  Ordinem_Staff
//
//  Created by netset on 11/11/19.
//  Copyright © 2019 netset. All rights reserved.
// netset.App.FeedGoals

import UIKit
import IQKeyboardManager
import SwiftKeychainWrapper

class SplashVC: UIViewController {
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        sleep(2)
        RootControllerProxy.shared.rootWithoutDrawer("HomeVC")
    }
}

