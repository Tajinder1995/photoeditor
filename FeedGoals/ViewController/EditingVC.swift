//
//  EditingVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 17/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import AVKit
import GPUImage
import SwiftKeychainWrapper
import UIImageColors

class EditingVC: UIViewController {
    
    //MARK:-
    @IBOutlet weak var lblSaveFilter: UILabel!
    @IBOutlet weak var lblEffectsFilter: UILabel!
    @IBOutlet weak var lblFlareFilter: UILabel!
    @IBOutlet weak var lblDustFilter: UILabel!
    @IBOutlet weak var lblColorFilter: UILabel!
    @IBOutlet weak var lblEditFilter: UILabel!
    @IBOutlet weak var lblPresetFilter: UILabel!
    @IBOutlet weak var imgVwSaveFilter: UIImageView!
    @IBOutlet weak var imgVwEffectFilter: UIImageView!
    @IBOutlet weak var imgVwFlareFilter: UIImageView!
    @IBOutlet weak var imgVwDustFilter: UIImageView!
    @IBOutlet weak var imgVwColorFilter: UIImageView!
    @IBOutlet weak var imgVwEditFilter: UIImageView!
    @IBOutlet weak var imgVwPresetFilter: UIImageView!
    @IBOutlet weak var colVwFilterList: UICollectionView!
    @IBOutlet weak var vwFilterList: UIView!
    @IBOutlet weak var imgVwEditing: UIImageView!
    @IBOutlet weak var vwPlayVideo: UIView!
    @IBOutlet weak var cnstHeightVwBottomTabbar: NSLayoutConstraint!
    @IBOutlet weak var btnPresetFilterClicked: UIButton!
    @IBOutlet weak var vwBottomTabbar: UIView! //70
    //MARK:- Edit Filter Outlets
    @IBOutlet weak var vwTopNavigation: UIView!
    @IBOutlet weak var vwTopNaviEditFilter: UIView!
    @IBOutlet weak var cnstHeightVwSlidingEditFilter: NSLayoutConstraint!
    @IBOutlet weak var vwSlidingEditFilter: UIView!
    @IBOutlet weak var cnstHeightVwFilterList: NSLayoutConstraint!
    @IBOutlet weak var lblEditFilterValue: UILabel!
    @IBOutlet weak var lblEditFilterName: UILabel!
    @IBOutlet weak var sliderEditingFilter: UISlider!
    //MARK:- Color Filter Outlets
    @IBOutlet weak var vwColorFilter: UIView!
    @IBOutlet weak var cnstHeightVwColorFilter: NSLayoutConstraint!//265
    @IBOutlet weak var lblHueColorFilterSliderValue: UILabel!
    @IBOutlet weak var lblSaturationColorFilterSliderValue: UILabel!
    @IBOutlet weak var lblLuminanceColorFilterSliderValue: UILabel!
    @IBOutlet weak var colVwColorFilter: UICollectionView!
    @IBOutlet weak var sliderColorHue: UISlider!
    @IBOutlet weak var sliderColorSaturation: UISlider!
    @IBOutlet weak var sliderColorLuminance: UISlider!
    //MARK:- Save Filter Outlets
    @IBOutlet weak var vwSaveFilter: UIView!
    @IBOutlet weak var cnstHeightVwSaveFilter: NSLayoutConstraint!//263
    @IBOutlet weak var tblVwSaveFilter: UITableView!
    @IBOutlet weak var cnstHeightVwSubscribePreset: NSLayoutConstraint!//45
    @IBOutlet weak var vwSubscribePreset: UIView!
    //MARK:- Video Detail Outlets
    @IBOutlet weak var vwVideoDetail: UIView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var lblTotalDuration: UILabel!
    @IBOutlet weak var lblStartDuration: UILabel!
    @IBOutlet weak var sliderVideoPlay: UISlider!
    @IBOutlet weak var cnstHeightVwVideoDetail: NSLayoutConstraint!
    @IBOutlet weak var sliderDustFlareIntensity: UISlider!
    @IBOutlet weak var lblDustFlareIntensity: UILabel!
    @IBOutlet weak var vwDustFlareIntensityDetail: UIView!
    @IBOutlet weak var vwDustOnBottom: UIView!
    @IBOutlet weak var vwFlareOnBottom: UIView!
    @IBOutlet weak var vwEffectsOnBottom: UIView!
    
    //MARK:- View Model Object
    var objEditingVM = EditingVM()
    
    //MARK:- Variables
    var customFilter: CIFilter!
    var context = CIContext()
    var outputImage = CIImage()
    var inputImage = CIImage()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Proxy.shared.setStatusBarColor(.default)
        sliderEditingFilter.setCustomThumb(thumbRadius: sliderRedius)
        sliderDustFlareIntensity.setCustomThumb(thumbRadius: sliderRedius)
        sliderVideoPlay.setCustomThumb(thumbRadius: sliderRedius)
        sliderColorHue.setCustomThumb(thumbRadius: sliderRedius)
        sliderColorSaturation.setCustomThumb(thumbRadius: sliderRedius)
        sliderColorLuminance.setCustomThumb(thumbRadius: sliderRedius)
        if objEditingVM.objImageVideoModel.isVideoSelected {
            activevideoFilter(-1)
            vwDustOnBottom.isHidden = true
            vwFlareOnBottom.isHidden = true
            vwEffectsOnBottom.isHidden = true
            vwVideoDetail.isHidden = false
            cnstHeightVwVideoDetail.constant = 50
            imgVwEditing.isHidden = true
            vwPlayVideo.isHidden = false
            palyVideoMethod(objEditingVM.objImageVideoModel.videoUrl!)
        } else {
            vwVideoDetail.isHidden = true
            cnstHeightVwVideoDetail.constant = 0
            imgVwEditing.isHidden = false
            vwPlayVideo.isHidden = true
            imgVwEditing.image = objEditingVM.objImageVideoModel.imageSelect
            objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
            resetAllPhoto()
        }
    }
    
    //MARK:- IBActions
    @IBAction func actionBtnUndo(_ sender: Any) {
        self.reloadCurrentViewController()
        objEditingVM.objSaveMyFilterModel = SaveMyFilterModel()
        if !objEditingVM.objImageVideoModel.isVideoSelected {
            objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
            resetAllPhoto()
        } else {
            playNormalVideoWithoutFilter(fileURL: objEditingVM.objImageVideoModel.videoUrl!)
            activevideoFilter(-1)
        }
        objEditingVM.selectFilterType = BottomFilters.Presets
        actionBtnBottomFilter(btnPresetFilterClicked)
    }
    
    @IBAction func actionBtnDustFlareIntensity(_ sender: Any) {
        vwDustFlareIntensityDetail.isHidden = true
    }
    
    @IBAction func sliderDustFlareIntensityUpdate(_ sender: UISlider) {
        lblDustFlareIntensity.text = (sender.value).setDecimalValue(2)
        if objEditingVM.selectFilterType == BottomFilters.Dust {
            objEditingVM.originalImage.processImage()
            let lookupFilterImg = UIImage(named: objEditingVM.arrayDustFilter[objEditingVM.selectedIndex])
            objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg?.withAlpha(CGFloat(sender.value)))
            objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
            objEditingVM.blendFilter.useNextFrameForImageCapture()
            objEditingVM.filterImage.processImage {
                let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                DispatchQueue.main.async {
                    self.imgVwEditing.image = image
                    self.objEditingVM.imgDustFilterApply = image!
                    self.objEditingVM.objSaveMyFilterModel.dustAlphaVal = Double(sender.value)
                }
            }
        } else if objEditingVM.selectFilterType == BottomFilters.Flare {
            objEditingVM.originalImage.processImage()
            let lookupFilterImg = UIImage(named: objEditingVM.arrayFlareFilter[objEditingVM.selectedIndex])
            objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg?.withAlpha(CGFloat(sender.value)))
            objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
            objEditingVM.blendFilter.useNextFrameForImageCapture()
            objEditingVM.filterImage.processImage {
                let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                DispatchQueue.main.async {
                    self.imgVwEditing.image = image
                    self.objEditingVM.imgFlareFilterApply = image!
                    self.objEditingVM.objSaveMyFilterModel.flareAlphaVal = Double(sender.value)
                }
            }
        } else if objEditingVM.selectFilterType == BottomFilters.Effects {
            objEditingVM.originalImage.processImage()
            let lookupFilterImg = UIImage(named: objEditingVM.arrayEffectsFilter[objEditingVM.selectedIndex])
            objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg?.withAlpha(CGFloat(sender.value)))
            objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
            objEditingVM.blendFilter.useNextFrameForImageCapture()
            objEditingVM.filterImage.processImage {
                let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                DispatchQueue.main.async {
                    self.imgVwEditing.image = image
                    self.objEditingVM.imgEffectsFilterApply = image!
                    self.objEditingVM.objSaveMyFilterModel.effectAlphaVal = Double(sender.value)
                }
            }
        } else if objEditingVM.selectFilterType == BottomFilters.Presets {
            objEditingVM.originalImage.processImage()
            let lookupFilterImg = UIImage(named: objEditingVM.filtersList[objEditingVM.selectedIndex].1)
            objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg)
            objEditingVM.filterImage.addTarget(objEditingVM.lookupFilterObj,atTextureLocation: 1)
            objEditingVM.lookupFilterObj.useNextFrameForImageCapture()
            objEditingVM.lookupFilterObj.intensity = CGFloat(sender.value)
            objEditingVM.filterImage.processImage {
                let image = self.objEditingVM.lookupFilterObj.imageFromCurrentFramebuffer()
                DispatchQueue.main.async {
                    self.imgVwEditing.image = image
                    self.objEditingVM.imgPresetFilterApply = image!
                }
            }
        }
    }
    
    @IBAction func actionBtnSave(_ sender: Any) {
        if !objEditingVM.objImageVideoModel.isVideoSelected {
            objEditingVM.objImageVideoModel.imageSelect = imgVwEditing.image!.fixedOrientation()
            objSaveDataHomeScreenDelegate?.saveDataInDatabase(objEditingVM.objImageVideoModel, selectIndex: objEditingVM.reEditItemIndex)
            self.displayAlertMessageWithOk(AppAlerts.titleValue.imageSaved) {
                self.popViewController(true)
            }
        } else {
            Proxy.shared.showActivityIndicator()
            saveVideoAfterFilterInHomeAndShare {
                Proxy.shared.hideActivityIndicator()
                self.objEditingVM.objImageVideoModel.videoUrl = self.objEditingVM.objVideoFilterModel.videoUrlFiltered
                self.objEditingVM.objImageVideoModel.videoThumbhImage = self.objEditingVM.objVideoFilterModel.thumbVideoImage
                objSaveDataHomeScreenDelegate?.saveDataInDatabase(self.objEditingVM.objImageVideoModel, selectIndex: self.objEditingVM.reEditItemIndex)
                DispatchQueue.main.async {
                    self.displayAlertMessageWithOk(AppAlerts.titleValue.videoSaved) {
                        self.removeNotificationObserver()
                        self.popViewController(true)
                    }
                }
            }
        }
    }
    
    @IBAction func actionBtnShare(_ sender: Any) {
        objSaveAndShareDelegate = self
        self.presentViewController("SaveAndSharePopUpVC", isAnimation: true)
    }
    
    @IBAction func actionBtnCancelEditFilter(_ sender: Any) {
        cnstHeightVwSlidingEditFilter.constant = 0
        vwSlidingEditFilter.isHidden = true
        vwFilterList.isHidden = false
        if objEditingVM.selectFilterType == BottomFilters.Edit {
            cnstHeightVwFilterList.constant = 60
        } else {
            cnstHeightVwFilterList.constant = 110
        }
        vwTopNavigation.isHidden = false
        vwTopNaviEditFilter.isHidden = true
        vwBottomTabbar.isHidden = false
        cnstHeightVwBottomTabbar.constant = 70
        if !objEditingVM.objImageVideoModel.isVideoSelected {
            imgVwEditing.image = objEditingVM.previewImageFilter
            objEditingVM.imgEditFilterApply = objEditingVM.previewImageFilter
        } else {
            objEditingVM.objVideoFilterModel.isEditVideoApply = false
            playNormalVideoWithoutFilter(fileURL: objEditingVM.objImageVideoModel.videoUrl!)
        }
    }
    
    @IBAction func actionBtnSaveEditFilter(_ sender: Any) {
        cnstHeightVwSlidingEditFilter.constant = 0
        vwSlidingEditFilter.isHidden = true
        vwFilterList.isHidden = false
        if objEditingVM.selectFilterType == BottomFilters.Edit {
            cnstHeightVwFilterList.constant = 60
        } else {
            cnstHeightVwFilterList.constant = 110
        }
        vwTopNavigation.isHidden = false
        vwTopNaviEditFilter.isHidden = true
        vwBottomTabbar.isHidden = false
        cnstHeightVwBottomTabbar.constant = 70
    }
    
    @IBAction func actionBtnBack(_ sender: Any) {
        removeNotificationObserver()
        self.popViewController(true)
    }
    
    @IBAction func actionBtnBottomFilter(_ sender: UIButton) {
        objEditingVM.selectFilterType = sender.tag
        setTabbarMethod(selectedIndex: sender.tag)
        vwSubscribePreset.isHidden = true
        vwDustFlareIntensityDetail.isHidden = true
        cnstHeightVwSubscribePreset.constant = 0
        if objEditingVM.selectFilterType == BottomFilters.Color {
            objEditingVM.selectedIndex = -1
            vwFilterList.isHidden = true
            cnstHeightVwFilterList.constant = 0
            vwSaveFilter.isHidden = true
            cnstHeightVwSaveFilter.constant = 0
            vwColorFilter.isHidden = false
            cnstHeightVwColorFilter.constant = 215
            objEditingVM.arrayColorListVal.removeAll()
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                imgVwEditing.image!.getColors { colors in
                    self.objEditingVM.arrayColorListVal.append((colors!.primary, colors!.primary.withAlphaComponent(0.3)))
                    self.objEditingVM.arrayColorListVal.append((colors!.secondary, colors!.secondary.withAlphaComponent(0.3)))
                    self.objEditingVM.arrayColorListVal.append((colors!.detail, colors!.detail.withAlphaComponent(0.3)))
                    self.objEditingVM.arrayColorListVal.append((colors!.background, colors!.background.withAlphaComponent(0.3)))
                    self.colVwColorFilter.delegate = self
                    self.colVwColorFilter.dataSource = self
                    self.colVwColorFilter.reloadData()
                }
            } else {
                colVwColorFilter.delegate = self
                colVwColorFilter.dataSource = self
                colVwColorFilter.reloadData()
            }
        } else if objEditingVM.selectFilterType == BottomFilters.Save {
            cnstHeightVwFilterList.constant = 0
            vwFilterList.isHidden = true
            vwColorFilter.isHidden = true
            cnstHeightVwColorFilter.constant = 0
            vwSaveFilter.isHidden = false
            cnstHeightVwSaveFilter.constant = 263
            tblVwSaveFilter.delegate = self
            tblVwSaveFilter.dataSource = self
            tblVwSaveFilter.reloadData()
        } else {
            colVwColorFilter.collectionViewLayout.invalidateLayout()
            if objEditingVM.selectFilterType == BottomFilters.Edit {
                cnstHeightVwFilterList.constant = 60
            } else {
                cnstHeightVwFilterList.constant = 110
            }
            vwFilterList.isHidden = false
            vwColorFilter.isHidden = true
            cnstHeightVwColorFilter.constant = 0
            vwSaveFilter.isHidden = true
            cnstHeightVwSaveFilter.constant = 0
            objEditingVM.selectedIndex = 0
            colVwFilterList.reloadData()
            colVwFilterList.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: 95, height: 100)
            colVwFilterList.collectionViewLayout = layout
        }
    }
    
    @IBAction func SliderValueUpdateEditFilter(_ sender: UISlider) {
        lblEditFilterValue.text = "\(sender.value.setDecimalValue(2))"
        if objEditingVM.objImageVideoModel.isVideoSelected {
            objEditingVM.objVideoFilterModel.filterValEdit = CGFloat(sender.value)
            applyVideoFilterMethod(filterClick: BottomFilters.Edit, fileURL: objEditingVM.objImageVideoModel.videoUrl!, isSlide: true)
        } else {
            objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
            self.applyEditFilterMethod(filterType: objEditingVM.arrayEditFilter[objEditingVM.selectEditIndex].0, sliderValue: CGFloat(sender.value), isVideoSlider: false)
        }
    }
    
    //MARK:- Color Filter Value Update Of Slider
    @IBAction func colorFilterValueUpdate(_ sender: UISlider) {
        switch sender.tag {
        case 1:
            lblHueColorFilterSliderValue.text = sender.value.setDecimalValue(2)
            objEditingVM.hueColorFilter = CGFloat(sender.value/255)
        case 2:
            lblSaturationColorFilterSliderValue.text = sender.value.setDecimalValue(2)
            objEditingVM.saturationColor = CGFloat(sender.value)
        default:
            lblLuminanceColorFilterSliderValue.text = sender.value.setDecimalValue(2)
            objEditingVM.luminanceColorFilter = CGFloat(sender.value)
        }
        if !objEditingVM.objImageVideoModel.isVideoSelected {
            if self.objEditingVM.selectedIndex != -1 {
                self.objEditingVM.objSaveMyFilterModel.colorHue = Double(self.objEditingVM.hueColorFilter)
                self.objEditingVM.objSaveMyFilterModel.colorSaturation = Double(self.objEditingVM.saturationColor)
                self.objEditingVM.objSaveMyFilterModel.colorLuminance = Double(self.objEditingVM.luminanceColorFilter)
                DispatchQueue.main.async {
                    switch self.objEditingVM.selectedIndex {
                    case 0:
                        self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: self.objEditingVM.hueColorFilter, y: self.objEditingVM.saturationColor, z: self.objEditingVM.luminanceColorFilter)
                    case 1:
                        self.objEditingVM.objMultiBandHSV.inputOrangeShift = CIVector(x: self.objEditingVM.hueColorFilter, y: self.objEditingVM.saturationColor, z: self.objEditingVM.luminanceColorFilter)
                    case 2:
                        self.objEditingVM.objMultiBandHSV.inputYellowShift = CIVector(x: self.objEditingVM.hueColorFilter, y: self.objEditingVM.saturationColor, z: self.objEditingVM.luminanceColorFilter)
                    case 3:
                        self.objEditingVM.objMultiBandHSV.inputGreenShift = CIVector(x: self.objEditingVM.hueColorFilter, y: self.objEditingVM.saturationColor, z: self.objEditingVM.luminanceColorFilter)
                    default:
                        self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: self.objEditingVM.hueColorFilter, y: self.objEditingVM.saturationColor, z: self.objEditingVM.luminanceColorFilter)
                    }
                    let imageVal = UIImage(ciImage: self.objEditingVM.objMultiBandHSV.outputImage!)
                    self.imgVwEditing.image = imageVal
                    self.objEditingVM.imgColorFilterApply = imageVal
                }
            }
        } else {
            objEditingVM.objVideoFilterModel.colorHue = objEditingVM.hueColorFilter
            objEditingVM.objVideoFilterModel.colorSaturation = objEditingVM.saturationColor
            objEditingVM.objVideoFilterModel.colorLuminance = objEditingVM.luminanceColorFilter
            applyVideoFilterMethod(filterClick: BottomFilters.Color, fileURL: objEditingVM.objImageVideoModel.videoUrl!, isSlide: true)
        }
    }
    
    @IBAction func actionBtnSaveFilterName(_ sender: Any) {
        if !self.objEditingVM.objImageVideoModel.isVideoSelected {
            objEditingVM.saveMyFilterSelected = -1
            showAlertWithTextField()
        } else {
            objEditingVM.saveMyFilterSelected = -1
            showAlertWithTextField()
        }
    }
    
    @IBAction func actionBtnSubscribePreset(_ sender: Any) {
        self.presentViewController("SubcribePlanVC", isAnimation: true)
    }
    
    @IBAction func actionBtnDeleteFilterName(_ sender: Any) {
        if arraySaveMyFilter.count > 0 {
            if objEditingVM.saveMyFilterSelected != -1 {
                deleteSaveFilterFromDatabase(objDetailMedia: arraySaveMyFilter[objEditingVM.saveMyFilterSelected])
                arraySaveMyFilter.remove(at: objEditingVM.saveMyFilterSelected)
                objEditingVM.saveMyFilterSelected = -1
                tblVwSaveFilter.reloadData()
            }
        } else {
            self.displayAlertMessage(AppAlerts.titleValue.imageNotFount)
        }
    }
    
    @IBAction func videoPaySliderValueUpdate(_ sender: UISlider) {
        let seconds : Int64 = Int64(sender.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        objEditingVM.avPlayerController.player!.seek(to: targetTime)
        objEditingVM.avPlayerController.player!.play()
        btnPlayVideo.isSelected = true
    }
    
    @IBAction func actionBtnPayVideo(_ sender: Any) {
        if objEditingVM.avPlayerController.player!.rate == 0 {
            objEditingVM.avPlayerController.player!.play()
            btnPlayVideo.isSelected = true
            objEditingVM.avPlayerController.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
                if self.objEditingVM.avPlayerController.player!.currentItem?.status == .readyToPlay {
                    let time : Float64 = CMTimeGetSeconds(self.objEditingVM.avPlayerController.player!.currentTime())
                    self.sliderVideoPlay.value = Float(time)
                    self.lblStartDuration.text = self.string(from: time)
                    self.sliderVideoPlay.isContinuous = false
                }
            }
        } else {
            objEditingVM.avPlayerController.player!.pause()
            btnPlayVideo.isSelected = false
        }
    }
    
    //MARK:- Enter Preser Name Method
    func showAlertWithTextField() {
        let alertController = UIAlertController(title: AppAlerts.titleValue.newPresent, message: AppAlerts.titleValue.presetNameEnter, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: AppAlerts.titleValue.ok, style: .default) { (_) in
            if let txtField = alertController.textFields?.first, let text = txtField.text {
                if text != "" {
                    if !self.objEditingVM.objImageVideoModel.isVideoSelected {
                        let objDetail = SaveMyFilterModel()
                        objDetail.isVideofilterApply = false
                        objDetail.isSelected = self.objEditingVM.objSaveMyFilterModel.isSelected
                        
                        objDetail.isPresetApply = self.objEditingVM.objSaveMyFilterModel.isPresetApply
                        objDetail.presetName = self.objEditingVM.objSaveMyFilterModel.presetName
                        
                        objDetail.editFilerName = self.objEditingVM.objSaveMyFilterModel.editFilerName
                        objDetail.editFilterValue = self.objEditingVM.objSaveMyFilterModel.editFilterValue
                        objDetail.isEditFilterApply = self.objEditingVM.objSaveMyFilterModel.isEditFilterApply
                        objDetail.filterInputkeyEdit = self.objEditingVM.objSaveMyFilterModel.filterInputkeyEdit
                        objDetail.filterKeyEdit = self.objEditingVM.objSaveMyFilterModel.filterKeyEdit
                        
                        objDetail.isColorFilterApply = self.objEditingVM.objSaveMyFilterModel.isColorFilterApply
                        objDetail.colorIndex = self.objEditingVM.objSaveMyFilterModel.colorIndex
                        objDetail.colorHue = self.objEditingVM.objSaveMyFilterModel.colorHue
                        objDetail.colorSaturation = self.objEditingVM.objSaveMyFilterModel.colorSaturation
                        objDetail.colorLuminance = self.objEditingVM.objSaveMyFilterModel.colorLuminance
                        
                        objDetail.dustFilterImageInString = self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString
                        objDetail.isDustFilterApply = self.objEditingVM.objSaveMyFilterModel.isDustFilterApply
                        objDetail.dustAlphaVal = self.objEditingVM.objSaveMyFilterModel.dustAlphaVal
                        
                        objDetail.flareFilterImageInString = self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString
                        objDetail.isFlareFilterApply = self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply
                        objDetail.flareAlphaVal = self.objEditingVM.objSaveMyFilterModel.flareAlphaVal
                        
                        objDetail.effectsFilterImageInString = self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString
                        objDetail.isEffectsFilterApply = self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply
                        objDetail.effectAlphaVal = self.objEditingVM.objSaveMyFilterModel.effectAlphaVal
                        
                        objDetail.filterImageInString = filterDummyImage
                        objDetail.filterName = text
                        arraySaveMyFilter.append(objDetail)
                        self.saveFiltersDataInDB(objDetailMedia: objDetail)
                        self.tblVwSaveFilter.reloadData()
                    } else {
                        let objVideoDetail = SaveMyFilterModel()
                        objVideoDetail.isVideofilterApply = true
                        objVideoDetail.isSelected = false
                        
                        objVideoDetail.isPresetApply = self.objEditingVM.objVideoFilterModel.isPresetVideoApply
                        objVideoDetail.presetName = self.objEditingVM.objVideoFilterModel.presetFilterName
                        
                        objVideoDetail.editFilerName = self.objEditingVM.objVideoFilterModel.filterNameEdit
                        objVideoDetail.editFilterValue = Double(self.objEditingVM.objVideoFilterModel.filterValEdit)
                        objVideoDetail.isEditFilterApply = self.objEditingVM.objVideoFilterModel.isEffectVideoApply
                        objVideoDetail.filterKeyEdit = self.objEditingVM.objVideoFilterModel.filterKeyEdit
                        objVideoDetail.filterInputkeyEdit = self.objEditingVM.objVideoFilterModel.filterInputkeyEdit
                        
                        objVideoDetail.isColorFilterApply = self.objEditingVM.objVideoFilterModel.isColorVideoApply
                        objVideoDetail.colorIndex = self.objEditingVM.objVideoFilterModel.colorApplyIndex
                        objVideoDetail.colorHue = Double(self.objEditingVM.objVideoFilterModel.colorHue)
                        objVideoDetail.colorSaturation = Double(self.objEditingVM.objVideoFilterModel.colorSaturation)
                        objVideoDetail.colorLuminance = Double(self.objEditingVM.objVideoFilterModel.colorLuminance)
                        
                        objVideoDetail.dustFilterImageInString = ""
                        objVideoDetail.isDustFilterApply = false
                        objVideoDetail.dustAlphaVal = 0.0
                        
                        objVideoDetail.flareFilterImageInString = ""
                        objVideoDetail.isFlareFilterApply = false
                        objVideoDetail.flareAlphaVal = 0.0
                        
                        objVideoDetail.effectsFilterImageInString = ""
                        objVideoDetail.isEffectsFilterApply = false
                        objVideoDetail.effectAlphaVal = 0.0
                        
                        objVideoDetail.filterImageInString = filterDummyImage
                        objVideoDetail.filterName = text
                        arraySaveMyFilter.append(objVideoDetail)
                        self.saveVideoFiltersDataInDB(objDetailMedia: objVideoDetail)
                        self.tblVwSaveFilter.reloadData()
                    }
                } else {
                    self.tblVwSaveFilter.reloadData()
                }
            }
        }
        let cancelAction = UIAlertAction(title: AppAlerts.titleValue.cancel, style: .cancel) { (_) in
            self.tblVwSaveFilter.reloadData()
        }
        alertController.addTextField { (textField) in
            textField.keyboardType = .asciiCapable
            textField.returnKeyType = .done
            textField.becomeFirstResponder()
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
extension EditingVC {
    
    //MARK:- Set Tabbar Setting and Selection
    func setTabbarMethod(selectedIndex: Int) {
        switch selectedIndex {
        case BottomFilters.Presets:
            setTabbarLabelColor(selectedLabel: lblPresetFilter)
            setTabbarImage(selectedImageView: imgVwPresetFilter, selectedImage: #imageLiteral(resourceName: "home-3"))
        case BottomFilters.Edit:
            setTabbarLabelColor(selectedLabel: lblEditFilter)
            setTabbarImage(selectedImageView: imgVwEditFilter, selectedImage: #imageLiteral(resourceName: "edit"))
        case BottomFilters.Color:
            setTabbarLabelColor(selectedLabel: lblColorFilter)
            setTabbarImage(selectedImageView: imgVwColorFilter, selectedImage: #imageLiteral(resourceName: "color"))
        case BottomFilters.Dust:
            setTabbarLabelColor(selectedLabel: lblDustFilter)
            setTabbarImage(selectedImageView: imgVwDustFilter, selectedImage: #imageLiteral(resourceName: "dust"))
        case BottomFilters.Flare:
            setTabbarLabelColor(selectedLabel: lblFlareFilter)
            setTabbarImage(selectedImageView: imgVwFlareFilter, selectedImage: #imageLiteral(resourceName: "flare"))
        case BottomFilters.Effects:
            setTabbarLabelColor(selectedLabel: lblEffectsFilter)
            setTabbarImage(selectedImageView: imgVwEffectFilter, selectedImage: #imageLiteral(resourceName: "effects_filter_selected"))
        case BottomFilters.Save:
            setTabbarLabelColor(selectedLabel: lblSaveFilter)
            setTabbarImage(selectedImageView: imgVwSaveFilter, selectedImage: #imageLiteral(resourceName: "open-menu-orange"))
        default:
            break
        }
    }
    
    //MARK:- Set Tabbar Label Color
    func setTabbarLabelColor(selectedLabel: UILabel) {
        lblPresetFilter.textColor = .black
        lblEditFilter.textColor = .black
        lblColorFilter.textColor = .black
        lblDustFilter.textColor = .black
        lblFlareFilter.textColor = .black
        lblEffectsFilter.textColor = .black
        lblSaveFilter.textColor = .black
        selectedLabel.textColor = Color.AppColor
    }
    
    //MARK:- Set Tabbar Icon Image
    func setTabbarImage(selectedImageView: UIImageView,selectedImage: UIImage) {
        imgVwPresetFilter.image = #imageLiteral(resourceName: "home")
        imgVwEditFilter.image = #imageLiteral(resourceName: "edit-black")
        imgVwColorFilter.image = #imageLiteral(resourceName: "color-black")
        imgVwDustFilter.image = #imageLiteral(resourceName: "dust-black")
        imgVwFlareFilter.image = #imageLiteral(resourceName: "flare-black")
        imgVwEffectFilter.image = #imageLiteral(resourceName: "effects_filter")
        imgVwSaveFilter.image = #imageLiteral(resourceName: "open-menu")
        selectedImageView.image = selectedImage
    }
    
    func resetAllPhoto() {
        objEditingVM.imgPresetFilterApply = objEditingVM.objImageVideoModel.imageSelect
        objEditingVM.imgEditFilterApply = objEditingVM.objImageVideoModel.imageSelect
        objEditingVM.imgDustFilterApply = objEditingVM.objImageVideoModel.imageSelect
        objEditingVM.imgFlareFilterApply = objEditingVM.objImageVideoModel.imageSelect
        objEditingVM.imgEffectsFilterApply = objEditingVM.objImageVideoModel.imageSelect
        objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
    }
    
}
