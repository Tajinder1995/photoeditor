//
//  HomeVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 14/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import AVKit
import GPUImage
import CoreData

class HomeVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var colVwPhotoList: UICollectionView!
    
    //MARK:- View Model Object
    var objHomeVM = HomeVM()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if arraySaveMyFilter.count == 0 && arraySelectImageVideo.count == 0 {
            appDelegate.clearAllFilesFromTempDirectory()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Proxy.shared.setStatusBarColor(.lightContent)
    }

    //MARK:- IBActions
    @IBAction func actionBtnLogin(_ sender: Any) {
        self.presentViewController("SignUpVC", isAnimation: true)
    }
    
    @IBAction func actionBtnOpenCamera(_ sender: Any) {
        cameraPermissionsMethod()
    }
    
    func cameraPermissionsMethod() {
        let cameraManager = CameraManager()
        cameraManager.askUserForCameraPermission { permissionGranted in
            if permissionGranted {
                objPassImageDelegate = self
                self.presentViewController("CustomCameraVC", isAnimation: true)
            } else {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }
        }
    }
    
    @IBAction func actionBtnOpenImageGallery(_ sender: Any) {
        objPassImageDelegate = self
        objHomeVM.objGalleryCameraImage.controller = self
        objHomeVM.objGalleryCameraImage.openGallary(image: true, video: false)
    }
    
    @IBAction func actionBtnOpenVideoGallery(_ sender: Any) {
        objPassImageDelegate = self
        objHomeVM.objGalleryCameraImage.controller = self
        objHomeVM.objGalleryCameraImage.openGallary(image: false,video: true)
    }
    
    @IBAction func actionBtnExport(_ sender: Any) {
        if arraySelectImageVideo.count > 0 {
            let selectValueArr = arraySelectImageVideo.filter({$0.isSelected == true}).map {($0)}
            if selectValueArr.count == 1 {
                objSaveAndShareDelegate = self
                objHomeVM.objSelectSaveExport = selectValueArr[0]
                self.presentViewController("SaveAndSharePopUpVC", isAnimation: true)
            } else {
                self.displayAlertMessage(AppAlerts.titleValue.selectOneItem)
            }
        } else {
            self.displayAlertMessage(AppAlerts.titleValue.imageVideoNotFount)
        }
    }
    
    @IBAction func actionBtnDelete(_ sender: Any) {
        if arraySelectImageVideo.count > 0 {
            removeMediaFromDatabase(arraySelectImageVideo.filter( {$0.isSelected == true }).map({ return $0 }))
            arraySelectImageVideo.removeAll { (objDetail) -> Bool in
                return objDetail.isSelected == true
            }
            colVwPhotoList.reloadData()
        } else {
            self.displayAlertMessage(AppAlerts.titleValue.imageVideoNotFount)
        }
    }
    
    @IBAction func actionBtnEditImageVideo(_ sender: UIButton) {
        Proxy.shared.showActivityIndicator()
        if arraySelectImageVideo.count > 0 {
            let selectValueArr = arraySelectImageVideo.filter({$0.isSelected == true}).map {($0)}
            if selectValueArr.count == 1 {
                sender.isUserInteractionEnabled = false
                let index = arraySelectImageVideo.firstIndex(where: { $0.isSelected == true })
                objSaveDataHomeScreenDelegate = self
                let objController = storyboardObj.instantiateViewController(withIdentifier: "EditingVC") as! EditingVC
                objController.objEditingVM.reEditItemIndex = index!
                objController.objEditingVM.objImageVideoModel = selectValueArr[0]
                if !selectValueArr[0].isVideoSelected {
                    self.convertLutfileInImages(selectValueArr[0].imageSelect) { (arrayFilterListing) in
                        objController.objEditingVM.filtersList = arrayFilterListing
                        Proxy.shared.hideActivityIndicator()
                        sender.isUserInteractionEnabled = true
                        self.navigationController?.pushViewController(objController, animated: true)
                    }
                } else {
                    self.convertLutfileInImages(selectValueArr[0].videoThumbhImage) { (arrayFilterListing) in
                        objController.objEditingVM.filtersList = arrayFilterListing
                        Proxy.shared.hideActivityIndicator()
                        sender.isUserInteractionEnabled = true
                        self.navigationController?.pushViewController(objController, animated: true)
                    }
                }
            } else {
                Proxy.shared.hideActivityIndicator()
                self.displayAlertMessage(AppAlerts.titleValue.selectOneItem)
            }
        } else {
            Proxy.shared.hideActivityIndicator()
            self.displayAlertMessage(AppAlerts.titleValue.imageVideoNotFount)
        }
    }
    
    //MARK:- Convert Lut file In Images Method
    func convertLutfileInImages(_ imageSelect: UIImage,completion:@escaping(_ array:[(UIImage,String)]) -> Void) {
        var arrayFilers = [(UIImage,String)]()
        for i in 0..<objHomeVM.arrayFiltersList.count {
            arrayFilers.append((imageSelect, objHomeVM.arrayFiltersList[i]))
//            if i == 0 {
//                arrayFilers.append((imageSelect, objHomeVM.arrayFiltersList[i]))
//            } else {
//                objHomeVM.originalImage.removeAllTargets()
//                objHomeVM.filterImage.removeAllTargets()
//                objHomeVM.lookupFilterObj = GPUImageLookupFilter()
//                objHomeVM.originalImage = GPUImagePicture(image: imageSelect)
//                objHomeVM.originalImage.addTarget(objHomeVM.lookupFilterObj,atTextureLocation: 0)
//                objHomeVM.originalImage.processImage()
//                let lookupFilterImg  = UIImage(named: "\(objHomeVM.arrayFiltersList[i])")
//                objHomeVM.filterImage = GPUImagePicture(image: lookupFilterImg)
//                objHomeVM.filterImage.addTarget(objHomeVM.lookupFilterObj,atTextureLocation: 1)
//                objHomeVM.filterImage.useNextFrameForImageCapture()
//                objHomeVM.filterImage.processImage {
//                    DispatchQueue.main.async {
//                        let imageObj = self.objHomeVM.lookupFilterObj.imageFromCurrentFramebuffer()
//                        arrayFilers.append(((imageObj ?? imageSelect) as UIImage, self.objHomeVM.arrayFiltersList[i]))
//                        if arrayFilers.count == self.objHomeVM.arrayFiltersList.count {
//                            completion(arrayFilers)
//                        }
//                    }
//                }
//            }
        }
        completion(arrayFilers)
    }
    
}
