//
//  SaveAndSharePopUpVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 19/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class SaveAndSharePopUpVC: UIViewController {

    //MARK:- View Model Object
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- IBActions
    @IBAction func actionBtnShare(_ sender: Any) {
        self.dismiss(animated: true) {
            objSaveAndShareDelegate?.SaveItemInGallery(false)
        }
    }
    
    @IBAction func actionBtnCross(_ sender: Any) {
        self.dissmisViewController(true)
    }
    
    @IBAction func actionBtnSave(_ sender: Any) {
        self.dismiss(animated: true) {
            objSaveAndShareDelegate?.SaveItemInGallery(true)
        }
    }
}
