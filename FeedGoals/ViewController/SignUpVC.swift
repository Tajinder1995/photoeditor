//
//  SignUpVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 15/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseCore
import FirebaseDatabase

class SignUpVC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var txtFldEmail: UITextField!
    
    var ref : DatabaseReference!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- IBActions
    @IBAction func actionBtnCross(_ sender: Any) {
        self.dissmisViewController(true)
    }
    
    @IBAction func actionBtnSignUp(_ sender: Any) {
        if txtFldEmail.isBlank {
            self.displayAlertMessage(AppAlerts.titleValue.emailEmpty)
        } else if !Proxy.shared.isValidEmail(txtFldEmail.text!) {
            self.displayAlertMessage(AppAlerts.titleValue.emailNotValid)
        } else {
            saveDataInFirebase()
        }
    }
    
    func saveDataInFirebase() {
        ref = Database.database().reference()
        let currentTimeStamp = String(Date().currentTimeStamp)
        let dict = [
            "Email":txtFldEmail.text!
            ] as [String : Any]
        ref.child("NewsLetterEmail").child(deviceId).child(currentTimeStamp).setValue(dict)
        self.dissmisViewController(true)
    }
}
