//
//  SubcribePlanVC.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 17/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit

class SubcribePlanVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var lblYearlyPlanDescription: UILabel!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- IBActions
    @IBAction func actionBtnCross(_ sender: Any) {
        self.dissmisViewController(true)
    }
    
    @IBAction func actionBtnScribeNow(_ sender: Any) {
    }
    
    @IBAction func actionBtnSubscribeMonthly(_ sender: Any) {
    }
    
    @IBAction func actionBtnSubscribeYearly(_ sender: Any) {
    }
}
