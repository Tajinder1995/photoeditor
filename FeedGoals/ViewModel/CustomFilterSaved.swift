//
//  CustomFilterSaved.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 03/08/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import GPUImage
import Firebase
import FirebaseAnalytics
import FirebaseDatabase
import CoreData

extension EditingVC {
    
    func applySaveFilterMethod(_ imageObj: UIImage,filterCompletion:@escaping(_ image: UIImage) -> Void) {
        if objEditingVM.objSaveMyFilterModel.isPresetApply {
            applyPresetFilterOnImage(objEditingVM.objSaveMyFilterModel.presetName,normalImage: imageObj) { (image) in
                
                if self.objEditingVM.objSaveMyFilterModel.isEditFilterApply {
                    self.objEditingVM.originalImage.removeAllTargets()
                    self.objEditingVM.originalImage = GPUImagePicture(image: image)
                    self.getEditFilterAfterSaveFilter(filterType: self.objEditingVM.objSaveMyFilterModel.editFilerName, sliderValue: CGFloat(self.objEditingVM.objSaveMyFilterModel.editFilterValue), normalImg: image) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isColorFilterApply {
                            self.applyColorFilterOnImage(normalImg: image) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                                        
                                        if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                                
                                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                                        filterCompletion(image)
                                                    }
                                                } else {
                                                    filterCompletion(image)
                                                }
                                            }
                                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                                filterCompletion(image)
                                            }
                                        } else {
                                            filterCompletion(image)
                                        }
                                    }
                                } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                        
                                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                                filterCompletion(image)
                                            }
                                        } else {
                                            filterCompletion(image)
                                        }
                                    }
                                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                        
                                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                                filterCompletion(image)
                                            }
                                        } else {
                                            filterCompletion(image)
                                        }
                                    }
                                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isColorFilterApply {
                    self.applyColorFilterOnImage(normalImg: image) { (image) in
                        if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                        
                                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                                filterCompletion(image)
                                            }
                                        } else {
                                            filterCompletion(image)
                                        }
                                    }
                                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                        filterCompletion(image)
                    }
                } else {
                    filterCompletion(image)
                }
            }
        }
            
        else if self.objEditingVM.objSaveMyFilterModel.isEditFilterApply {
            self.objEditingVM.originalImage.removeAllTargets()
            self.objEditingVM.originalImage = GPUImagePicture(image: imageObj)
            self.getEditFilterAfterSaveFilter(filterType: self.objEditingVM.objSaveMyFilterModel.editFilerName, sliderValue: CGFloat(self.objEditingVM.objSaveMyFilterModel.editFilterValue), normalImg: imageObj) { (image) in
                
                if self.objEditingVM.objSaveMyFilterModel.isColorFilterApply {
                    self.applyColorFilterOnImage(normalImg: image) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                        
                                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                                filterCompletion(image)
                                            }
                                        } else {
                                            filterCompletion(image)
                                        }
                                    }
                                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                        filterCompletion(image)
                    }
                } else {
                    filterCompletion(image)
                }
            }
        }
            
        else if self.objEditingVM.objSaveMyFilterModel.isColorFilterApply {
            self.applyColorFilterOnImage(normalImg: imageObj) { (image) in
                
                if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: image, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                                
                                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                        filterCompletion(image)
                                    }
                                } else {
                                    filterCompletion(image)
                                }
                            }
                        } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                        filterCompletion(image)
                    }
                } else {
                    filterCompletion(image)
                }
            }
        }
            
        else if self.objEditingVM.objSaveMyFilterModel.isDustFilterApply {
            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString, normalImage: imageObj, filterType: OverlayFilterType.dust, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.dustAlphaVal) { (image) in
                
                if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: image, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                        
                        if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                                filterCompletion(image)
                            }
                        } else {
                            filterCompletion(image)
                        }
                    }
                } else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                        filterCompletion(image)
                    }
                } else {
                    filterCompletion(image)
                }
            }
        }
            
        else if self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply {
            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString, normalImage: imageObj, filterType: OverlayFilterType.flare, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.flareAlphaVal) { (image) in
                
                if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
                    self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: image, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                        filterCompletion(image)
                    }
                } else {
                    filterCompletion(image)
                }
            }
        }
            
        else if self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply {
            self.applyDustFlareEffectsFilterOnImage(self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString, normalImage: imageObj, filterType: OverlayFilterType.effects, filterAlphaVaue: self.objEditingVM.objSaveMyFilterModel.effectAlphaVal) { (image) in
                filterCompletion(image)
            }
        } else {
            filterCompletion(imageObj)
        }
    }
    
    func applyPresetFilterOnImage(_ lookupFilterImg: String,normalImage: UIImage,completion:@escaping(_ image: UIImage) -> Void) {
        objEditingVM.originalImage.removeAllTargets()
        objEditingVM.filterImage.removeAllTargets()
        objEditingVM.lookupFilterObj = GPUImageLookupFilter()
        objEditingVM.originalImage = GPUImagePicture(image: normalImage)
        objEditingVM.originalImage.addTarget(objEditingVM.lookupFilterObj,atTextureLocation: 0)
        objEditingVM.originalImage.processImage()
        let filterImage = UIImage(named: lookupFilterImg)
        objEditingVM.filterImage = GPUImagePicture(image: filterImage)
        objEditingVM.filterImage.addTarget(objEditingVM.lookupFilterObj,atTextureLocation: 1)
        objEditingVM.lookupFilterObj.useNextFrameForImageCapture()
        objEditingVM.lookupFilterObj.intensity = 1.1
        objEditingVM.filterImage.processImage {
            let image = self.objEditingVM.lookupFilterObj.imageFromCurrentFramebuffer()
            DispatchQueue.main.async {
                completion(image!)
            }
        }
    }
    
    func applyDustFlareEffectsFilterOnImage(_ effectImage: String,normalImage: UIImage,filterType: String,filterAlphaVaue: Double,completion:@escaping(_ image: UIImage) -> Void) {
        if filterType == OverlayFilterType.dust {
            objEditingVM.originalImage.removeAllTargets()
            objEditingVM.filterImage.removeAllTargets()
            objEditingVM.blendFilter = GPUImageLightenBlendFilter()
            objEditingVM.originalImage = GPUImagePicture(image: normalImage)
            objEditingVM.originalImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 0)
            objEditingVM.originalImage.processImage()
            let filterImage = UIImage(named: effectImage)
            objEditingVM.filterImage = GPUImagePicture(image: filterImage?.withAlpha(CGFloat(filterAlphaVaue)))
            objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
            objEditingVM.blendFilter.useNextFrameForImageCapture()
            objEditingVM.filterImage.processImage {
                let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                DispatchQueue.main.async {
                    completion(image!)
                }
            }
        } else {
            objEditingVM.originalImage.removeAllTargets()
            objEditingVM.filterImage.removeAllTargets()
            objEditingVM.blendFilter = GPUImageLightenBlendFilter()
            objEditingVM.originalImage = GPUImagePicture(image: normalImage)
            objEditingVM.originalImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 0)
            objEditingVM.originalImage.processImage()
            let filterImage = UIImage(named: effectImage)
            objEditingVM.filterImage = GPUImagePicture(image: filterImage?.withAlpha(CGFloat(filterAlphaVaue)))
            objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
            objEditingVM.blendFilter.useNextFrameForImageCapture()
            objEditingVM.filterImage.processImage {
                let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                DispatchQueue.main.async {
                    completion(image!)
                }
            }
        }
    }
    
    //MARK:- Apply Edit Filter On Video And Image
    func getEditFilterAfterSaveFilter(filterType: String,sliderValue:CGFloat,normalImg:UIImage,completion:@escaping(_ image: UIImage) -> Void) {
        switch filterType {
        case EditFilter.exposure:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.exposureFilter = GPUImageExposureFilter()
                objEditingVM.exposureFilter.exposure = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.exposureFilter,atTextureLocation: 0)
                objEditingVM.exposureFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.exposureFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.brightness:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.brightnessFilter = GPUImageBrightnessFilter()
                objEditingVM.brightnessFilter.brightness = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.brightnessFilter,atTextureLocation: 0)
                objEditingVM.brightnessFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.brightnessFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.contrest:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.contrestFilter = GPUImageContrastFilter()
                objEditingVM.contrestFilter.contrast = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.contrestFilter,atTextureLocation: 0)
                objEditingVM.contrestFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.contrestFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.shadow:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.hightltShadFilter = GPUImageHighlightShadowFilter()
                objEditingVM.hightltShadFilter.shadows = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.hightltShadFilter,atTextureLocation: 0)
                objEditingVM.hightltShadFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.hightltShadFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.hightlights:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.hightltShadFilter = GPUImageHighlightShadowFilter()
                objEditingVM.hightltShadFilter.highlights = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.hightltShadFilter,atTextureLocation: 0)
                objEditingVM.hightltShadFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.hightltShadFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.Saturation:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.SaturationFilter = GPUImageSaturationFilter()
                objEditingVM.SaturationFilter.saturation = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.SaturationFilter,atTextureLocation: 0)
                objEditingVM.SaturationFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.SaturationFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.vibrance:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.vibranceFilter = GPUImageVibranceFilter()
                objEditingVM.vibranceFilter.vibrance = GLfloat(sliderValue)
                objEditingVM.originalImage.addTarget(objEditingVM.vibranceFilter,atTextureLocation: 0)
                objEditingVM.vibranceFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.vibranceFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.temperature:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                let aCGImage = (normalImg.resizedImage(imgVwEditing.image!.size))?.cgImage
                self.inputImage = CIImage(cgImage: aCGImage!)
                self.context = CIContext(options: nil)
                customFilter = CIFilter(name: "CITemperatureAndTint")
                customFilter.setValue(inputImage, forKey: "inputImage")
                customFilter.setValue(CIVector(x: sliderValue, y: 100), forKey: "inputNeutral")
                outputImage = customFilter.outputImage!
                let imageRef = context.createCGImage(outputImage, from: outputImage.extent)
                completion(UIImage(cgImage: imageRef!))
            }
        case EditFilter.tint:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.tintFilter = GPUImageHighlightShadowTintFilter()
                objEditingVM.tintFilter.shadowTintIntensity = GLfloat(sliderValue)
                objEditingVM.originalImage.addTarget(objEditingVM.tintFilter,atTextureLocation: 0)
                objEditingVM.tintFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.tintFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.sharpen:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.sharpenFilter = GPUImageSharpenFilter()
                objEditingVM.sharpenFilter.sharpness = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.sharpenFilter,atTextureLocation: 0)
                objEditingVM.sharpenFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.sharpenFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.vignette:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.vignetteFilter = GPUImageVignetteFilter()
                objEditingVM.vignetteFilter.vignetteStart = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.vignetteFilter,atTextureLocation: 0)
                objEditingVM.vignetteFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.vignetteFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                }
            }
        case EditFilter.grain:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                inputImage = CIImage(cgImage: ((normalImg.resizedImage(imgVwEditing.image!.size))?.cgImage!)!)
                applyGrainFilterOnImage(sliderValue: sliderValue) { (image) in
                    completion(image)
                }
            }
        default:
            break
        }
    }
    
    func applyColorFilterOnImage(normalImg:UIImage,completion:@escaping(_ image: UIImage) -> Void) {
        objEditingVM.objMultiBandHSV = MultiBandHSV()
        let newCgImage = normalImg.resizedImage(imgVwEditing.image!.size)?.cgImage
        if newCgImage != nil {
            let ciImageNew = CIImage(cgImage: newCgImage!)
            objEditingVM.objMultiBandHSV.inputImage = ciImageNew
            switch objEditingVM.objSaveMyFilterModel.colorIndex {
            case 0:
                self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: CGFloat(objEditingVM.objSaveMyFilterModel.colorHue), y: CGFloat(objEditingVM.objSaveMyFilterModel.colorSaturation), z: CGFloat(objEditingVM.objSaveMyFilterModel.colorLuminance))
            case 1:
                self.objEditingVM.objMultiBandHSV.inputOrangeShift = CIVector(x: CGFloat(objEditingVM.objSaveMyFilterModel.colorHue), y: CGFloat(objEditingVM.objSaveMyFilterModel.colorSaturation), z: CGFloat(objEditingVM.objSaveMyFilterModel.colorLuminance))
            case 2:
                self.objEditingVM.objMultiBandHSV.inputYellowShift = CIVector(x: CGFloat(objEditingVM.objSaveMyFilterModel.colorHue), y: CGFloat(objEditingVM.objSaveMyFilterModel.colorSaturation), z: CGFloat(objEditingVM.objSaveMyFilterModel.colorLuminance))
            case 3:
                self.objEditingVM.objMultiBandHSV.inputGreenShift = CIVector(x: CGFloat(objEditingVM.objSaveMyFilterModel.colorHue), y: CGFloat(objEditingVM.objSaveMyFilterModel.colorSaturation), z: CGFloat(objEditingVM.objSaveMyFilterModel.colorLuminance))
            default:
                self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: CGFloat(objEditingVM.objSaveMyFilterModel.colorHue), y: CGFloat(objEditingVM.objSaveMyFilterModel.colorSaturation), z: CGFloat(objEditingVM.objSaveMyFilterModel.colorLuminance))
            }
            let imageVal = UIImage(ciImage: objEditingVM.objMultiBandHSV.outputImage!)
            completion(imageVal.resizedImage(imgVwEditing.image!.size)!)
        } else {
            completion(normalImg)
        }
    }
}



extension EditingVC {
    
    func fetchDataFromFirebase(_ filterKey: String) {
        let ref : DatabaseReference!
        ref = Database.database().reference()
        ref.child("PresetFilter").observeSingleEvent(of: .value, with: {(snapshot) in
            if snapshot.exists() {
                if let userDict = snapshot.value as? [String:Any] {
                    if var filterKeyValue = userDict[filterKey] as? Int {
                        filterKeyValue = filterKeyValue + 1
                        ref.child("PresetFilter").child(filterKey).setValue(filterKeyValue)
                    } else {
                        ref.child("PresetFilter").child(filterKey).setValue(1)
                    }
                }
            }
        })
    }
    
    func saveFiltersDataInDB(objDetailMedia: SaveMyFilterModel) {
        objDetailMedia.filterId = generateRandomString
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "SaveFiltersTable", in: managedContext)!
        let objSaveFilter = NSManagedObject(entity: entity, insertInto: managedContext)
        
        objSaveFilter.setValue(objDetailMedia.isPresetApply, forKeyPath: "isPresetApply")
        objSaveFilter.setValue(objDetailMedia.presetName, forKeyPath: "presetName")
        
        objSaveFilter.setValue(objDetailMedia.editFilterValue, forKeyPath: "editFilterValue")
        objSaveFilter.setValue(objDetailMedia.isEditFilterApply, forKeyPath: "isEditFilterApply")
        objSaveFilter.setValue(objDetailMedia.editFilerName, forKeyPath: "editFilerName")
        objSaveFilter.setValue(objDetailMedia.filterInputkeyEdit, forKeyPath: "filterInputkeyEdit")
        objSaveFilter.setValue(objDetailMedia.filterKeyEdit, forKeyPath: "filterKeyEdit")
        
        objSaveFilter.setValue(objDetailMedia.colorIndex, forKeyPath: "colorIndex")
        objSaveFilter.setValue(objDetailMedia.isColorFilterApply, forKeyPath: "isColorFilterApply")
        objSaveFilter.setValue(objDetailMedia.colorHue, forKeyPath: "colorHue")
        objSaveFilter.setValue(objDetailMedia.colorSaturation, forKeyPath: "colorSaturation")
        objSaveFilter.setValue(objDetailMedia.colorLuminance, forKeyPath: "colorLuminance")
        
        objSaveFilter.setValue(objDetailMedia.dustFilterImageInString, forKeyPath: "dustFilterImageInString")
        objSaveFilter.setValue(objDetailMedia.isDustFilterApply, forKeyPath: "isDustFilterApply")
        objSaveFilter.setValue(objDetailMedia.dustAlphaVal, forKeyPath: "dustAlphaVal")
        
        objSaveFilter.setValue(objDetailMedia.isFlareFilterApply, forKeyPath: "isFlareFilterApply")
        objSaveFilter.setValue(objDetailMedia.flareFilterImageInString, forKeyPath: "flareFilterImageInString")
        objSaveFilter.setValue(objDetailMedia.flareAlphaVal, forKeyPath: "flareAlphaVal")
        
        objSaveFilter.setValue(objDetailMedia.effectsFilterImageInString, forKeyPath: "effectsFilterImageInString")
        objSaveFilter.setValue(objDetailMedia.isEffectsFilterApply, forKeyPath: "isEffectsFilterApply")
        objSaveFilter.setValue(objDetailMedia.effectAlphaVal, forKeyPath: "effectAlphaVal")
        
        objSaveFilter.setValue(objDetailMedia.isVideofilterApply, forKeyPath: "isVideofilterApply")
        objSaveFilter.setValue(objDetailMedia.filterId, forKeyPath: "filterId")
        objSaveFilter.setValue(objDetailMedia.filterImageInString, forKeyPath: "filterImageInString")
        objSaveFilter.setValue(objDetailMedia.filterName, forKeyPath: "filterName")
        objSaveFilter.setValue(objDetailMedia.isSelected, forKeyPath: "isSelected")
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveVideoFiltersDataInDB(objDetailMedia: SaveMyFilterModel) {
        objDetailMedia.filterId = generateRandomString
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "SaveFiltersTable", in: managedContext)!
        let objSaveFilter = NSManagedObject(entity: entity, insertInto: managedContext)
        
        objSaveFilter.setValue(objDetailMedia.isPresetApply, forKeyPath: "isPresetApply")
        objSaveFilter.setValue(objDetailMedia.presetName, forKeyPath: "presetName")
        
        objSaveFilter.setValue(objDetailMedia.editFilterValue, forKeyPath: "editFilterValue")
        objSaveFilter.setValue(objDetailMedia.isEditFilterApply, forKeyPath: "isEditFilterApply")
        objSaveFilter.setValue(objDetailMedia.editFilerName, forKeyPath: "editFilerName")
        objSaveFilter.setValue(objDetailMedia.filterInputkeyEdit, forKeyPath: "filterInputkeyEdit")
        objSaveFilter.setValue(objDetailMedia.filterKeyEdit, forKeyPath: "filterKeyEdit")
        
        objSaveFilter.setValue(objDetailMedia.colorIndex, forKeyPath: "colorIndex")
        objSaveFilter.setValue(objDetailMedia.isColorFilterApply, forKeyPath: "isColorFilterApply")
        objSaveFilter.setValue(objDetailMedia.colorHue, forKeyPath: "colorHue")
        objSaveFilter.setValue(objDetailMedia.colorSaturation, forKeyPath: "colorSaturation")
        objSaveFilter.setValue(objDetailMedia.colorLuminance, forKeyPath: "colorLuminance")
        
        objSaveFilter.setValue(objDetailMedia.dustFilterImageInString, forKeyPath: "dustFilterImageInString")
        objSaveFilter.setValue(objDetailMedia.isDustFilterApply, forKeyPath: "isDustFilterApply")
        objSaveFilter.setValue(objDetailMedia.dustAlphaVal, forKeyPath: "dustAlphaVal")
        
        objSaveFilter.setValue(objDetailMedia.isFlareFilterApply, forKeyPath: "isFlareFilterApply")
        objSaveFilter.setValue(objDetailMedia.flareFilterImageInString, forKeyPath: "flareFilterImageInString")
        objSaveFilter.setValue(objDetailMedia.flareAlphaVal, forKeyPath: "flareAlphaVal")
        
        objSaveFilter.setValue(objDetailMedia.effectsFilterImageInString, forKeyPath: "effectsFilterImageInString")
        objSaveFilter.setValue(objDetailMedia.isEffectsFilterApply, forKeyPath: "isEffectsFilterApply")
        objSaveFilter.setValue(objDetailMedia.effectAlphaVal, forKeyPath: "effectAlphaVal")
        
        objSaveFilter.setValue(objDetailMedia.isVideofilterApply, forKeyPath: "isVideofilterApply")
        objSaveFilter.setValue(objDetailMedia.filterId, forKeyPath: "filterId")
        objSaveFilter.setValue(objDetailMedia.filterImageInString, forKeyPath: "filterImageInString")
        objSaveFilter.setValue(objDetailMedia.filterName, forKeyPath: "filterName")
        objSaveFilter.setValue(objDetailMedia.isSelected, forKeyPath: "isSelected")
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func deleteSaveFilterFromDatabase(objDetailMedia: SaveMyFilterModel) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SaveFiltersTable")
        fetchRequest.predicate = NSPredicate(format: "filterId = %@", objDetailMedia.filterId)
        do {
            let detailMediaTable = try managedContext.fetch(fetchRequest)
            let deleteObject = detailMediaTable[0]
            managedContext.delete(deleteObject)
            do {
                try managedContext.save()
            }
            catch {
                print(error)
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}

