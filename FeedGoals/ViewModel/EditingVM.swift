//
//  EditingVM.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 17/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import AVKit
import GPUImage
import Firebase
import FirebaseAnalytics
import FirebaseDatabase
import Photos


class EditingVM {
    
    //MARK:- Variables
    var objImageVideoModel = ImageVideoModel(),
    objSaveMyFilterModel = SaveMyFilterModel(),
    saveMyFilterSelected = -1,
    selectedIndex = Int(),
    reEditItemIndex = -1,
    selectFilterType = 1,
    
    arrayDustFilter = ["noEffects","dust1","dust2","dust3","dust4","dust5","dust6","dust7","dust8","dust9","dust10","dust11","dust12","dust13","dust14","dust15","dust16","dust17", "dust18","dust19","dust20","dust21","dust22","dust23"],
    arrayFlareFilter = ["noEffects","flare1","flare2","flare3","flare4","flare5","flare6","flare7","flare8","flare9","flare10","flare11"],
    arrayEffectsFilter = ["noEffects","effect1","effect2","effect3","effect4","effect5","effect6","effect7","effect8","effect9","effect10"],
    arrayVideoColorList = [Color.redColor,Color.aquaColor,Color.yellowColor,Color.greenColor,Color.orangeColor,Color.magentaColor,Color.purpleColor,Color.blueColor],
    arrayEditFilter = [(EditFilter.exposure,UIImage(named: "exposure"),0,"CIExposureAdjust","inputEV"),
                       (EditFilter.brightness,UIImage(named: "brightness"),0,"CIColorControls","inputBrightness"),
                       (EditFilter.contrest,UIImage(named: "contrest"),1,"CIColorControls","inputContrast"),
                       (EditFilter.shadow,UIImage(named: "shadow"),0,"CIHighlightShadowAdjust","inputShadowAmount"),
                       (EditFilter.hightlights,UIImage(named: "hightlights"),0,"CIHighlightShadowAdjust","inputHighlightAmount"),
                       (EditFilter.Saturation,UIImage(named: "Saturation"),1,"CIColorControls","inputSaturation"),
                       (EditFilter.vibrance,UIImage(named: "vibrance"),0,"CIVibrance","inputAmount"),
                       (EditFilter.temperature,UIImage(named: "nature"),3500,"CITemperatureAndTint","inputNeutral"),
                       (EditFilter.tint,UIImage(named: "tint"),3500,"CITemperatureAndTint","inputTargetNeutral"),
                       (EditFilter.sharpen,UIImage(named: "sharpen"),0,"CISharpenLuminance","inputSharpness"),
                       (EditFilter.vignette,UIImage(named: "vignette"),100,"CIVignetteEffect","inputRadius"),
                       (EditFilter.grain,UIImage(named: "grain"),0.0,"CISepiaTone","CISepiaTone")],
    arrayColorListVal = [(UIColor,UIColor)](),
    videoPlayer = AVPlayer(),
    avPlayerController = AVPlayerViewController(),
    playerItem : AVPlayerItem! = nil,
    videoCurentTime : Float64 = 0.00,
    filtersList = [(UIImage,String)](),
    blendFilter = GPUImageLightenBlendFilter(),
    lookupFilterObj = GPUImageLookupFilter(),
    originalImage = GPUImagePicture(),
    filterImage = GPUImagePicture(),
    exposureFilter = GPUImageExposureFilter(),
    brightnessFilter = GPUImageBrightnessFilter(),
    contrestFilter = GPUImageContrastFilter(),
    hightltShadFilter = GPUImageHighlightShadowFilter(),
    SaturationFilter = GPUImageSaturationFilter(),
    vibranceFilter = GPUImageVibranceFilter(),
    tintFilter = GPUImageHighlightShadowTintFilter(),
    sharpenFilter = GPUImageSharpenFilter(),
    vignetteFilter = GPUImageVignetteFilter(),
    selectEditIndex = Int(),
    filteredView = GPUImageView(),
    objMultiBandHSV = MultiBandHSV(),
    hueColorFilter = CGFloat(),
    saturationColor = CGFloat(),
    luminanceColorFilter = CGFloat(),
    previewImageFilter = UIImage(),
    arrayFilterListApply = NSMutableArray(),
    imgPresetFilterApply = UIImage(),
    imgEditFilterApply = UIImage(),
    imgColorFilterApply = UIImage(),
    imgDustFilterApply = UIImage(),
    imgFlareFilterApply = UIImage(),
    imgEffectsFilterApply = UIImage(),
    videoPlayerLayer = AVPlayerLayer(),
    objVideoFilterModel = VideoFilterModel()
}

extension EditingVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- Collection View Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == colVwFilterList {
            switch objEditingVM.selectFilterType {
            case BottomFilters.Presets:
                return objEditingVM.filtersList.count
            case BottomFilters.Edit:
                return objEditingVM.arrayEditFilter.count
            case BottomFilters.Dust:
                return objEditingVM.arrayDustFilter.count
            case BottomFilters.Flare:
                return objEditingVM.arrayFlareFilter.count
            case BottomFilters.Effects:
                return objEditingVM.arrayEffectsFilter.count
            default:
                return 0
            }
        } else {
            return objEditingVM.objImageVideoModel.isVideoSelected ? objEditingVM.arrayVideoColorList.count : objEditingVM.arrayColorListVal.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == colVwFilterList {
            if objEditingVM.selectFilterType == BottomFilters.Edit {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditingEditFilterVC", for: indexPath) as! EditingEditFilterCVC
                let detail = objEditingVM.arrayEditFilter[indexPath.item]
                cell.imgVwEditPic.image = detail.1
                cell.lblEditFilterName.text = detail.0
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditingFilterCVC", for: indexPath) as! EditingFilterCVC
                switch objEditingVM.selectFilterType {
                case BottomFilters.Presets:
                    if objEditingVM.filtersList[indexPath.row].1 == "LANIKAI" {
                        cell.lblFilterName.text = "\(objEditingVM.filtersList[indexPath.item].1.uppercased()) (M)"
                    } else {
                        cell.lblFilterName.text = objEditingVM.filtersList[indexPath.item].1.uppercased()
                    }
                    cell.imgVwLock.isHidden = true
                    if !objEditingVM.objImageVideoModel.isVideoSelected {
                        if indexPath.row == 0 {
                            cell.imgVwFilter.image = objEditingVM.previewImageFilter
                        } else {
                            cell.imgVwFilter.image = objEditingVM.filtersList[indexPath.item].0
                        }
                    } else {
                        if indexPath.row == 0 {
                            cell.imgVwFilter.image = objEditingVM.objImageVideoModel.videoThumbhImage
                        } else {
                            cell.imgVwFilter.image = objEditingVM.filtersList[indexPath.item].0
                        }
                    }
                case BottomFilters.Dust:
                    cell.lblFilterName.text = indexPath.item == 0 ? "NO EFFECT" : "\(indexPath.item)"
                    cell.imgVwFilter.image = UIImage(named: objEditingVM.arrayDustFilter[indexPath.item])
                    cell.imgVwLock.isHidden = true
                case BottomFilters.Flare:
                    cell.lblFilterName.text = indexPath.item == 0 ? "NO EFFECT" : "\(indexPath.item)"
                    cell.imgVwFilter.image = UIImage(named: objEditingVM.arrayFlareFilter[indexPath.item])
                    cell.imgVwLock.isHidden = true
                case BottomFilters.Effects:
                    cell.imgVwLock.isHidden = true
                    cell.lblFilterName.text = indexPath.item == 0 ? "NO EFFECT" : "\(indexPath.item)"
                    cell.imgVwFilter.image = UIImage(named: objEditingVM.arrayEffectsFilter[indexPath.item])
                default:
                    break
                }
                if objEditingVM.selectedIndex == indexPath.item {
                    cell.vwBackground.layer.borderWidth = 3
                    cell.vwBackground.layer.borderColor = #colorLiteral(red: 0.8549019608, green: 0.4235294118, blue: 0.2549019608, alpha: 1)
                } else {
                    cell.vwBackground.layer.borderWidth = 0
                    cell.vwBackground.layer.borderColor = UIColor.clear.cgColor
                }
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditingColorFilterCVC", for: indexPath) as! EditingColorFilterCVC
            if objEditingVM.objImageVideoModel.isVideoSelected {
                if objEditingVM.selectedIndex == indexPath.item {
                    cell.vwColorList.backgroundColor = objEditingVM.arrayVideoColorList[indexPath.item]
                    cell.vwColorList.layer.borderWidth = 0
                    cell.vwColorList.layer.borderColor = UIColor.clear.cgColor
                } else {
                    cell.vwColorList.backgroundColor = .white
                    cell.vwColorList.layer.borderWidth = 5
                    cell.vwColorList.layer.borderColor = objEditingVM.arrayVideoColorList[indexPath.item].cgColor
                }
            } else {
                if objEditingVM.arrayColorListVal.count > 0 {
                    if objEditingVM.selectedIndex == indexPath.item {
                        cell.vwColorList.backgroundColor = objEditingVM.arrayColorListVal[indexPath.item].0
                        cell.vwColorList.layer.borderWidth = 0
                        cell.vwColorList.layer.borderColor = UIColor.clear.cgColor
                    } else {
                        cell.vwColorList.backgroundColor = .white
                        cell.vwColorList.layer.borderWidth = 5
                        cell.vwColorList.layer.borderColor = objEditingVM.arrayColorListVal[indexPath.item].0.cgColor
                    }
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colVwFilterList {
            if objEditingVM.selectFilterType == BottomFilters.Edit {
                lblEditFilterName.text = objEditingVM.arrayEditFilter[indexPath.item].0
                vwSlidingEditFilter.isHidden = false
                cnstHeightVwSlidingEditFilter.constant = 150
                vwFilterList.isHidden = true
                cnstHeightVwFilterList.constant = 0
                vwTopNavigation.isHidden = true
                vwTopNaviEditFilter.isHidden = false
                vwBottomTabbar.isHidden = true
                cnstHeightVwBottomTabbar.constant = 0
                objEditingVM.selectEditIndex = indexPath.item
                let detailEdit = objEditingVM.arrayEditFilter[indexPath.item]
                if objEditingVM.objImageVideoModel.isVideoSelected {
                    activevideoFilter(BottomFilters.Edit)
                    objEditingVM.objVideoFilterModel.filterNameEdit = detailEdit.0
                    objEditingVM.objVideoFilterModel.filterValEdit = CGFloat(detailEdit.2)
                    objEditingVM.objVideoFilterModel.filterKeyEdit = detailEdit.3
                    objEditingVM.objVideoFilterModel.filterInputkeyEdit = detailEdit.4
                    applyVideoFilterMethod(filterClick: BottomFilters.Edit, fileURL: objEditingVM.objImageVideoModel.videoUrl!, isSlide: false)
                    if detailEdit.0 == EditFilter.vignette {
                        sliderEditingFilter.minimumValue = 100
                        sliderEditingFilter.maximumValue = 1000
                    } else if detailEdit.0 == EditFilter.hightlights {
                        sliderEditingFilter.minimumValue = -2
                        sliderEditingFilter.maximumValue = 2
                    } else if detailEdit.0 == EditFilter.temperature {
                        sliderEditingFilter.minimumValue = 3500
                        sliderEditingFilter.maximumValue = 10000
                    } else if detailEdit.0 == EditFilter.tint {
                        sliderEditingFilter.minimumValue = 3500
                        sliderEditingFilter.maximumValue = 10000
                    } else if detailEdit.0 == EditFilter.grain {
                        sliderEditingFilter.minimumValue = 0
                        sliderEditingFilter.maximumValue = 0.9
                    } else {
                        sliderEditingFilter.minimumValue = -1
                        sliderEditingFilter.maximumValue = 1
                    }
                    sliderEditingFilter.value = Float(detailEdit.2)
                    lblEditFilterValue.text = "\(detailEdit.2.setDecimalValue(2))"
                } else {
                    isApplyFilterOnImage(filterType: BottomFilters.Edit)
                    objEditingVM.objSaveMyFilterModel.isEditFilterApply = true
                    objEditingVM.objSaveMyFilterModel.editFilerName = detailEdit.0
                    objEditingVM.objSaveMyFilterModel.editFilterValue = detailEdit.2
                    objEditingVM.objSaveMyFilterModel.filterKeyEdit = detailEdit.3
                    objEditingVM.objSaveMyFilterModel.filterInputkeyEdit = detailEdit.4
                    if detailEdit.0 == EditFilter.temperature {
                        sliderEditingFilter.minimumValue = 3500
                        sliderEditingFilter.maximumValue = 10000
                        let aCGImage = (objEditingVM.previewImageFilter.resizedImage(imgVwEditing.image!.size))?.cgImage
                        inputImage = CIImage(cgImage: aCGImage!)
                        context = CIContext(options: nil)
                        applyTempratureFilter(filterValue: 10000)
                        sliderEditingFilter.value = 10000
                        lblEditFilterValue.text = "\((10000).setDecimalValue(2))"
                    } else if detailEdit.0 == EditFilter.grain {
                        sliderEditingFilter.minimumValue = 0
                        sliderEditingFilter.maximumValue = 0.9
                        let cgImage = objEditingVM.previewImageFilter.resizedImage(imgVwEditing.image!.size)!.cgImage
                        inputImage = CIImage(cgImage: cgImage!)
                        applyGrainFilterOnImage(sliderValue: 0) { (image) in
                            self.imgVwEditing.image = image
                            self.objEditingVM.imgEditFilterApply = image
                        }
                        sliderEditingFilter.value = 0
                        lblEditFilterValue.text = "\((0).setDecimalValue(2))"
                    } else if detailEdit.0 == EditFilter.vignette {
                        sliderEditingFilter.minimumValue = 0
                        sliderEditingFilter.maximumValue = 0.5
                        objEditingVM.originalImage.removeAllTargets()
                        objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                        self.applyEditFilterMethod(filterType: detailEdit.0, sliderValue: CGFloat(0), isVideoSlider: false)
                        sliderEditingFilter.value = 0
                        lblEditFilterValue.text = "\((0).setDecimalValue(2))"
                    } else if detailEdit.0 == EditFilter.hightlights {
                        sliderEditingFilter.minimumValue = -2
                        sliderEditingFilter.maximumValue = 2
                        objEditingVM.originalImage.removeAllTargets()
                        objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                        self.applyEditFilterMethod(filterType: detailEdit.0, sliderValue: CGFloat(0), isVideoSlider: false)
                        sliderEditingFilter.value = 0
                        lblEditFilterValue.text = "\((0).setDecimalValue(2))"
                    } else {
                        sliderEditingFilter.minimumValue = -1
                        sliderEditingFilter.maximumValue = 1
                        objEditingVM.originalImage.removeAllTargets()
                        objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                        self.applyEditFilterMethod(filterType: detailEdit.0, sliderValue: CGFloat(detailEdit.2), isVideoSlider: false)
                        sliderEditingFilter.value = Float(detailEdit.2)
                        lblEditFilterValue.text = "\(detailEdit.2.setDecimalValue(2))"
                    }
                }
            } else {
                switch objEditingVM.selectFilterType {
                case BottomFilters.Presets:
                    if !objEditingVM.objImageVideoModel.isVideoSelected {
                        isApplyFilterOnImage(filterType: BottomFilters.Presets)
                        if indexPath.row == 0 {
                            objEditingVM.objSaveMyFilterModel.isPresetApply = false
                            imgVwEditing.image = objEditingVM.previewImageFilter
                            objEditingVM.imgPresetFilterApply = objEditingVM.previewImageFilter
                            objEditingVM.selectedIndex = indexPath.row
                            colVwFilterList.reloadData()
                        } else {
                            Analytics.logEvent("preset_filter",parameters: [
                                "FilterName": objEditingVM.filtersList[indexPath.item].1,
                            ])
                            fetchDataFromFirebase(objEditingVM.filtersList[indexPath.item].1)
                            if indexPath.row == objEditingVM.selectedIndex {
                                sliderDustFlareIntensity.minimumValue = 0
                                sliderDustFlareIntensity.maximumValue = 1
                                vwDustFlareIntensityDetail.isHidden = false
                                sliderDustFlareIntensity.value = 1
                                lblDustFlareIntensity.text = 1.setDecimalValue(2)
                            } else {
                                objEditingVM.selectedIndex = indexPath.row
                                objEditingVM.originalImage.removeAllTargets()
                                objEditingVM.filterImage.removeAllTargets()
                                objEditingVM.lookupFilterObj = GPUImageLookupFilter()
                                objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                                objEditingVM.originalImage.addTarget(objEditingVM.lookupFilterObj,atTextureLocation: 0)
                                objEditingVM.originalImage.processImage()
                                let lookupFilterImg = UIImage(named: objEditingVM.filtersList[indexPath.item].1)
                                objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg)
                                objEditingVM.filterImage.addTarget(objEditingVM.lookupFilterObj,atTextureLocation: 1)
                                objEditingVM.lookupFilterObj.useNextFrameForImageCapture()
                                objEditingVM.lookupFilterObj.intensity = 1
                                objEditingVM.filterImage.processImage {
                                    let image = self.objEditingVM.lookupFilterObj.imageFromCurrentFramebuffer()
                                    DispatchQueue.main.async {
                                        self.objEditingVM.objSaveMyFilterModel.isPresetApply = true
                                        self.objEditingVM.objSaveMyFilterModel.presetName = self.objEditingVM.filtersList[indexPath.row].1
                                        self.imgVwEditing.image = image
                                        self.objEditingVM.imgPresetFilterApply = image!
                                        self.objEditingVM.selectedIndex = indexPath.row
                                        self.colVwFilterList.reloadData()
                                    }
                                }
                            }
                        }
                    } else {
                        objEditingVM.selectedIndex = indexPath.row
                        if indexPath.row == 0 {
                            playNormalVideoWithoutFilter(fileURL: objEditingVM.objImageVideoModel.videoUrl!)
                        } else {
                            fetchDataFromFirebase(objEditingVM.filtersList[indexPath.item].1)
                            activevideoFilter(BottomFilters.Presets)
                            objEditingVM.objVideoFilterModel.presetFilterName = objEditingVM.filtersList[indexPath.row].1
                            applyVideoFilterMethod(filterClick: BottomFilters.Presets, fileURL: objEditingVM.objImageVideoModel.videoUrl!, isSlide: false)
                        }
                        colVwFilterList.reloadData()
                    }
                case BottomFilters.Dust:
                    if !objEditingVM.objImageVideoModel.isVideoSelected {
                        isApplyFilterOnImage(filterType: BottomFilters.Dust)
                        if indexPath.row == 0 {
                            objEditingVM.objSaveMyFilterModel.isDustFilterApply = false
                            imgVwEditing.image = objEditingVM.previewImageFilter
                            objEditingVM.imgDustFilterApply = objEditingVM.previewImageFilter
                            objEditingVM.selectedIndex = indexPath.row
                            colVwFilterList.reloadData()
                        } else {
                            if indexPath.row == objEditingVM.selectedIndex {
                                sliderDustFlareIntensity.minimumValue = 0
                                sliderDustFlareIntensity.maximumValue = 1
                                vwDustFlareIntensityDetail.isHidden = false
                                sliderDustFlareIntensity.value = 1
                                lblDustFlareIntensity.text = 1.setDecimalValue(2)
                            } else {
                                objEditingVM.selectedIndex = indexPath.row
                                objEditingVM.originalImage.removeAllTargets()
                                objEditingVM.filterImage.removeAllTargets()
                                objEditingVM.blendFilter = GPUImageLightenBlendFilter()
                                objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                                objEditingVM.originalImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 0)
                                objEditingVM.originalImage.processImage()
                                let lookupFilterImg = UIImage(named: objEditingVM.arrayDustFilter[indexPath.item])
                                objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg?.withAlpha(1))
                                objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
                                objEditingVM.blendFilter.useNextFrameForImageCapture()
                                objEditingVM.filterImage.processImage {
                                    let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                                    DispatchQueue.main.async {
                                        self.imgVwEditing.image = image
                                        self.objEditingVM.imgDustFilterApply = image!
                                        self.objEditingVM.objSaveMyFilterModel.isDustFilterApply = true
                                        self.objEditingVM.objSaveMyFilterModel.dustFilterImageInString = self.objEditingVM.arrayDustFilter[indexPath.row]
                                        self.objEditingVM.objSaveMyFilterModel.dustAlphaVal = 1
                                        self.objEditingVM.selectedIndex = indexPath.row
                                        self.colVwFilterList.reloadData()
                                    }
                                }
                            }
                        }
                    } else {
                        objEditingVM.selectedIndex = indexPath.row
                        if indexPath.row == 0 {
                            playNormalVideoWithoutFilter(fileURL: objEditingVM.objImageVideoModel.videoUrl!)
                        } else {
                            activevideoFilter(BottomFilters.Dust)
                            applyDustFlareEffectFilterVideoMethod(fileURL: objEditingVM.objImageVideoModel.videoUrl!, filterImage: UIImage(named: objEditingVM.arrayDustFilter[indexPath.row])!)
                        }
                        colVwFilterList.reloadData()
                    }
                case BottomFilters.Flare:
                    if !objEditingVM.objImageVideoModel.isVideoSelected {
                        isApplyFilterOnImage(filterType: BottomFilters.Flare)
                        if indexPath.row == 0 {
                            objEditingVM.objSaveMyFilterModel.isFlareFilterApply = false
                            imgVwEditing.image = objEditingVM.previewImageFilter
                            objEditingVM.imgFlareFilterApply = objEditingVM.previewImageFilter
                            objEditingVM.selectedIndex = indexPath.row
                            colVwFilterList.reloadData()
                        } else {
                            if indexPath.row == objEditingVM.selectedIndex {
                                sliderDustFlareIntensity.minimumValue = 0
                                sliderDustFlareIntensity.maximumValue = 1
                                vwDustFlareIntensityDetail.isHidden = false
                                sliderDustFlareIntensity.value = 0.6
                                lblDustFlareIntensity.text = 0.6.setDecimalValue(2)
                            } else {
                                objEditingVM.selectedIndex = indexPath.row
                                objEditingVM.originalImage.removeAllTargets()
                                objEditingVM.filterImage.removeAllTargets()
                                objEditingVM.blendFilter = GPUImageLightenBlendFilter()
                                objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                                objEditingVM.originalImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 0)
                                objEditingVM.originalImage.processImage()
                                let lookupFilterImg = UIImage(named: objEditingVM.arrayFlareFilter[indexPath.item])
                                objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg?.withAlpha(0.6))
                                objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
                                objEditingVM.blendFilter.useNextFrameForImageCapture()
                                objEditingVM.filterImage.processImage {
                                    let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                                    DispatchQueue.main.async {
                                        self.imgVwEditing.image = image
                                        self.objEditingVM.imgFlareFilterApply = image!
                                        self.objEditingVM.objSaveMyFilterModel.isFlareFilterApply = true
                                        self.objEditingVM.objSaveMyFilterModel.flareFilterImageInString = self.objEditingVM.arrayFlareFilter[indexPath.row]
                                        self.objEditingVM.objSaveMyFilterModel.flareAlphaVal = 0.6
                                        self.objEditingVM.selectedIndex = indexPath.row
                                        self.colVwFilterList.reloadData()
                                    }
                                }
                            }
                        }
                    } else {
                        objEditingVM.selectedIndex = indexPath.row
                        if indexPath.row == 0 {
                            playNormalVideoWithoutFilter(fileURL: objEditingVM.objImageVideoModel.videoUrl!)
                        } else {
                            activevideoFilter(BottomFilters.Flare)
                            applyDustFlareEffectFilterVideoMethod(fileURL: objEditingVM.objImageVideoModel.videoUrl!, filterImage: UIImage(named: objEditingVM.arrayFlareFilter[indexPath.row])!)
                        }
                        colVwFilterList.reloadData()
                    }
                default:
                    if !objEditingVM.objImageVideoModel.isVideoSelected {
                        isApplyFilterOnImage(filterType: BottomFilters.Effects)
                        if indexPath.row == 0 {
                            objEditingVM.objSaveMyFilterModel.isEffectsFilterApply = false
                            imgVwEditing.image = objEditingVM.previewImageFilter
                            objEditingVM.imgEffectsFilterApply = objEditingVM.previewImageFilter
                            objEditingVM.selectedIndex = indexPath.row
                            colVwFilterList.reloadData()
                        } else {
                            if indexPath.row == objEditingVM.selectedIndex {
                                sliderDustFlareIntensity.minimumValue = 0
                                sliderDustFlareIntensity.maximumValue = 1
                                vwDustFlareIntensityDetail.isHidden = false
                                sliderDustFlareIntensity.value = 0.6
                                lblDustFlareIntensity.text = 0.6.setDecimalValue(2)
                            } else {
                                objEditingVM.selectedIndex = indexPath.row
                                objEditingVM.originalImage.removeAllTargets()
                                objEditingVM.filterImage.removeAllTargets()
                                objEditingVM.blendFilter = GPUImageLightenBlendFilter()
                                objEditingVM.originalImage = GPUImagePicture(image: objEditingVM.previewImageFilter)
                                objEditingVM.originalImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 0)
                                objEditingVM.originalImage.processImage()
                                let lookupFilterImg = UIImage(named: objEditingVM.arrayEffectsFilter[indexPath.item])
                                objEditingVM.filterImage = GPUImagePicture(image: lookupFilterImg?.withAlpha(0.6))
                                objEditingVM.filterImage.addTarget(objEditingVM.blendFilter,atTextureLocation: 1)
                                objEditingVM.blendFilter.useNextFrameForImageCapture()
                                objEditingVM.filterImage.processImage {
                                    let image = self.objEditingVM.blendFilter.imageFromCurrentFramebuffer()
                                    DispatchQueue.main.async {
                                        self.imgVwEditing.image = image
                                        self.objEditingVM.imgEffectsFilterApply = image!
                                        self.objEditingVM.objSaveMyFilterModel.isEffectsFilterApply = true
                                        self.objEditingVM.objSaveMyFilterModel.effectsFilterImageInString = self.objEditingVM.arrayEffectsFilter[indexPath.row]
                                        self.objEditingVM.objSaveMyFilterModel.effectAlphaVal = 0.6
                                        self.objEditingVM.selectedIndex = indexPath.row
                                        self.colVwFilterList.reloadData()
                                    }
                                }
                            }
                        }
                    } else {
                        objEditingVM.selectedIndex = indexPath.row
                        if indexPath.row == 0 {
                            playNormalVideoWithoutFilter(fileURL: objEditingVM.objImageVideoModel.videoUrl!)
                        } else {
                            activevideoFilter(BottomFilters.Effects)
                            applyDustFlareEffectFilterVideoMethod(fileURL: objEditingVM.objImageVideoModel.videoUrl!, filterImage: UIImage(named: objEditingVM.arrayEffectsFilter[indexPath.row])!)
                        }
                        colVwFilterList.reloadData()
                    }
                }
            }
        } else {
            objEditingVM.hueColorFilter = 0
            sliderColorHue.value = 0
            objEditingVM.saturationColor = 1
            sliderColorSaturation.value = 1
            objEditingVM.luminanceColorFilter = 1
            sliderColorLuminance.value = 1
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                
                if objEditingVM.arrayColorListVal.count > 0 {
                    isApplyFilterOnImage(filterType: BottomFilters.Color)
                    objEditingVM.selectedIndex = indexPath.row
                    objEditingVM.objMultiBandHSV = MultiBandHSV()
                    objEditingVM.objSaveMyFilterModel.isColorFilterApply = true
                    objEditingVM.objSaveMyFilterModel.colorHue = Double(objEditingVM.hueColorFilter)
                    objEditingVM.objSaveMyFilterModel.colorSaturation = Double(objEditingVM.saturationColor)
                    objEditingVM.objSaveMyFilterModel.colorLuminance = Double(objEditingVM.luminanceColorFilter)
                    objEditingVM.objSaveMyFilterModel.colorIndex = indexPath.row
                    let caImage = (objEditingVM.previewImageFilter).cgImage
                    if caImage == nil {
                        let caImageNew = (objEditingVM.previewImageFilter.resizedImage(imgVwEditing.image!.size)!).cgImage
                        if caImageNew != nil {
                            let ciImageNew = CIImage(cgImage: caImageNew!)
                            objEditingVM.objMultiBandHSV.inputImage = ciImageNew
                            switch indexPath.row {
                            case 0:
                                self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: 30/255, y: 1, z: 1)
                            case 1:
                                self.objEditingVM.objMultiBandHSV.inputOrangeShift = CIVector(x: 30/255, y: 1, z: 1)
                            case 2:
                                self.objEditingVM.objMultiBandHSV.inputYellowShift = CIVector(x: 30/255, y: 1, z: 1)
                            case 3:
                                self.objEditingVM.objMultiBandHSV.inputGreenShift = CIVector(x: 30/255, y: 1, z: 1)
                            default:
                                self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: 30/255, y: 1, z: 1)
                            }
                            let imageVal = UIImage(ciImage: objEditingVM.objMultiBandHSV.outputImage!)
                            let resizeImage = imageVal.resizedImage(imgVwEditing.image!.size)
                            imgVwEditing.image = resizeImage
                            objEditingVM.imgColorFilterApply = resizeImage!
                        } else {
                            objEditingVM.selectedIndex = -1
                        }
                    } else {
                        let ciImage = CIImage(cgImage: caImage!)
                        objEditingVM.objMultiBandHSV.inputImage = ciImage
                        switch indexPath.row {
                        case 0:
                            self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: 30/255, y: 1, z: 1)
                        case 1:
                            self.objEditingVM.objMultiBandHSV.inputOrangeShift = CIVector(x: 30/255, y: 1, z: 1)
                        case 2:
                            self.objEditingVM.objMultiBandHSV.inputYellowShift = CIVector(x: 30/255, y: 1, z: 1)
                        case 3:
                            self.objEditingVM.objMultiBandHSV.inputGreenShift = CIVector(x: 30/255, y: 1, z: 1)
                        default:
                            self.objEditingVM.objMultiBandHSV.inputRedShift = CIVector(x: 30/255, y: 1, z: 1)
                        }
                        let imageVal = UIImage(ciImage: objEditingVM.objMultiBandHSV.outputImage!)
                        imgVwEditing.image = imageVal
                        objEditingVM.imgColorFilterApply = imageVal
                    }
                    colVwColorFilter.reloadData()
                }
            } else {
                activevideoFilter(BottomFilters.Color)
                objEditingVM.objVideoFilterModel.colorHue = 0
                objEditingVM.objVideoFilterModel.colorSaturation = 1
                objEditingVM.objVideoFilterModel.colorLuminance = 1
                objEditingVM.objVideoFilterModel.colorApplyIndex = indexPath.row
                objEditingVM.selectedIndex = indexPath.row
                applyVideoFilterMethod(filterClick: BottomFilters.Color, fileURL: objEditingVM.objImageVideoModel.videoUrl!, isSlide: false)
                colVwColorFilter.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == colVwFilterList {
            if objEditingVM.selectFilterType == BottomFilters.Edit {
                return CGSize(width: 70, height: 60)
            } else {
                return CGSize(width: 75, height: 100)
            }
        } else {
            return CGSize(width: 32, height: 32)
        }
    }
    
}

extension EditingVC: SaveAndShareDelegate {
    
    //MARK:- Save Data In Gellery Delegate Method
    func SaveItemInGallery(_ isSave: Bool) {
        if isSave {
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                self.displayAlertMessage(AppAlerts.titleValue.imageSavedGallery)
                PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                    if authorizationStatus == .authorized {
                        PHPhotoLibrary.shared().savePhotoInGallery(image: self.imgVwEditing.image!, albumName: "FeedGoals Images") { (phassert) in
                        }
                    }
                })
            } else {
                Proxy.shared.showActivityIndicator()
                saveVideoAfterFilterInHomeAndShare {
                    Proxy.shared.hideActivityIndicator()
                    DispatchQueue.main.async {
                        self.displayAlertMessage(AppAlerts.titleValue.videoSavedGallery)
                    }
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        if authorizationStatus == .authorized {
                            if self.objEditingVM.objVideoFilterModel.videoUrlFiltered != nil {
                                PHPhotoLibrary.shared().saveVideoInGallery(videoUrl: self.objEditingVM.objVideoFilterModel.videoUrlFiltered!, albumName: "FeedGoals Videos") { (phassert) in
                                }
                            }
                        }
                    })
                }
            }
        } else {
            if objEditingVM.objImageVideoModel.isVideoSelected {
                Proxy.shared.showActivityIndicator()
                saveVideoAfterFilterInHomeAndShare {
                    Proxy.shared.hideActivityIndicator()
                    self.shareVideoButton(self.objEditingVM.objVideoFilterModel.videoUrlFiltered!)
                }
            } else {
                self.shareImageButton(imgVwEditing.image!)
            }
        }
    }
    
    func string(from timeInterval: TimeInterval) -> String? {
        let interval = Int(timeInterval)
        let seconds: Int = interval % 60
        let minutes: Int = (interval / 60) % 60
        return String(format: "%0.2ld:%0.2ld", minutes, seconds)
    }
    
    //MARK:- Paly Video Method
    func palyVideoMethod(_ myURL: URL) {
        vwPlayVideo.backgroundColor = .black
        
        let asset = AVURLAsset(url: myURL, options: nil)
        objEditingVM.videoPlayer = AVPlayer()
        objEditingVM.playerItem = AVPlayerItem(url: myURL)
        
        objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
        objEditingVM.avPlayerController.view.frame = CGRect(x: vwPlayVideo.frame.origin.x, y: vwPlayVideo.frame.origin.y, width: vwPlayVideo.frame.size.width, height: vwPlayVideo.frame.size.height)
        vwPlayVideo.addSubview(objEditingVM.avPlayerController.view)
        objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
        objEditingVM.avPlayerController.showsPlaybackControls = false
        objEditingVM.avPlayerController.videoGravity = .resizeAspectFill
        objEditingVM.avPlayerController.player?.play()
        
        sliderVideoPlay.value = 0
        sliderVideoPlay.minimumValue = 0
        lblStartDuration.text = "00.00"
        
        let audioDuration = asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
        if audioDurationSeconds.isNaN || audioDurationSeconds.isInfinite {
        } else {
            lblTotalDuration.text = string(from: audioDurationSeconds)
            sliderVideoPlay.maximumValue = Float(audioDurationSeconds)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: objEditingVM.avPlayerController.player!.currentItem)
        btnPlayVideo.isSelected = true
        objEditingVM.avPlayerController.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.objEditingVM.avPlayerController.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.objEditingVM.avPlayerController.player!.currentTime())
                self.sliderVideoPlay.value = Float(time)
                self.lblStartDuration.text = self.string(from: time)
                self.sliderVideoPlay.isContinuous = false
            }
        }
    }
    
    func loopPlayVideoMethod() {
        btnPlayVideo.isSelected = true
        NotificationCenter.default.addObserver(self, selector: #selector(finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: objEditingVM.avPlayerController.player!.currentItem)
        objEditingVM.avPlayerController.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.objEditingVM.avPlayerController.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.objEditingVM.avPlayerController.player!.currentTime())
                self.sliderVideoPlay.value = Float(time)
                self.lblStartDuration.text = self.string(from: time)
                self.sliderVideoPlay.isContinuous = false
            }
        }
    }
    
    //MARK:- Repeat Paly Video Method
    @objc func finishVideo(_ notification: Notification) {
        self.sliderVideoPlay.value = 0
        self.lblStartDuration.text = "00:00"
        self.btnPlayVideo.isSelected = false
        let seconds : Int64 = Int64(0)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        objEditingVM.avPlayerController.player!.seek(to: targetTime)
        objEditingVM.avPlayerController.player!.play()
        btnPlayVideo.isSelected = true
    }
    
    func removeNotificationObserver() {
        if objEditingVM.objImageVideoModel.isVideoSelected {
            objEditingVM.avPlayerController.player!.pause()
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            vwPlayVideo.removeFromSuperview()
        }
    }
}
extension EditingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySaveMyFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditingSaveFilterTVC", for: indexPath) as! EditingSaveFilterTVC
        if arraySaveMyFilter.count > 0 {
            let detail = arraySaveMyFilter[indexPath.row]
            cell.lblFilterName.text = detail.filterName
            cell.imgVwFilter.image = UIImage(named: detail.filterImageInString)
            cell.vwBackground.backgroundColor = objEditingVM.saveMyFilterSelected == indexPath.row ? Color.selectedSaveFilterColor : .clear
            cell.lblFilterName.textColor = objEditingVM.saveMyFilterSelected == indexPath.row ? .white : .black
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arraySaveMyFilter.count > 0 {
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                Proxy.shared.showActivityIndicator()
                objEditingVM.objSaveMyFilterModel = arraySaveMyFilter[indexPath.row]
                self.applySaveFilterMethod(objEditingVM.objImageVideoModel.imageSelect) { (filterImage) in
                    self.imgVwEditing.image = filterImage
                    self.objEditingVM.saveMyFilterSelected = indexPath.row
                    self.tblVwSaveFilter.reloadData()
                    Proxy.shared.hideActivityIndicator()
                }
            } else {
                let objVideoDetail = arraySaveMyFilter[indexPath.row]
                objEditingVM.objVideoFilterModel.isPresetVideoApply = objVideoDetail.isPresetApply
                objEditingVM.objVideoFilterModel.presetFilterName = objVideoDetail.presetName
                objEditingVM.objVideoFilterModel.filterNameEdit = objVideoDetail.editFilerName
                objEditingVM.objVideoFilterModel.filterValEdit = CGFloat(objVideoDetail.editFilterValue)
                objEditingVM.objVideoFilterModel.filterInputkeyEdit = objVideoDetail.filterInputkeyEdit
                objEditingVM.objVideoFilterModel.filterKeyEdit = objVideoDetail.filterKeyEdit
                objEditingVM.objVideoFilterModel.isEditVideoApply = objVideoDetail.isEditFilterApply
                objEditingVM.objVideoFilterModel.isColorVideoApply = objVideoDetail.isColorFilterApply
                objEditingVM.objVideoFilterModel.colorApplyIndex = objVideoDetail.colorIndex
                objEditingVM.objVideoFilterModel.colorHue = CGFloat(objVideoDetail.colorHue)
                objEditingVM.objVideoFilterModel.colorSaturation = CGFloat(objVideoDetail.colorSaturation)
                objEditingVM.objVideoFilterModel.colorLuminance = CGFloat(objVideoDetail.colorLuminance)
                applyVideoFilterMethod(filterClick: -1, fileURL: objEditingVM.objImageVideoModel.videoUrl!, isSlide: false)
                objEditingVM.saveMyFilterSelected = indexPath.row
                tblVwSaveFilter.reloadData()
            }
        }
    }
}
extension EditingVC {
    
    func isApplyFilterOnImage(filterType: Int) {
        switch filterType {
        case BottomFilters.Presets:
            if objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Presets)
                    objEditingVM.previewImageFilter = objEditingVM.imgEditFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Presets)
                    objEditingVM.previewImageFilter =  objEditingVM.imgColorFilterApply.resizedImage(imgVwEditing.image!.size)!
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Presets)
                    objEditingVM.previewImageFilter = objEditingVM.imgDustFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Presets)
                    objEditingVM.previewImageFilter = objEditingVM.imgFlareFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Presets)
                    objEditingVM.previewImageFilter = objEditingVM.imgEffectsFilterApply
                }
            } else {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Presets)
                    objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
                }
            }
        case BottomFilters.Edit:
            if objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Edit)
                    objEditingVM.previewImageFilter = objEditingVM.imgPresetFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Edit)
                    objEditingVM.previewImageFilter = objEditingVM.imgColorFilterApply.resizedImage(imgVwEditing.image!.size)!
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Edit)
                    objEditingVM.previewImageFilter = objEditingVM.imgDustFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Edit)
                    objEditingVM.previewImageFilter = objEditingVM.imgFlareFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Edit)
                    objEditingVM.previewImageFilter = objEditingVM.imgEffectsFilterApply
                }
            } else {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Edit)
                    objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
                }
            }
        case BottomFilters.Color:
            if objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Color)
                    objEditingVM.previewImageFilter = objEditingVM.imgPresetFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Color)
                    objEditingVM.previewImageFilter = objEditingVM.imgEditFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Color)
                    objEditingVM.previewImageFilter = objEditingVM.imgDustFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Color)
                    objEditingVM.previewImageFilter = objEditingVM.imgFlareFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Color)
                    objEditingVM.previewImageFilter = objEditingVM.imgEffectsFilterApply
                }
            } else {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Color)
                    objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
                }
            }
        case BottomFilters.Dust:
            if objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Dust)
                    objEditingVM.previewImageFilter = objEditingVM.imgPresetFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Dust)
                    objEditingVM.previewImageFilter = objEditingVM.imgEditFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Dust)
                    objEditingVM.previewImageFilter = objEditingVM.imgColorFilterApply.resizedImage(imgVwEditing.image!.size)!
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Dust)
                    objEditingVM.previewImageFilter = objEditingVM.imgFlareFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Dust)
                    objEditingVM.previewImageFilter = objEditingVM.imgEffectsFilterApply
                }
            } else {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Dust)
                    objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
                }
            }
        case BottomFilters.Flare:
            if objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Flare)
                    objEditingVM.previewImageFilter = objEditingVM.imgPresetFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Flare)
                    objEditingVM.previewImageFilter = objEditingVM.imgEditFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Flare)
                    objEditingVM.previewImageFilter = objEditingVM.imgColorFilterApply.resizedImage(imgVwEditing.image!.size)!
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Flare)
                    objEditingVM.previewImageFilter = objEditingVM.imgDustFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Flare)
                    objEditingVM.previewImageFilter = objEditingVM.imgEffectsFilterApply
                }
            } else {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Flare)
                    objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
                }
            }
        case BottomFilters.Effects:
            if objEditingVM.arrayFilterListApply.contains(BottomFilters.Presets) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Effects)
                    objEditingVM.previewImageFilter = objEditingVM.imgPresetFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Edit) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Effects)
                    objEditingVM.previewImageFilter = objEditingVM.imgEditFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Color) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Effects)
                    objEditingVM.previewImageFilter = objEditingVM.imgColorFilterApply.resizedImage(imgVwEditing.image!.size)!
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Dust) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Effects)
                    objEditingVM.previewImageFilter = objEditingVM.imgDustFilterApply
                }
            } else if objEditingVM.arrayFilterListApply.contains(BottomFilters.Flare) {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Effects)
                    objEditingVM.previewImageFilter = objEditingVM.imgFlareFilterApply
                }
            } else {
                if !objEditingVM.arrayFilterListApply.contains(BottomFilters.Effects) {
                    objEditingVM.arrayFilterListApply.removeAllObjects()
                    objEditingVM.arrayFilterListApply.add(BottomFilters.Effects)
                    objEditingVM.previewImageFilter = objEditingVM.objImageVideoModel.imageSelect
                }
            }
        default:
            break
        }
    }
    
    //MARK:- Apply Edit Filter On Video And Image
    func applyEditFilterMethod(filterType: String,sliderValue:CGFloat,isVideoSlider: Bool) {
        objEditingVM.objSaveMyFilterModel.editFilterValue = Double(sliderValue)
        switch filterType {
        case EditFilter.exposure:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.exposureFilter = GPUImageExposureFilter()
                objEditingVM.exposureFilter.exposure = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.exposureFilter,atTextureLocation: 0)
                objEditingVM.exposureFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.exposureFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.brightness:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.brightnessFilter = GPUImageBrightnessFilter()
                objEditingVM.brightnessFilter.brightness = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.brightnessFilter,atTextureLocation: 0)
                objEditingVM.brightnessFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.brightnessFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.contrest:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.contrestFilter = GPUImageContrastFilter()
                objEditingVM.contrestFilter.contrast = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.contrestFilter,atTextureLocation: 0)
                objEditingVM.contrestFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.contrestFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.shadow:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.hightltShadFilter = GPUImageHighlightShadowFilter()
                objEditingVM.hightltShadFilter.shadows = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.hightltShadFilter,atTextureLocation: 0)
                objEditingVM.hightltShadFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.hightltShadFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.hightlights:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.hightltShadFilter = GPUImageHighlightShadowFilter()
                objEditingVM.hightltShadFilter.highlights = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.hightltShadFilter,atTextureLocation: 0)
                objEditingVM.hightltShadFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.hightltShadFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.Saturation:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.SaturationFilter = GPUImageSaturationFilter()
                objEditingVM.SaturationFilter.saturation = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.SaturationFilter,atTextureLocation: 0)
                objEditingVM.SaturationFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.SaturationFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.vibrance:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.vibranceFilter = GPUImageVibranceFilter()
                objEditingVM.vibranceFilter.vibrance = GLfloat(sliderValue)
                objEditingVM.originalImage.addTarget(objEditingVM.vibranceFilter,atTextureLocation: 0)
                objEditingVM.vibranceFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.vibranceFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.temperature:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                applyTempratureFilter(filterValue: sliderValue)
            }
        case EditFilter.tint:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.tintFilter = GPUImageHighlightShadowTintFilter()
                objEditingVM.tintFilter.shadowTintIntensity = GLfloat(sliderValue)
                objEditingVM.originalImage.addTarget(objEditingVM.tintFilter,atTextureLocation: 0)
                objEditingVM.tintFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.tintFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.sharpen:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.sharpenFilter = GPUImageSharpenFilter()
                objEditingVM.sharpenFilter.sharpness = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.sharpenFilter,atTextureLocation: 0)
                objEditingVM.sharpenFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.sharpenFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.vignette:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                objEditingVM.vignetteFilter = GPUImageVignetteFilter()
                objEditingVM.vignetteFilter.vignetteStart = sliderValue
                objEditingVM.originalImage.addTarget(objEditingVM.vignetteFilter,atTextureLocation: 0)
                objEditingVM.vignetteFilter.useNextFrameForImageCapture()
                objEditingVM.originalImage.processImage {
                    let image = self.objEditingVM.vignetteFilter.imageFromCurrentFramebuffer()
                    DispatchQueue.main.async {
                        self.imgVwEditing.image = image
                        self.objEditingVM.imgEditFilterApply = image!
                    }
                }
            }
        case EditFilter.grain:
            if !objEditingVM.objImageVideoModel.isVideoSelected {
                applyGrainFilterOnImage(sliderValue: sliderValue) { (image) in
                    self.imgVwEditing.image = image
                    self.objEditingVM.imgEditFilterApply = image
                }
            }
        default:
            break
        }
    }
    
    //MARK:- Apply Edit Temprature Filter On Image
    func applyTempratureFilter(filterValue: CGFloat) {
        customFilter = CIFilter(name: "CITemperatureAndTint")
        customFilter.setValue(inputImage, forKey: "inputImage")
        customFilter.setValue(CIVector(x: filterValue, y: 100), forKey: "inputNeutral")
        outputImage = customFilter.outputImage!
        let imageRef = context.createCGImage(outputImage, from: outputImage.extent)
        imgVwEditing.image = UIImage(cgImage: imageRef!)
        objEditingVM.imgEditFilterApply = UIImage(cgImage: imageRef!)
        
    }
    
    //MARK:- Apply Edit Grain Filter On Image
    func applyGrainFilterOnImage(sliderValue: CGFloat,completion:@escaping(_ image: UIImage) -> Void) {
        guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
            kCIInputImageKey: inputImage,
            kCIInputIntensityKey: sliderValue
        ]) else {
            print("App bundle doesn't exist")
            return
        }
        
        guard let sepiaCIImage = sepiaFilter.outputImage else {
            print("App bundle doesn't exist")
            return
        }
        
        guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
            let noiseImage = coloredNoise.outputImage else {
                print("App bundle doesn't exist")
                return
        }
        
        let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
        let fineGrain = CIVector(x:0, y: sliderValue, z:0, w:0)
        let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
        
        guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                             parameters:
            [
                kCIInputImageKey: noiseImage,
                "inputRVector": whitenVector,
                "inputGVector": whitenVector,
                "inputBVector": whitenVector,
                "inputAVector": fineGrain,
                "inputBiasVector": zeroVector
        ]), let whiteSpecks = whiteningFilter.outputImage else {
            print("App bundle doesn't exist")
            return
        }
        
        guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                             parameters:
            [
                kCIInputImageKey: whiteSpecks,
                kCIInputBackgroundImageKey: sepiaCIImage
        ]) , let speckledImage = speckCompositor.outputImage else{
            print("App bundle doesn't exist")
            return
        }
        
        let verticalScale = CGAffineTransform(scaleX: sliderValue == 0 ? 0 : 1.5, y: sliderValue == 0 ? 0 : 25)
        let transformedNoise = noiseImage.transformed(by: verticalScale)
        
        let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
        let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
        
        guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
            kCIInputImageKey: transformedNoise,
            "inputRVector": darkenVector,
            "inputGVector": zeroVector,
            "inputBVector": zeroVector,
            "inputAVector": zeroVector,
            "inputBiasVector": darkenBias
        ]),
            let randomScratches = darkeningFilter.outputImage else {
                return
        }
        
        guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
            [
                kCIInputImageKey: randomScratches
        ]),
            let darkScratches = grayscaleFilter.outputImage
            else {
                return
        }
        
        guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
            [
                kCIInputImageKey: darkScratches,
                kCIInputBackgroundImageKey: speckledImage
        ]),
            let oldFilmImage = oldFilmCompositor.outputImage
            else {
                return
        }
        let finalImage = oldFilmImage.cropped(to: inputImage.extent)
        completion(UIImage(ciImage: finalImage))
    }
}
extension EditingVC {
    
    func activevideoFilter(_ filterName: Int) {
        switch filterName {
        case BottomFilters.Presets:
            objEditingVM.objVideoFilterModel.isPresetVideoApply = true
        case BottomFilters.Edit:
            objEditingVM.objVideoFilterModel.isEditVideoApply = true
        case BottomFilters.Color:
            objEditingVM.objVideoFilterModel.isColorVideoApply = true
        case BottomFilters.Dust:
            objEditingVM.objVideoFilterModel.isDustVideoApply = true
        case BottomFilters.Flare:
            objEditingVM.objVideoFilterModel.isFlareVideoApply = true
        case BottomFilters.Effects:
            objEditingVM.objVideoFilterModel.isEffectVideoApply = true
        default:
            objEditingVM.objVideoFilterModel.isPresetVideoApply = false
            objEditingVM.objVideoFilterModel.isEditVideoApply = false
            objEditingVM.objVideoFilterModel.isColorVideoApply = false
            objEditingVM.objVideoFilterModel.isDustVideoApply = false
            objEditingVM.objVideoFilterModel.isFlareVideoApply = false
            objEditingVM.objVideoFilterModel.isEffectVideoApply = false
        }
    }
    
    func saveVideoAfterFilterInHomeAndShare(_ completion:@escaping() -> Void) {
        objEditingVM.avPlayerController.player?.pause()
        btnPlayVideo.isSelected = false
        saveAndShareVideoMethod(fileURL: objEditingVM.objImageVideoModel.videoUrl!) { (videoUrl, isFilterApply) in
            if isFilterApply {
                self.objEditingVM.objVideoFilterModel.videoUrlFiltered = videoUrl
                self.objEditingVM.objVideoFilterModel.thumbVideoImage = Proxy.shared.getVideoThumbnailImageFromVideo(self.objEditingVM.objVideoFilterModel.videoUrlFiltered!)
                completion()
            } else {
                self.objEditingVM.objVideoFilterModel.videoUrlFiltered = self.objEditingVM.objImageVideoModel.videoUrl
                self.objEditingVM.objVideoFilterModel.thumbVideoImage = self.objEditingVM.objImageVideoModel.videoThumbhImage
                completion()
            }
        }
    }
}
