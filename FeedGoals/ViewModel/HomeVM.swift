//
//  HomeVM.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 14/05/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import AVKit
import GPUImage
import Photos
import CoreData

class HomeVM {
    
    //MARK:- Varibales
    var objGalleryCameraImage = GalleryCameraImage(),
    arrayFiltersList = ["No Effect","New Preset File", "KAWAII", "LAGOON", "LANAI", "LANIKAI", "WAIKIKI","MAUI","ASHLEIGH","OLIVIA","WANDERLUST BLUSH","GOLDEN FADE","Aspyn Ovard1","Aspyn Ovard2","Aspyn Ovard3","BIANCA","Bonnie Cee - SUMMER GLOW","BONNIE","DAPHNE","firecracker","GIGI","Indio Highlight Recover","Indio","KUTTI DEUX","LIGHTS-JAIPUR","Moana+","NATALIE","Orange County","Praias e desertos"],
    lookupFilterObj = GPUImageLookupFilter(),
    originalImage = GPUImagePicture(),
    filterImage = GPUImagePicture(),
    objSelectSaveExport = ImageVideoModel()
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- Collection View Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arraySelectImageVideo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePhotoCVC", for: indexPath) as! HomePhotoCVC
        let detail = arraySelectImageVideo[indexPath.item]
        if detail.isVideoSelected {
            cell.imgVwPhoto.image = detail.videoThumbhImage
            cell.imgVwVideoIcon.isHidden = false
        } else {
            cell.imgVwPhoto.image = detail.imageSelect
            cell.imgVwVideoIcon.isHidden = true
        }
        if detail.isSelected {
            cell.imgVwPhoto.layer.borderWidth = 4
            cell.imgVwPhoto.layer.borderColor = #colorLiteral(red: 0.8549019608, green: 0.4235294118, blue: 0.2549019608, alpha: 1)
        } else {
            cell.imgVwPhoto.layer.borderWidth = 0
            cell.imgVwPhoto.layer.borderColor = UIColor.clear.cgColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arraySelectImageVideo[indexPath.item].isSelected {
            arraySelectImageVideo[indexPath.item].isSelected = false
        } else {
            arraySelectImageVideo[indexPath.item].isSelected = true
        }
        colVwPhotoList.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/4) - 10, height: (collectionView.frame.width/4) - 10)
    }
    
}
extension HomeVC: PassImageDelegate,SaveDataHomeScreenDelegate,SaveAndShareDelegate {
    
    //MARK:- Select Image Delegate Method
    func passSelectedImage(selectImage: UIImage) {
        let objImageVideoModel = ImageVideoModel()
        objImageVideoModel.isVideoSelected = false
        objImageVideoModel.imageSelect = selectImage.fixedOrientation()
    
        objImageVideoModel.idMedia = generateRandomString
        
        saveDataInDB(objDetailMedia: objImageVideoModel)
        arraySelectImageVideo.append(objImageVideoModel)
        colVwPhotoList.reloadData()
    }
    
    //MARK:- Select Video Delegate Method
    func passSelectedVideo(videoUrl: URL,isCamera: Bool) {
        let videoThumbImg = Proxy.shared.getVideoThumbnailImageFromVideo(videoUrl)
        let objImageVideoModel = ImageVideoModel()
        objImageVideoModel.isVideoSelected = true
        objImageVideoModel.videoUrl = videoUrl
        objImageVideoModel.videoThumbhImage = videoThumbImg
        objImageVideoModel.idMedia = generateRandomString
        
        let videoPath = "\(Date().currentTimeStamp)Video.mp4"
        saveVideoDocumentDirectory(videoUrl, videoLstPathComp: videoPath)
        objImageVideoModel.videoUrlInString = videoPath
        
        saveDataInDB(objDetailMedia: objImageVideoModel)
        arraySelectImageVideo.append(objImageVideoModel)
        colVwPhotoList.reloadData()
    }
    
    //MARK:- Save Data In Database Delegate Method
    func saveDataInDatabase(_ objImageVideoModel: ImageVideoModel,selectIndex: Int) {
        if selectIndex == -1 {
            arraySelectImageVideo.append(objImageVideoModel)
        } else {
            arraySelectImageVideo[selectIndex] = objImageVideoModel
        }
        updateMediaDataFromDatabase(objImageVideoModel,selectIndex: selectIndex)
        DispatchQueue.main.async {
            self.colVwPhotoList.reloadData()
        }
    }
    
    //MARK:- Save Data In Gellery Delegate Method
    func SaveItemInGallery(_ isSave: Bool) {
        if isSave {
            //Save Video
            if objHomeVM.objSelectSaveExport.isVideoSelected {
                self.displayAlertMessage(AppAlerts.titleValue.videoSavedGallery)
                PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                    if authorizationStatus == .authorized {
                        if self.objHomeVM.objSelectSaveExport.videoUrl != nil {
                            PHPhotoLibrary.shared().saveVideoInGallery(videoUrl: self.objHomeVM.objSelectSaveExport.videoUrl!, albumName: "FeedGoals Videos") { (phassert) in
                                
                            }
                        }
                    }
                })
            } else {
                self.displayAlertMessage(AppAlerts.titleValue.imageSavedGallery)
                PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                    if authorizationStatus == .authorized {
                        PHPhotoLibrary.shared().savePhotoInGallery(image: self.objHomeVM.objSelectSaveExport.imageSelect, albumName: "FeedGoals Images") { (phassert) in
                            
                        }
                    }
                })
            }
        } else {
            if objHomeVM.objSelectSaveExport.isVideoSelected {
                self.shareVideoButton(objHomeVM.objSelectSaveExport.videoUrl!)
            } else {
                self.shareImageButton(objHomeVM.objSelectSaveExport.imageSelect)
            }
        }
    }
    
}
extension HomeVC {
    
    func saveDataInDB(objDetailMedia: ImageVideoModel) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "MediaTable", in: managedContext)!
        let objMediaTable = NSManagedObject(entity: entity, insertInto: managedContext)
        objMediaTable.setValue(objDetailMedia.isVideoSelected, forKeyPath: "isVideoSelected")
        objMediaTable.setValue(objDetailMedia.idMedia, forKeyPath: "id")
        if objDetailMedia.isVideoSelected {
            let thumbImageData = objDetailMedia.videoThumbhImage.pngData()
            objMediaTable.setValue(thumbImageData, forKeyPath: "videoThumbhImage")
            objMediaTable.setValue(objDetailMedia.videoUrlInString, forKeyPath: "videoUrl")
        } else {
            let imageData = objDetailMedia.imageSelect.pngData()
            objMediaTable.setValue(imageData, forKeyPath: "imageSelectUrl")
        }
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveVideoDocumentDirectory(_ videoUrl: URL,videoLstPathComp: String) {
        let videoData = NSData(contentsOf: videoUrl)
        let path = try! FileManager.default.url(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: false)
        let filepath = path.appendingPathComponent(videoLstPathComp)
        do {
            try videoData?.write(to: filepath)
        } catch {
            print(error)
        }
    }
    
    func updateMediaDataFromDatabase(_ objDetail: ImageVideoModel,selectIndex: Int) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MediaTable")
        fetchRequest.predicate = NSPredicate(format: "id = %@", objDetail.idMedia)
        do {
            let detailMediaTable = try managedContext.fetch(fetchRequest)
            let updateObject = detailMediaTable[0]
            if objDetail.isVideoSelected {
                let videoThumbImageData = objDetail.videoThumbhImage.pngData()
                let videoPath = "\(Date().currentTimeStamp)Video.mp4"
                saveVideoDocumentDirectory(objDetail.videoUrl!, videoLstPathComp: videoPath)
                updateObject.setValue(videoThumbImageData, forKey: "videoThumbhImage")
                updateObject.setValue(videoPath, forKey: "videoUrl")
                arraySelectImageVideo[selectIndex].videoUrlInString = videoPath
            } else {
                let imageData = objDetail.imageSelect.pngData()
                updateObject.setValue(imageData, forKey: "imageSelectUrl")
            }
            do {
                try managedContext.save()
            }
            catch {
                print(error)
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func removeMediaFromDatabase(_ arrayDetail: [ImageVideoModel]) {
        for i in 0..<arrayDetail.count {
            let objDetail = arrayDetail[i]
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MediaTable")
            fetchRequest.predicate = NSPredicate(format: "id = %@", objDetail.idMedia)
            do {
                let detailMediaTable = try managedContext.fetch(fetchRequest)
                let deleteObject = detailMediaTable[0]
                managedContext.delete(deleteObject)
                do {
                    try managedContext.save()
                }
                catch {
                    print(error)
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
        }
    }
}

