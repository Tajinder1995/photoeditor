//
//  ImageCustomFilter.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 15/08/20.
//  Copyright © 2020 netset. All rights reserved.


import UIKit
import GPUImage

extension EditingVC {
    
    func blendWithColorAndRect(normalImage: UIImage,filterImage: UIImage,value:Float) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: normalImage.size.width, height: normalImage.size.height)
        UIGraphicsBeginImageContextWithOptions(imgVwEditing.image!.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(UIColor.white.cgColor)
        context!.fill(rect)
        normalImage.draw(in: rect, blendMode: .normal, alpha: 1)
        filterImage.draw(in: rect, blendMode: .softLight, alpha: CGFloat(value))
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func blendWithColorAndRectDustFilter(normalImage: UIImage,filterImage: UIImage,value:Float) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: normalImage.size.width, height: normalImage.size.height)
        UIGraphicsBeginImageContextWithOptions(imgVwEditing.image!.size, false, 1.0)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(UIColor.white.cgColor)
        context!.fill(rect)
        normalImage.draw(in: rect, blendMode: .normal, alpha: 1)
        filterImage.draw(in: rect, blendMode: .lighten, alpha: CGFloat(value))
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func applyVideoFilterMethod(filterClick: Int,fileURL: URL,isSlide: Bool) {
        if isSlide {
            objEditingVM.avPlayerController.player?.pause()
            btnPlayVideo.isSelected = false
        }
        let vidAsset = AVURLAsset(url: fileURL, options: nil)
        let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
            switch filterClick {
            case BottomFilters.Presets:
                if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    let source = request.sourceImage.clampedToExtent()
                    if self.objEditingVM.objVideoFilterModel.filterNameEdit == EditFilter.grain {
                        guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                            kCIInputImageKey: source,
                            kCIInputIntensityKey: self.objEditingVM.objVideoFilterModel.filterValEdit
                        ]) else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let sepiaCIImage = sepiaFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                            let noiseImage = coloredNoise.outputImage else {
                                print("App bundle doesn't exist")
                                return
                        }
                        
                        let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        let fineGrain = CIVector(x:0, y: self.objEditingVM.objVideoFilterModel.filterValEdit, z:0, w:0)
                        let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        
                        guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                             parameters:
                            [
                                kCIInputImageKey: noiseImage,
                                "inputRVector": whitenVector,
                                "inputGVector": whitenVector,
                                "inputBVector": whitenVector,
                                "inputAVector": fineGrain,
                                "inputBiasVector": zeroVector
                                
                        ]), let whiteSpecks = whiteningFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                             parameters:
                            [
                                kCIInputImageKey: whiteSpecks,
                                kCIInputBackgroundImageKey: sepiaCIImage
                        ]) , let speckledImage = speckCompositor.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
                        let transformedNoise = noiseImage.transformed(by: verticalScale)
                        
                        let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
                        let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
                        
                        guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                            kCIInputImageKey: transformedNoise,
                            "inputRVector": darkenVector,
                            "inputGVector": zeroVector,
                            "inputBVector": zeroVector,
                            "inputAVector": zeroVector,
                            "inputBiasVector": darkenBias
                        ]),
                            let randomScratches = darkeningFilter.outputImage else {
                                return
                        }
                        
                        guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                            [
                                kCIInputImageKey: randomScratches
                        ]),
                            let darkScratches = grayscaleFilter.outputImage
                            else {
                                return
                        }
                        
                        guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                            [
                                kCIInputImageKey: darkScratches,
                                kCIInputBackgroundImageKey: speckledImage
                        ]),
                            let oldFilmImage = oldFilmCompositor.outputImage
                            else {
                                return
                        }
                        self.objEditingVM.objVideoFilterModel.editCiImage = oldFilmImage.cropped(to: request.sourceImage.extent)
                    } else {
                        let editFilter = CIFilter(name: self.objEditingVM.objVideoFilterModel.filterKeyEdit)!
                        editFilter.setValue(source, forKey: kCIInputImageKey)
                        switch self.objEditingVM.objVideoFilterModel.filterNameEdit {
                        case EditFilter.tint:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 200), forKey: "inputTargetNeutral")
                        case EditFilter.temperature:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 100), forKey: "inputNeutral")
                        case EditFilter.vignette:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: "inputRadius")
                            editFilter.setValue(1.0, forKey: "inputIntensity")
                        default:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: self.objEditingVM.objVideoFilterModel.filterInputkeyEdit)
                        }
                        let outputEdit = editFilter.outputImage!.cropped(to: request.sourceImage.extent)
                        self.objEditingVM.objVideoFilterModel.editCiImage = outputEdit
                    }
                }
                if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objMultiBandHSV = MultiBandHSV()
                    let filter = self.objEditingVM.objMultiBandHSV
                    if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                        filter.inputImage = self.objEditingVM.objVideoFilterModel.editCiImage
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        filter.inputImage = source
                    }
                    switch self.objEditingVM.objVideoFilterModel.colorApplyIndex {
                    case 0:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 1:
                        filter.inputOrangeShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 2:
                        filter.inputYellowShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 3:
                        filter.inputGreenShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 4:
                        filter.inputAquaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 5:
                        filter.inputBlueShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 6:
                        filter.inputPurpleShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 7:
                        filter.inputMagentaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    default:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    }
                    let outputColor = filter.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.colorCiImage = outputColor
                }
                if self.objEditingVM.objVideoFilterModel.isEditVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.colorCiImage
                } else if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.editCiImage
                } else if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.colorCiImage
                } else {
                    let source = request.sourceImage.clampedToExtent()
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = source
                }
                let filterPreset = self.colorCubeFilterFromLUT(imageName: self.objEditingVM.objVideoFilterModel.presetFilterName)
                filterPreset?.setValue(self.objEditingVM.objVideoFilterModel.lastFilterCiImage, forKey: kCIInputImageKey)
                let outputPreset = filterPreset?.outputImage!.cropped(to: request.sourceImage.extent)
                self.objEditingVM.objVideoFilterModel.presetCiImage = outputPreset!
                request.finish(with: self.objEditingVM.objVideoFilterModel.presetCiImage, context: nil)
            case BottomFilters.Edit:
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    let filter = self.colorCubeFilterFromLUT(imageName: self.objEditingVM.objVideoFilterModel.presetFilterName)
                    let source = request.sourceImage.clampedToExtent()
                    filter?.setValue(source, forKey: kCIInputImageKey)
                    let outputPreset = filter?.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.presetCiImage = outputPreset!
                }
                if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objMultiBandHSV = MultiBandHSV()
                    let filter = self.objEditingVM.objMultiBandHSV
                    if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                        filter.inputImage = self.objEditingVM.objVideoFilterModel.presetCiImage
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        filter.inputImage = source
                    }
                    switch self.objEditingVM.objVideoFilterModel.colorApplyIndex {
                    case 0:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 1:
                        filter.inputOrangeShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 2:
                        filter.inputYellowShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 3:
                        filter.inputGreenShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 4:
                        filter.inputAquaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 5:
                        filter.inputBlueShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 6:
                        filter.inputPurpleShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 7:
                        filter.inputMagentaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    default:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    }
                    let outputColor = filter.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.colorCiImage = outputColor
                }
                
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.colorCiImage
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.presetCiImage
                } else if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.colorCiImage
                } else {
                    let source = request.sourceImage.clampedToExtent()
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = source
                }
                if self.objEditingVM.objVideoFilterModel.filterNameEdit == EditFilter.grain {
                    guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                        kCIInputImageKey: self.objEditingVM.objVideoFilterModel.lastFilterCiImage,
                        kCIInputIntensityKey: self.objEditingVM.objVideoFilterModel.filterValEdit
                    ]) else {
                        print("App bundle doesn't exist")
                        return
                    }
                    
                    guard let sepiaCIImage = sepiaFilter.outputImage else {
                        print("App bundle doesn't exist")
                        return
                    }
                    
                    guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                        let noiseImage = coloredNoise.outputImage else {
                            print("App bundle doesn't exist")
                            return
                    }
                    
                    let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                    let fineGrain = CIVector(x:0, y: self.objEditingVM.objVideoFilterModel.filterValEdit, z:0, w:0)
                    let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                    
                    guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                         parameters:
                        [
                            kCIInputImageKey: noiseImage,
                            "inputRVector": whitenVector,
                            "inputGVector": whitenVector,
                            "inputBVector": whitenVector,
                            "inputAVector": fineGrain,
                            "inputBiasVector": zeroVector
                            
                    ]), let whiteSpecks = whiteningFilter.outputImage else {
                        print("App bundle doesn't exist")
                        return
                    }
                    guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                         parameters:
                        [
                            kCIInputImageKey: whiteSpecks,
                            kCIInputBackgroundImageKey: sepiaCIImage
                    ]) , let speckledImage = speckCompositor.outputImage else {
                        print("App bundle doesn't exist")
                        return
                    }
                    
                    let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
                    let transformedNoise = noiseImage.transformed(by: verticalScale)
                    
                    let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
                    let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
                    
                    guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                        kCIInputImageKey: transformedNoise,
                        "inputRVector": darkenVector,
                        "inputGVector": zeroVector,
                        "inputBVector": zeroVector,
                        "inputAVector": zeroVector,
                        "inputBiasVector": darkenBias
                    ]),
                        let randomScratches = darkeningFilter.outputImage else {
                            return
                    }
                    
                    guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                        [
                            kCIInputImageKey: randomScratches
                    ]),
                        let darkScratches = grayscaleFilter.outputImage
                        else {
                            return
                    }
                    
                    guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                        [
                            kCIInputImageKey: darkScratches,
                            kCIInputBackgroundImageKey: speckledImage
                    ]),
                        let oldFilmImage = oldFilmCompositor.outputImage
                        else {
                            return
                    }
                    self.objEditingVM.objVideoFilterModel.editCiImage = oldFilmImage.cropped(to: request.sourceImage.extent)
                } else {
                    let editFilter = CIFilter(name: self.objEditingVM.objVideoFilterModel.filterKeyEdit)!
                    editFilter.setValue(self.objEditingVM.objVideoFilterModel.lastFilterCiImage, forKey: kCIInputImageKey)
                    switch self.objEditingVM.objVideoFilterModel.filterNameEdit {
                    case EditFilter.tint:
                        let seconds = CMTimeGetSeconds(request.compositionTime)
                        editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 200), forKey: "inputTargetNeutral")
                    case EditFilter.temperature:
                        let seconds = CMTimeGetSeconds(request.compositionTime)
                        editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 100), forKey: "inputNeutral")
                    case EditFilter.vignette:
                        editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: "inputRadius")
                        editFilter.setValue(1.0, forKey: "inputIntensity")
                    default:
                        editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: self.objEditingVM.objVideoFilterModel.filterInputkeyEdit)
                    }
                    let outputEdit = editFilter.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.editCiImage = outputEdit
                }
                request.finish(with: self.objEditingVM.objVideoFilterModel.editCiImage, context: nil)
            case BottomFilters.Color:
                if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    let source = request.sourceImage.clampedToExtent()
                    if self.objEditingVM.objVideoFilterModel.filterNameEdit == EditFilter.grain {
                        guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                            kCIInputImageKey: source,
                            kCIInputIntensityKey: self.objEditingVM.objVideoFilterModel.filterValEdit
                        ]) else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let sepiaCIImage = sepiaFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                            let noiseImage = coloredNoise.outputImage else {
                                print("App bundle doesn't exist")
                                return
                        }
                        
                        let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        let fineGrain = CIVector(x:0, y: self.objEditingVM.objVideoFilterModel.filterValEdit, z:0, w:0)
                        let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        
                        guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                             parameters:
                            [
                                kCIInputImageKey: noiseImage,
                                "inputRVector": whitenVector,
                                "inputGVector": whitenVector,
                                "inputBVector": whitenVector,
                                "inputAVector": fineGrain,
                                "inputBiasVector": zeroVector
                                
                        ]), let whiteSpecks = whiteningFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                             parameters:
                            [
                                kCIInputImageKey: whiteSpecks,
                                kCIInputBackgroundImageKey: sepiaCIImage
                        ]) , let speckledImage = speckCompositor.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
                        let transformedNoise = noiseImage.transformed(by: verticalScale)
                        
                        let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
                        let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
                        
                        guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                            kCIInputImageKey: transformedNoise,
                            "inputRVector": darkenVector,
                            "inputGVector": zeroVector,
                            "inputBVector": zeroVector,
                            "inputAVector": zeroVector,
                            "inputBiasVector": darkenBias
                        ]),
                            let randomScratches = darkeningFilter.outputImage else {
                                return
                        }
                        
                        guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                            [
                                kCIInputImageKey: randomScratches
                        ]),
                            let darkScratches = grayscaleFilter.outputImage
                            else {
                                return
                        }
                        
                        guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                            [
                                kCIInputImageKey: darkScratches,
                                kCIInputBackgroundImageKey: speckledImage
                        ]),
                            let oldFilmImage = oldFilmCompositor.outputImage
                            else {
                                return
                        }
                        self.objEditingVM.objVideoFilterModel.editCiImage = oldFilmImage.cropped(to: request.sourceImage.extent)
                    } else {
                        let editFilter = CIFilter(name: self.objEditingVM.objVideoFilterModel.filterKeyEdit)!
                        editFilter.setValue(source, forKey: kCIInputImageKey)
                        switch self.objEditingVM.objVideoFilterModel.filterNameEdit {
                        case EditFilter.tint:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 200), forKey: "inputTargetNeutral")
                        case EditFilter.temperature:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 100), forKey: "inputNeutral")
                        case EditFilter.vignette:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: "inputRadius")
                            editFilter.setValue(1.0, forKey: "inputIntensity")
                        default:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: self.objEditingVM.objVideoFilterModel.filterInputkeyEdit)
                        }
                        let outputEdit = editFilter.outputImage!.cropped(to: request.sourceImage.extent)
                        self.objEditingVM.objVideoFilterModel.editCiImage = outputEdit
                    }
                }
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    let filter = self.colorCubeFilterFromLUT(imageName: self.objEditingVM.objVideoFilterModel.presetFilterName)
                    if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                        filter?.setValue(self.objEditingVM.objVideoFilterModel.editCiImage, forKey: kCIInputImageKey)
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        filter?.setValue(source, forKey: kCIInputImageKey)
                    }
                    let outputPreset = filter?.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.presetCiImage = outputPreset!
                }
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.presetCiImage
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.presetCiImage
                } else if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.editCiImage
                } else {
                    let source = request.sourceImage.clampedToExtent()
                    self.objEditingVM.objVideoFilterModel.lastFilterCiImage = source
                }
                self.objEditingVM.objMultiBandHSV = MultiBandHSV()
                let filter = self.objEditingVM.objMultiBandHSV
                filter.inputImage = self.objEditingVM.objVideoFilterModel.lastFilterCiImage
                switch self.objEditingVM.objVideoFilterModel.colorApplyIndex {
                case 0:
                    filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 1:
                    filter.inputOrangeShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 2:
                    filter.inputYellowShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 3:
                    filter.inputGreenShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 4:
                    filter.inputAquaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 5:
                    filter.inputBlueShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 6:
                    filter.inputPurpleShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                case 7:
                    filter.inputMagentaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                default:
                    filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                }
                let outputColor = filter.outputImage!.cropped(to: request.sourceImage.extent)
                self.objEditingVM.objVideoFilterModel.colorCiImage = outputColor
                request.finish(with: self.objEditingVM.objVideoFilterModel.colorCiImage, context: nil)
            default:
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    let filter = self.colorCubeFilterFromLUT(imageName: self.objEditingVM.objVideoFilterModel.presetFilterName)
                    let source = request.sourceImage.clampedToExtent()
                    filter?.setValue(source, forKey: kCIInputImageKey)
                    let outputPreset = filter?.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.presetCiImage = outputPreset!
                }
                if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objMultiBandHSV = MultiBandHSV()
                    let filter = self.objEditingVM.objMultiBandHSV
                    if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                        filter.inputImage = self.objEditingVM.objVideoFilterModel.presetCiImage
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        filter.inputImage = source
                    }
                    switch self.objEditingVM.objVideoFilterModel.colorApplyIndex {
                    case 0:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 1:
                        filter.inputOrangeShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 2:
                        filter.inputYellowShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 3:
                        filter.inputGreenShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 4:
                        filter.inputAquaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 5:
                        filter.inputBlueShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 6:
                        filter.inputPurpleShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 7:
                        filter.inputMagentaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    default:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    }
                    let outputColor = filter.outputImage!.cropped(to: request.sourceImage.extent)
                    self.objEditingVM.objVideoFilterModel.colorCiImage = outputColor
                }
                if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                        self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.colorCiImage
                    } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                        self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.presetCiImage
                    } else if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                        self.objEditingVM.objVideoFilterModel.lastFilterCiImage = self.objEditingVM.objVideoFilterModel.colorCiImage
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        self.objEditingVM.objVideoFilterModel.lastFilterCiImage = source
                    }
                    if self.objEditingVM.objVideoFilterModel.filterNameEdit == EditFilter.grain {
                        guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                            kCIInputImageKey: self.objEditingVM.objVideoFilterModel.lastFilterCiImage,
                            kCIInputIntensityKey: self.objEditingVM.objVideoFilterModel.filterValEdit
                        ]) else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let sepiaCIImage = sepiaFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                            let noiseImage = coloredNoise.outputImage else {
                                print("App bundle doesn't exist")
                                return
                        }
                        
                        let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        let fineGrain = CIVector(x:0, y: self.objEditingVM.objVideoFilterModel.filterValEdit, z:0, w:0)
                        let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        
                        guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                             parameters:
                            [
                                kCIInputImageKey: noiseImage,
                                "inputRVector": whitenVector,
                                "inputGVector": whitenVector,
                                "inputBVector": whitenVector,
                                "inputAVector": fineGrain,
                                "inputBiasVector": zeroVector
                                
                        ]), let whiteSpecks = whiteningFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                             parameters:
                            [
                                kCIInputImageKey: whiteSpecks,
                                kCIInputBackgroundImageKey: sepiaCIImage
                        ]) , let speckledImage = speckCompositor.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
                        let transformedNoise = noiseImage.transformed(by: verticalScale)
                        
                        let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
                        let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
                        
                        guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                            kCIInputImageKey: transformedNoise,
                            "inputRVector": darkenVector,
                            "inputGVector": zeroVector,
                            "inputBVector": zeroVector,
                            "inputAVector": zeroVector,
                            "inputBiasVector": darkenBias
                        ]),
                            let randomScratches = darkeningFilter.outputImage else {
                                return
                        }
                        
                        guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                            [
                                kCIInputImageKey: randomScratches
                        ]),
                            let darkScratches = grayscaleFilter.outputImage
                            else {
                                return
                        }
                        
                        guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                            [
                                kCIInputImageKey: darkScratches,
                                kCIInputBackgroundImageKey: speckledImage
                        ]),
                            let oldFilmImage = oldFilmCompositor.outputImage
                            else {
                                return
                        }
                        self.objEditingVM.objVideoFilterModel.editCiImage = oldFilmImage.cropped(to: request.sourceImage.extent)
                    } else {
                        let editFilter = CIFilter(name: self.objEditingVM.objVideoFilterModel.filterKeyEdit)!
                        editFilter.setValue(self.objEditingVM.objVideoFilterModel.lastFilterCiImage, forKey: kCIInputImageKey)
                        switch self.objEditingVM.objVideoFilterModel.filterNameEdit {
                        case EditFilter.tint:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 200), forKey: "inputTargetNeutral")
                        case EditFilter.temperature:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 100), forKey: "inputNeutral")
                        case EditFilter.vignette:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: "inputRadius")
                            editFilter.setValue(1.0, forKey: "inputIntensity")
                        default:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: self.objEditingVM.objVideoFilterModel.filterInputkeyEdit)
                        }
                        let outputEdit = editFilter.outputImage!.cropped(to: request.sourceImage.extent)
                        self.objEditingVM.objVideoFilterModel.editCiImage = outputEdit
                    }
                }
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply && self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    request.finish(with: self.objEditingVM.objVideoFilterModel.editCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    request.finish(with: self.objEditingVM.objVideoFilterModel.colorCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isEffectVideoApply {
                    request.finish(with: self.objEditingVM.objVideoFilterModel.editCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    request.finish(with: self.objEditingVM.objVideoFilterModel.presetCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    request.finish(with: self.objEditingVM.objVideoFilterModel.editCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    request.finish(with: self.objEditingVM.objVideoFilterModel.colorCiImage, context: nil)
                }
            }
        })
        objEditingVM.playerItem.videoComposition = compositionNew
        objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
    }
    
    func saveAndShareVideoMethod(fileURL: URL,completion:@escaping(_ url: URL,_ isfilter:Bool) -> Void) {
        if !objEditingVM.objVideoFilterModel.isPresetVideoApply && !objEditingVM.objVideoFilterModel.isEditVideoApply && !objEditingVM.objVideoFilterModel.isColorVideoApply {
            completion(fileURL, false)
        } else {
            var presetCiImage = CIImage(),
            editCiImage = CIImage(),
            colorCiImage = CIImage(),
            lastFilterCiImage = CIImage()
            let vidAsset = AVURLAsset(url: fileURL, options: nil)
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    let filter = self.colorCubeFilterFromLUT(imageName: self.objEditingVM.objVideoFilterModel.presetFilterName)
                    let source = request.sourceImage.clampedToExtent()
                    filter?.setValue(source, forKey: kCIInputImageKey)
                    let outputPreset = filter?.outputImage!.cropped(to: request.sourceImage.extent)
                    presetCiImage = outputPreset!
                }
                if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    self.objEditingVM.objMultiBandHSV = MultiBandHSV()
                    let filter = self.objEditingVM.objMultiBandHSV
                    if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                        filter.inputImage = presetCiImage
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        filter.inputImage = source
                    }
                    switch self.objEditingVM.objVideoFilterModel.colorApplyIndex {
                    case 0:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 1:
                        filter.inputOrangeShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 2:
                        filter.inputYellowShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 3:
                        filter.inputGreenShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 4:
                        filter.inputAquaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 5:
                        filter.inputBlueShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 6:
                        filter.inputPurpleShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    case 7:
                        filter.inputMagentaShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    default:
                        filter.inputRedShift = CIVector(x: self.objEditingVM.objVideoFilterModel.colorHue, y: self.objEditingVM.objVideoFilterModel.colorSaturation, z: self.objEditingVM.objVideoFilterModel.colorLuminance)
                    }
                    let outputColor = filter.outputImage!.cropped(to: request.sourceImage.extent)
                    colorCiImage = outputColor
                }
                if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                        lastFilterCiImage = colorCiImage
                    } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                        lastFilterCiImage = presetCiImage
                    } else if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                        lastFilterCiImage = colorCiImage
                    } else {
                        let source = request.sourceImage.clampedToExtent()
                        self.objEditingVM.objVideoFilterModel.lastFilterCiImage = source
                    }
                    if self.objEditingVM.objVideoFilterModel.filterNameEdit == EditFilter.grain {
                        guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                            kCIInputImageKey: lastFilterCiImage,
                            kCIInputIntensityKey: self.objEditingVM.objVideoFilterModel.filterValEdit
                        ]) else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let sepiaCIImage = sepiaFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                            let noiseImage = coloredNoise.outputImage else {
                                print("App bundle doesn't exist")
                                return
                        }
                        
                        let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        let fineGrain = CIVector(x:0, y: self.objEditingVM.objVideoFilterModel.filterValEdit, z:0, w:0)
                        let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                        
                        guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                             parameters:
                            [
                                kCIInputImageKey: noiseImage,
                                "inputRVector": whitenVector,
                                "inputGVector": whitenVector,
                                "inputBVector": whitenVector,
                                "inputAVector": fineGrain,
                                "inputBiasVector": zeroVector
                                
                        ]), let whiteSpecks = whiteningFilter.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                             parameters:
                            [
                                kCIInputImageKey: whiteSpecks,
                                kCIInputBackgroundImageKey: sepiaCIImage
                        ]) , let speckledImage = speckCompositor.outputImage else {
                            print("App bundle doesn't exist")
                            return
                        }
                        
                        let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
                        let transformedNoise = noiseImage.transformed(by: verticalScale)
                        
                        let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
                        let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
                        
                        guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                            kCIInputImageKey: transformedNoise,
                            "inputRVector": darkenVector,
                            "inputGVector": zeroVector,
                            "inputBVector": zeroVector,
                            "inputAVector": zeroVector,
                            "inputBiasVector": darkenBias
                        ]),
                            let randomScratches = darkeningFilter.outputImage else {
                                return
                        }
                        
                        guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                            [
                                kCIInputImageKey: randomScratches
                        ]),
                            let darkScratches = grayscaleFilter.outputImage
                            else {
                                return
                        }
                        
                        guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                            [
                                kCIInputImageKey: darkScratches,
                                kCIInputBackgroundImageKey: speckledImage
                        ]),
                            let oldFilmImage = oldFilmCompositor.outputImage
                            else {
                                return
                        }
                        editCiImage = oldFilmImage.cropped(to: request.sourceImage.extent)
                    } else {
                        let editFilter = CIFilter(name: self.objEditingVM.objVideoFilterModel.filterKeyEdit)!
                        editFilter.setValue(lastFilterCiImage, forKey: kCIInputImageKey)
                        switch self.objEditingVM.objVideoFilterModel.filterNameEdit {
                        case EditFilter.tint:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 200), forKey: "inputTargetNeutral")
                        case EditFilter.temperature:
                            let seconds = CMTimeGetSeconds(request.compositionTime)
                            editFilter.setValue(CIVector(x: CGFloat(seconds) * self.objEditingVM.objVideoFilterModel.filterValEdit, y: 100), forKey: "inputNeutral")
                        case EditFilter.vignette:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: "inputRadius")
                            editFilter.setValue(1.0, forKey: "inputIntensity")
                        default:
                            editFilter.setValue(self.objEditingVM.objVideoFilterModel.filterValEdit, forKey: self.objEditingVM.objVideoFilterModel.filterInputkeyEdit)
                        }
                        let outputEdit = editFilter.outputImage!.cropped(to: request.sourceImage.extent)
                        editCiImage = outputEdit
                    }
                }
                if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply && self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    request.finish(with: editCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    request.finish(with: colorCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply && self.objEditingVM.objVideoFilterModel.isEffectVideoApply {
                    request.finish(with: editCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isPresetVideoApply {
                    request.finish(with: presetCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isEditVideoApply {
                    request.finish(with: editCiImage, context: nil)
                } else if self.objEditingVM.objVideoFilterModel.isColorVideoApply {
                    request.finish(with: colorCiImage, context: nil)
                }
            })
            DispatchQueue.main.async {
                self.saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "videoPath.mp4") { (urlVideo) in
                    completion(urlVideo, true)
                }
            }
        }
    }
    
    
    
}
