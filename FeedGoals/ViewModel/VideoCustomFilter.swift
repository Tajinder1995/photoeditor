//
//  VideoCustomFilter.swift
//  FeedGoals
//
//  Created by Sandeep Chaudhary on 13/08/20.
//  Copyright © 2020 netset. All rights reserved.
//

import UIKit
import AVKit
import Photos

extension EditingVC {
    
    func colorCubeFilterFromLUT(imageName : String) -> CIFilter? {
        let size = 64
        let lutImage    = UIImage(named: imageName)!.cgImage
        let lutWidth    = lutImage!.width
        let lutHeight   = lutImage!.height
        let rowCount    = lutHeight / size
        let columnCount = lutWidth / size
        if ((lutWidth % size != 0) || (lutHeight % size != 0) || (rowCount * columnCount != size)) {
            NSLog("Invalid colorLUT %@", imageName);
            return nil
        }
        let bitmap  = getBytesFromImage(image: UIImage(named: imageName))!
        let floatSize = MemoryLayout<Float>.size
        let cubeData = UnsafeMutablePointer<Float>.allocate(capacity: size * size * size * 4 * floatSize)
        var z = 0
        var bitmapOffset = 0
        for _ in 0 ..< rowCount {
            for y in 0 ..< size {
                let tmp = z
                for _ in 0 ..< columnCount {
                    for x in 0 ..< size {
                        
                        let alpha   = Float(bitmap[bitmapOffset]) / 255.0
                        let red     = Float(bitmap[bitmapOffset+1]) / 255.0
                        let green   = Float(bitmap[bitmapOffset+2]) / 255.0
                        let blue    = Float(bitmap[bitmapOffset+3]) / 255.0
                        
                        let dataOffset = (z * size * size + y * size + x) * 4
                        
                        cubeData[dataOffset + 3] = alpha
                        cubeData[dataOffset + 2] = red
                        cubeData[dataOffset + 1] = green
                        cubeData[dataOffset + 0] = blue
                        bitmapOffset += 4
                    }
                    z += 1
                }
                z = tmp
            }
            z += columnCount
        }
        let colorCubeData = NSData(bytesNoCopy: cubeData, length: size * size * size * 4 * floatSize, freeWhenDone: true)
        let filter = CIFilter(name: "CIColorCube")
        filter?.setValue(colorCubeData, forKey: "inputCubeData")
        filter?.setValue(size, forKey: "inputCubeDimension")
        return filter
    }
    
    func getBytesFromImage(image:UIImage?) -> [UInt8]? {
        var pixelValues: [UInt8]?
        if let imageRef = image?.cgImage {
            let width = Int(imageRef.width)
            let height = Int(imageRef.height)
            let bitsPerComponent = 8
            let bytesPerRow = width * 4
            let totalBytes = height * bytesPerRow
            let bitmapInfo = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            var intensities = [UInt8](repeating: 0, count: totalBytes)
            let contextRef = CGContext(data: &intensities, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
            contextRef?.draw(imageRef, in: CGRect(x: 0.0, y: 0.0, width: CGFloat(width), height: CGFloat(height)))
            
            pixelValues = intensities
        }
        return pixelValues!
    }
    
    func applyVideoPresetMethod(fileURL: URL,presetImage: String) {
        let vidAsset = AVURLAsset(url: fileURL as URL, options: nil)
        objEditingVM.videoPlayer = AVPlayer()
        objEditingVM.playerItem = AVPlayerItem(url: fileURL)
        let filter = self.colorCubeFilterFromLUT(imageName: presetImage)
        let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
            let source = request.sourceImage.clampedToExtent()
            filter?.setValue(source, forKey: kCIInputImageKey)
            let output = filter?.outputImage!.cropped(to: request.sourceImage.extent)
            request.finish(with: output!, context: nil)
        })
        objEditingVM.playerItem.videoComposition = compositionNew
        objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
        objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
        objEditingVM.avPlayerController.player?.play()
        loopPlayVideoMethod()
    }
    
    func saveVideoUrlFromApplyPresetFilter(fileURL: URL,presetImage: String,completion:@escaping(_ url: URL) -> Void) {
        let vidAsset = AVURLAsset(url: fileURL as URL, options: nil)
        let filter = self.colorCubeFilterFromLUT(imageName: presetImage)
        let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
            let source = request.sourceImage.clampedToExtent()
            filter?.setValue(source, forKey: kCIInputImageKey)
            let output = filter?.outputImage!.cropped(to: request.sourceImage.extent)
            request.finish(with: output!, context: nil)
        })
        let videoPath = "\(Date().currentTimeStamp)PresetVideo.mp4"
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as NSString
        let movieFilePath = docsDir.appendingPathComponent(videoPath)
        let movieDestinationUrl = NSURL(fileURLWithPath: movieFilePath)
        
        let assetExport = AVAssetExportSession(asset: vidAsset, presetName:AVAssetExportPresetHighestQuality)
        assetExport?.outputFileType = AVFileType.mov
        assetExport?.videoComposition = compositionNew
        
        do {
            try FileManager.default.removeItem(at:movieDestinationUrl as URL)
        } catch {
        }
        
        assetExport?.outputURL = movieDestinationUrl as URL
        assetExport?.exportAsynchronously(completionHandler: {
            switch assetExport!.status {
            case AVAssetExportSession.Status.failed:
                print("failed")
                print(assetExport?.error ?? "unknown error")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled")
                print(assetExport?.error ?? "unknown error")
            default:
                print("Movie complete")
                print(movieDestinationUrl as URL)
                completion(movieDestinationUrl.absoluteURL!)
            }
        })
    }
    
    func applyDustFlareEffectFilterVideoMethod(fileURL: URL,filterImage: UIImage) {
        Proxy.shared.showActivityIndicator()
        let asset = AVURLAsset(url: fileURL)
        let composition = AVMutableComposition()
        guard let compositionTrack = composition.addMutableTrack(
            withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid),
            let assetTrack = asset.tracks(withMediaType: .video).first
            else {
                print("Something is wrong with the asset.")
                return
        }
        do {
            let timeRange = CMTimeRange(start: .zero, duration: asset.duration)
            try compositionTrack.insertTimeRange(timeRange, of: assetTrack, at: .zero)
            if let audioAssetTrack = asset.tracks(withMediaType: .audio).first,
                let compositionAudioTrack = composition.addMutableTrack(
                    withMediaType: .audio,
                    preferredTrackID: kCMPersistentTrackID_Invalid) {
                try compositionAudioTrack.insertTimeRange(
                    timeRange,
                    of: audioAssetTrack,
                    at: .zero)
            }
        } catch {
            print(error)
            return
        }
        compositionTrack.preferredTransform = assetTrack.preferredTransform
        let videoInfo = orientation(from: assetTrack.preferredTransform)
        
        let videoSize: CGSize
        if videoInfo.isPortrait {
            videoSize = CGSize(width: assetTrack.naturalSize.height,height: assetTrack.naturalSize.width)
        } else {
            videoSize = assetTrack.naturalSize
        }
        
        let videoLayer = CALayer()
        videoLayer.frame = CGRect(origin: .zero, size: videoSize)
        videoLayer.contents = filterImage.cgImage
        videoLayer.compositingFilter = "softLightBlendMode"
        
        let overlayLayer = CALayer()
        overlayLayer.frame = CGRect(origin: .zero, size: videoSize)
        
        let outputLayer = CALayer()
        outputLayer.frame = CGRect(origin: .zero, size: videoSize)
        outputLayer.addSublayer(overlayLayer)
        outputLayer.addSublayer(videoLayer)
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = videoSize
        videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: overlayLayer,in: outputLayer)
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRange(start: .zero, duration: composition.duration)
        videoComposition.instructions = [instruction]
        let layerInstruction = compositionLayerInstruction(for: compositionTrack,assetTrack: assetTrack)
        instruction.layerInstructions = [layerInstruction]
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as NSString
        let movieFilePath = docsDir.appendingPathComponent("dustMoviewImage.mp4")
        let movieDestinationUrl = NSURL(fileURLWithPath: movieFilePath)
        
        let assetExport = AVAssetExportSession(asset: composition, presetName:AVAssetExportPresetHighestQuality)
        assetExport?.outputFileType = AVFileType.mov
        assetExport?.videoComposition = videoComposition
        do {
            try FileManager.default.removeItem(at:movieDestinationUrl as URL)
        } catch {
        }
        
        assetExport?.outputURL = movieDestinationUrl as URL
        assetExport?.exportAsynchronously(completionHandler: {
            Proxy.shared.hideActivityIndicator()
            switch assetExport!.status {
            case AVAssetExportSession.Status.failed:
                print("failed")
                print(assetExport?.error ?? "unknown error")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled")
                print(assetExport?.error ?? "unknown error")
            case AVAssetExportSession.Status.completed:
                print("Movie complete")
                self.objEditingVM.objVideoFilterModel.dustFlareEffectUrl = movieDestinationUrl.absoluteURL!
                self.objEditingVM.videoPlayer = AVPlayer()
                self.objEditingVM.playerItem = AVPlayerItem(url: movieDestinationUrl.absoluteURL!)
                self.objEditingVM.playerItem = AVPlayerItem(url: movieDestinationUrl.absoluteURL!)
                self.objEditingVM.videoPlayer.replaceCurrentItem(with: self.objEditingVM.playerItem)
                self.objEditingVM.avPlayerController.player = self.objEditingVM.videoPlayer
                self.objEditingVM.avPlayerController.player!.play()
                self.loopPlayVideoMethod()
            default:
                break
            }
        })
    }
    
    
    func applyEditFilterVideoMethod(fileURL: URL) {
        let detailEdit = objEditingVM.arrayEditFilter[objEditingVM.selectEditIndex]
        if detailEdit.0 == EditFilter.temperature {
            applyTempreatureFilterVideoMethod(fileURL, sliderValue: CGFloat(detailEdit.2), isSlide: false)
        } else if detailEdit.0 == EditFilter.tint {
            let vidAsset = AVURLAsset(url: fileURL as URL, options: nil)
            objEditingVM.videoPlayer = AVPlayer()
            objEditingVM.playerItem = AVPlayerItem(url: fileURL)
            let filter = CIFilter(name: detailEdit.3)!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                let seconds = CMTimeGetSeconds(request.compositionTime)
                filter.setValue(CIVector(x: CGFloat(seconds) * CGFloat(detailEdit.2), y: 200), forKey: "inputTargetNeutral")
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            objEditingVM.playerItem.videoComposition = compositionNew
            objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
            objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
            objEditingVM.avPlayerController.player?.play()
            loopPlayVideoMethod()
        } else if detailEdit.0 == EditFilter.grain {
            applyGrainFiterVideoMethod(fileURL, slideVal: CGFloat(detailEdit.2), isSlide: false)
        } else if detailEdit.0 == EditFilter.vignette {
            let vidAsset = AVURLAsset(url: fileURL as URL, options: nil)
            objEditingVM.videoPlayer = AVPlayer()
            objEditingVM.playerItem = AVPlayerItem(url: fileURL)
            let filter = CIFilter(name: detailEdit.3)!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                filter.setValue(100, forKey: "inputRadius")
                filter.setValue(1.0, forKey: "inputIntensity")
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            objEditingVM.playerItem.videoComposition = compositionNew
            objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
            objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
            objEditingVM.avPlayerController.player?.play()
            loopPlayVideoMethod()
        } else {
            let vidAsset = AVURLAsset(url: fileURL as URL, options: nil)
            objEditingVM.videoPlayer = AVPlayer()
            objEditingVM.playerItem = AVPlayerItem(url: fileURL)
            let filter = CIFilter(name: detailEdit.3)!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                filter.setValue(detailEdit.2, forKey: detailEdit.4)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            objEditingVM.playerItem.videoComposition = compositionNew
            objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
            objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
            objEditingVM.avPlayerController.player?.play()
            loopPlayVideoMethod()
        }
    }
    
    func editFilterOfSaveVideo(filterName: String,fileURL: URL,filterKey: String,filterInputkey: String,filterVal: CGFloat,completion:@escaping(_ url: URL) -> Void) {
        let vidAsset = AVURLAsset(url: fileURL as URL, options: nil)
        if filterName == EditFilter.temperature {
            let filter = CIFilter(name: "CITemperatureAndTint")!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: "inputImage")
                let seconds = CMTimeGetSeconds(request.compositionTime)
                filter.setValue(CIVector(x: CGFloat(seconds) * filterVal, y: 100), forKey: "inputNeutral")
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset:vidAsset, videofilePath: "editMoviewImage.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        } else if filterName == EditFilter.tint {
            let filter = CIFilter(name: "CITemperatureAndTint")!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                let seconds = CMTimeGetSeconds(request.compositionTime)
                filter.setValue(CIVector(x: CGFloat(seconds) * CGFloat(filterVal), y: 200), forKey: "inputTargetNeutral")
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset:vidAsset, videofilePath: "editMoviewImage.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        } else if filterName == EditFilter.grain {
            let composition = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                    kCIInputImageKey: source,
                    kCIInputIntensityKey: filterVal
                ]) else {
                    print("App bundle doesn't exist")
                    return
                }
                
                guard let sepiaCIImage = sepiaFilter.outputImage else {
                    print("App bundle doesn't exist")
                    return
                }
                
                guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                    let noiseImage = coloredNoise.outputImage else {
                        print("App bundle doesn't exist")
                        return
                }
                
                let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                let fineGrain = CIVector(x:0, y: filterVal, z:0, w:0)
                let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
                
                guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                     parameters:
                    [
                        kCIInputImageKey: noiseImage,
                        "inputRVector": whitenVector,
                        "inputGVector": whitenVector,
                        "inputBVector": whitenVector,
                        "inputAVector": fineGrain,
                        "inputBiasVector": zeroVector
                        
                ]), let whiteSpecks = whiteningFilter.outputImage else {
                    print("App bundle doesn't exist")
                    return
                }
                guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                     parameters:
                    [
                        kCIInputImageKey: whiteSpecks,
                        kCIInputBackgroundImageKey: sepiaCIImage
                ]) , let speckledImage = speckCompositor.outputImage else {
                    print("App bundle doesn't exist")
                    return
                }
                
                let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
                let transformedNoise = noiseImage.transformed(by: verticalScale)
                
                let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
                let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
                
                guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                    kCIInputImageKey: transformedNoise,
                    "inputRVector": darkenVector,
                    "inputGVector": zeroVector,
                    "inputBVector": zeroVector,
                    "inputAVector": zeroVector,
                    "inputBiasVector": darkenBias
                ]),
                    let randomScratches = darkeningFilter.outputImage else {
                        return
                }
                
                guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                    [
                        kCIInputImageKey: randomScratches
                ]),
                    let darkScratches = grayscaleFilter.outputImage
                    else {
                        return
                }
                
                guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                    [
                        kCIInputImageKey: darkScratches,
                        kCIInputBackgroundImageKey: speckledImage
                ]),
                    let oldFilmImage = oldFilmCompositor.outputImage
                    else {
                        return
                }
                request.finish(with: oldFilmImage.cropped(to: request.sourceImage.extent), context: nil)
            })
            saveEditVideoInDirectory(composition: composition,asset:vidAsset, videofilePath: "editMoviewImage.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        } else if filterName == EditFilter.vignette {
            let filter = CIFilter(name: "CIVignetteEffect")!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                filter.setValue(filterVal, forKey: "inputRadius")
                filter.setValue(1.0, forKey: "inputIntensity")
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset:vidAsset, videofilePath: "editMoviewImage.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        } else {
            let filter = CIFilter(name: filterKey)!
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                filter.setValue(filterVal, forKey: filterInputkey)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset:vidAsset, videofilePath: "editMoviewImage.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        }
    }
    
    func saveEditVideoInDirectory(composition: AVVideoComposition,asset: AVAsset,videofilePath: String,completion:@escaping(_ url: URL) -> Void) {
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as NSString
        let movieFilePath = docsDir.appendingPathComponent(videofilePath)
        let movieDestinationUrl = NSURL(fileURLWithPath: movieFilePath)
        let assetExport = AVAssetExportSession(asset: asset, presetName:AVAssetExportPresetHighestQuality)
        assetExport?.outputFileType = AVFileType.mov
        assetExport?.videoComposition = composition
        do {
            try FileManager.default.removeItem(at: movieDestinationUrl as URL)
        } catch {
        }
        assetExport?.outputURL = movieDestinationUrl as URL
        assetExport?.exportAsynchronously(completionHandler: {
            switch assetExport!.status {
            case AVAssetExportSession.Status.failed:
                print("failed")
                print(assetExport?.error ?? "unknown error")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled")
                print(assetExport?.error ?? "unknown error")
            case AVAssetExportSession.Status.completed:
                print("Movie complete")
                completion(movieDestinationUrl.absoluteURL!)
            default:
                break
            }
        })
    }
    
    func applyGrainFiterVideoMethod(_ urlVideo: URL,slideVal: CGFloat,isSlide:Bool) {
        let asset = AVURLAsset(url: urlVideo, options: nil)
        if !isSlide {
            objEditingVM.videoPlayer = AVPlayer()
            objEditingVM.playerItem = AVPlayerItem(url: urlVideo)
        }
        let composition = AVVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in
            let source = request.sourceImage.clampedToExtent()
            guard let sepiaFilter = CIFilter(name:"CISepiaTone",parameters: [
                kCIInputImageKey: source,
                kCIInputIntensityKey: slideVal
            ]) else {
                print("App bundle doesn't exist")
                return
            }
            
            guard let sepiaCIImage = sepiaFilter.outputImage else {
                print("App bundle doesn't exist")
                return
            }
            
            guard let coloredNoise = CIFilter(name:"CIRandomGenerator"),
                let noiseImage = coloredNoise.outputImage else {
                    print("App bundle doesn't exist")
                    return
            }
            
            let whitenVector = CIVector(x: 0, y: 0, z: 0, w: 0)
            let fineGrain = CIVector(x:0, y: slideVal, z:0, w:0)
            let zeroVector = CIVector(x: 0, y: 0, z: 0, w: 0)
            
            guard let whiteningFilter = CIFilter(name:"CIColorMatrix",
                                                 parameters:
                [
                    kCIInputImageKey: noiseImage,
                    "inputRVector": whitenVector,
                    "inputGVector": whitenVector,
                    "inputBVector": whitenVector,
                    "inputAVector": fineGrain,
                    "inputBiasVector": zeroVector
                    
            ]), let whiteSpecks = whiteningFilter.outputImage else {
                print("App bundle doesn't exist")
                return
            }
            guard let speckCompositor = CIFilter(name:"CISourceOverCompositing",
                                                 parameters:
                [
                    kCIInputImageKey: whiteSpecks,
                    kCIInputBackgroundImageKey: sepiaCIImage
            ]) , let speckledImage = speckCompositor.outputImage else {
                print("App bundle doesn't exist")
                return
            }
            
            let verticalScale = CGAffineTransform(scaleX: 1.5, y: 25)
            let transformedNoise = noiseImage.transformed(by: verticalScale)
            
            let darkenVector = CIVector(x: 4, y: 0, z: 0, w: 0)
            let darkenBias = CIVector(x: 0, y: 1, z: 1, w: 1)
            
            guard let darkeningFilter = CIFilter(name:"CIColorMatrix", parameters: [
                kCIInputImageKey: transformedNoise,
                "inputRVector": darkenVector,
                "inputGVector": zeroVector,
                "inputBVector": zeroVector,
                "inputAVector": zeroVector,
                "inputBiasVector": darkenBias
            ]),
                let randomScratches = darkeningFilter.outputImage else {
                    return
            }
            
            guard let grayscaleFilter = CIFilter(name:"CIMinimumComponent", parameters:
                [
                    kCIInputImageKey: randomScratches
            ]),
                let darkScratches = grayscaleFilter.outputImage
                else {
                    return
            }
            
            guard let oldFilmCompositor = CIFilter(name:"CIMultiplyCompositing",parameters:
                [
                    kCIInputImageKey: darkScratches,
                    kCIInputBackgroundImageKey: speckledImage
            ]),
                let oldFilmImage = oldFilmCompositor.outputImage
                else {
                    return
            }
            request.finish(with: oldFilmImage.cropped(to: request.sourceImage.extent), context: nil)
        })
        objEditingVM.playerItem.videoComposition = composition
        if !isSlide {
            objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
            objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
            objEditingVM.avPlayerController.player?.play()
            loopPlayVideoMethod()
        } else {
            if objEditingVM.avPlayerController.player!.rate == 0 {
                objEditingVM.avPlayerController.player?.play()
            }
        }
    }
    
    func applyTempreatureFilterVideoMethod(_ urlVideo: URL,sliderValue: CGFloat,isSlide: Bool) {
        let asset = AVURLAsset(url: urlVideo, options: nil)
        if !isSlide {
            objEditingVM.videoPlayer = AVPlayer()
            objEditingVM.playerItem = AVPlayerItem(url: urlVideo)
        }
        let filter = CIFilter(name: "CITemperatureAndTint")!
        let composition = AVVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: "inputImage")
            let seconds = CMTimeGetSeconds(request.compositionTime)
            filter.setValue(CIVector(x: CGFloat(seconds) * sliderValue, y: 100), forKey: "inputNeutral")
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            request.finish(with: output, context: nil)
        })
        objEditingVM.playerItem.videoComposition = composition
        if !isSlide {
            objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
            objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
            objEditingVM.avPlayerController.player?.play()
            loopPlayVideoMethod()
        } else {
            if objEditingVM.avPlayerController.player!.rate == 0 {
                objEditingVM.avPlayerController.player?.play()
            }
        }
    }
    
    func orientation(from transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
    func compositionLayerInstruction(for track: AVCompositionTrack, assetTrack: AVAssetTrack) -> AVMutableVideoCompositionLayerInstruction {
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let transform = assetTrack.preferredTransform
        instruction.setTransform(transform, at: .zero)
        return instruction
    }
    
    func playNormalVideoWithoutFilter(fileURL: URL) {
        objEditingVM.videoPlayer = AVPlayer()
        objEditingVM.playerItem = AVPlayerItem(url: fileURL)
        objEditingVM.videoPlayer.replaceCurrentItem(with: objEditingVM.playerItem)
        objEditingVM.avPlayerController.player = objEditingVM.videoPlayer
        objEditingVM.avPlayerController.player?.play()
        loopPlayVideoMethod()
    }
    
    func colorSaveVideoMethod(fileURL: URL,selectColorIndex: Int,hueColor: CGFloat,saturationColor: CGFloat,luminanceColor: CGFloat,completion:@escaping(_ url: URL) -> Void) {
        let vidAsset = AVURLAsset(url: fileURL, options: nil)
        let filter = objEditingVM.objMultiBandHSV
        switch selectColorIndex {
        case 0:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputRedShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 1:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputOrangeShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 2:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputYellowShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 3:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputGreenShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 4:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputAquaShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 5:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputBlueShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 6:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputPurpleShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        case 7:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputMagentaShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        default:
            let compositionNew = AVVideoComposition(asset: vidAsset, applyingCIFiltersWithHandler: { request in
                let source = request.sourceImage.clampedToExtent()
                filter.inputImage = source
                filter.inputRedShift = CIVector(x: hueColor, y: saturationColor, z: luminanceColor)
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                request.finish(with: output, context: nil)
            })
            saveEditVideoInDirectory(composition: compositionNew,asset: vidAsset, videofilePath: "colorPath.mp4") { (urlVideo) in
                completion(urlVideo)
            }
        }
    }
}
